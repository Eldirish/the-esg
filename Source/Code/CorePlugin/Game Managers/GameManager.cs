﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Resources;
using Duality.Components;
using Duality.Components.Renderers;
using MFEP.Duality.Plugins.InputPlugin;

namespace The_ESG
{
	public class GameManager : Component, ICmpInitializable, ICmpUpdatable
	{
		//public ContentRef<StageProperties> stageRef { get; set; }
		public static  StageProperties stageProps;
		public ContentRef<LevelList> levelListRef { get; set; }

		public ContentRef<StateManager> stateRef { get; set; }
		public static StateManager state;
		//public static List<ContentRef<Scene>> sceneRefs;

		public ContentRef<Prefab> enemySpawnTemplate { get; set; }
		public ContentRef<Prefab> cargoSpawnTemplate { get; set; }
		public ContentRef<Prefab> hazardSpawnTemplate { get; set; }
		public ContentRef<Prefab> bossSpawnTemplate { get; set; }
		public ContentRef<Prefab> playerTemplate { get; set; }
		public Prefab eSpawn;
		public Prefab cSpawn;
		public Prefab hSpawn;
		public Prefab bSpawn;
		public Prefab pSpawn;

		[DontSerialize]
		private List<KeyValuePair<ContentRef<Scene>, ContentRef<StageProperties>>> _stageList;
		public List<KeyValuePair<ContentRef<Scene>, ContentRef<StageProperties>>> stageList { get { return _stageList; } 
																							set { _stageList = value; } }


		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{
				GameObj.GetComponent<SpawnManager>().ActiveSingle = false;
				//scen = levelListRef.Res.Stages.ToList();
				//sceneRefs = new List<ContentRef<Scene>>();
				state = stateRef.Res;
				stageProps = levelListRef.Res.Stages[Scene.Current].Res;
				//if (stageList.Count > 0) stageList.Clear();
				_stageList = levelListRef.Res.Stages.ToList();
			

				eSpawn = enemySpawnTemplate.Res;
				cSpawn = cargoSpawnTemplate.Res;
				hSpawn = hazardSpawnTemplate.Res;
				bSpawn = bossSpawnTemplate.Res;
				pSpawn = playerTemplate.Res;



				if (stageProps.BgMusic != null && DualityApp.Sound.Playing.Count() == 0)
				{
					DualityApp.Sound.PlaySound(stageProps.BgMusic);
				}
			}


		}

		



		public void OnShutdown(ShutdownContext context)
		{
			
		}

		public void OnUpdate()
		{
			Log.Game.Write(Scene.Current.Name + "index:"+ stageProps.stageIndex.ToString());
			
			if (stageProps.stageIndex == 1 && DualityApp.Sound.Playing.Count() == 0)
				{
				DualityApp.Sound.PlaySound(stageProps.BgMusic);
				}

			if(state.gameState == 3)
			{
				if(InputManager.IsButtonHit("Fire"))
				{
					Restart();

				}

			}
		}

		public void ResolveStage(bool live)
		{
			GameObj.GetComponent<SpawnManager>().ActiveSingle = true;

			if (!live)
			{
				//ScoreManager.countScore(ScoreManager.baseScore, ScoreManager.enemiesKilled, ScoreManager.shotsFired, ScoreManager.shotsHit);
                

				state.setMode(3);
				DualityApp.Sound.StopAll();
				Scene.SwitchTo(stageList[0].Key.Res, false);
			}

			 if (live)
			{
				int index = stageProps.stageIndex++;
				if (stageProps.Vertical)
				{

					GenGrid grid = new GenGrid(stageProps.cellQuantity, 240);
					grid.DefineSpawns(true, 0.60f, 0.30f, 0.1f, 0.1f);
					state.setMode(1);
					Scene.SwitchTo(stageList[stageProps.stageIndex++].Key.Res,false);
					grid.Populate(pSpawn, eSpawn, hSpawn, bSpawn, cSpawn);

				}

				if (!stageProps.Vertical)
				{
					Scene.SwitchTo(stageList[1].Key.Res, true);
					state.setMode(0);
					stageProps.stageIndex = 1;
					Scene.Reload();

				}



				ScoreManager.baseScore += 500;

			}



		}

		public void Restart()
		{

			ScoreManager.ResetScore();
			Scene.SwitchTo(stageList[1].Key.Res, true);
			state.setMode(0);
			stageProps.stageIndex = 1;
			Scene.Reload();
		}



	}
}
