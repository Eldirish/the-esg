﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;

namespace The_ESG
{
	public class Tag: Component
	{
		public string id { get; private set; }
		public string wideId { get; private set; }

		public void SetId(string text){
			id = text;
		}
		public void SetWideId(string text){

			wideId = text;
		}
	}
}
