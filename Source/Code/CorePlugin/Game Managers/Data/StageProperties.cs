﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;

namespace The_ESG
{
	public class StageProperties: Resource
	{
		public bool Vertical 		{ get; set; }

		public string stageName 	{ get; private set; }
		public int stageIndex 		{ get;  set; }

		public ContentRef<Sound> BgMusic 		{ get; private set;  }

		public int maxSpawners 		{ get; set; }
		public int maxSquadSpawns 	{ get; set; }

		public float spawnIntervals { get; private set; }

		public List<ContentRef<Prefab>> squadList { get; set; }

		public int cellQuantity { get; set;}


	}
}
