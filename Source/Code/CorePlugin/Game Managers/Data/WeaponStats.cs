﻿using System;
using Duality;
using Duality.Resources;

namespace The_ESG
{
	public class WeaponStats: Resource
	{


		public int 	bulletDamage 		{ get; set; } = 5;
		public float bulletRange 		{ get; set; } = 10;
		public float bulletCritChance 	{ get; set; } = 0.2f;
		public float bulletSpeed 		{ get; set; } = 0.5f;



		public float attackCooldown { get; set;}
		public float attackCooldownDefault { get; set; }



	}
}
