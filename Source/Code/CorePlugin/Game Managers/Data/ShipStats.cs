﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Resources;
using Duality.Components;
using Duality.Components.Renderers;

namespace The_ESG
{
	public class ShipStats : Resource
	{
		public Vector3 linearSpeed { get; set; }
		public float rotationSpeed { get; set; } = 0.05f;

		public float damageModifier { get; set; } = 0;
		public float damageMultiplier { get; set; } = 1;
		public float shotCooldownModifier { get; set; } = 0;
		public float abiltyCooldownModifier { get; set; } = 0;
		public float damageCooldown { get; set; } = 0;

		public int shipRamDamage { get; set; } = 0;

		public int shipMaxHealthPoints { get; private set; } = 40;
		public int shipCurrentHealthPoints { get; set; }


		public Vector3 maxVelocity { get; set; } = new Vector3(0, 0, 0);
		public Vector3 friction { get; set; } = new Vector3(0, 0, 0);

		public int scoreWorth { get; set; }
		public ContentRef<Prefab> deathPrefabRef { get; set; }
		public ContentRef<Sound> deathSound {get; set;}

		//public Transform ship;
		//public AnimSpriteRenderer spriteAnim { get; set; }

	}

}
