﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Resources;
             
namespace The_ESG
{
	public class StateManager: Resource
	{
		/// <summary>
		/// 0 = Vertical level flight
		/// 1 = Free Flight level
		/// 2 = Menu
		/// 3 = Game Over
		/// </summary>
		/// <value>The state of the game.</value>
		public int gameState { get; set; } = 0;

		/// <summary>
		/// 0 = Vertical level flight
		/// 1 = Free Flight level
		/// 2 = Menu
		/// 3 = Game Over
		/// </summary>
		/// <value>The state of the game.</value>
		public void setMode(int i)
		{
			gameState = i;

		}


	}
}
