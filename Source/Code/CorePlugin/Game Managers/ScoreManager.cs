﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;

namespace The_ESG
{
	public static  class ScoreManager
	{
		public static int  baseScore = 0;
		public static int enemiesKilled = 0;
		public static float shotsFired = 0;
		public static float shotsHit = 0;
		public static float accuracy = 0.0f;

		public static float finalScore { get;  set; } = 0;


		public static float countScore(int bs, int ek, float sf,float sh)
		{
			finalScore = bs + ek;
			if(sf != 0)
			{
				accuracy = (sh / sf) * 100;
				finalScore *= sh / sf;
				accuracy = MathF.Round(accuracy,1);
			}

			return MathF.RoundToInt(finalScore);
		}

		public static void ResetScore()
		{
			baseScore = 0;
			enemiesKilled = 0;
			shotsHit = 0;
			shotsFired = 0;
			accuracy = 0;
			finalScore = 0;
		}

	}
}
