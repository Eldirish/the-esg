﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;

namespace The_ESG
{
	public class SpawnManager: Component, ICmpInitializable, ICmpUpdatable
	{
		float spawnTimer;


		public ContentRef<StageProperties> stageRef { get; set; }
		StageProperties stage;
		public ContentRef<StateManager> stateRef { get; set; }
		StateManager state;


		[DontSerialize]
		public List<ContentRef<Prefab>> Squads;

		[DontSerialize]
		List<GameObject> Spawners;
		public List<GameObject> _spawners { get { return Spawners; } }
		[DontSerialize]
		List<Prefab> sqd;







		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{



				state = stateRef.Res;
				stage = stageRef.Res;
				Squads = stage.squadList;
				sqd = new List<Prefab>();
				Spawners = new List<GameObject>();
				//sqd.Clear();
				//Spawners.Clear();
				spawnTimer = stage.spawnIntervals;


				foreach (Tag found in Scene.Current.FindComponents<Tag>())
				{
					if (found.id == "_Spawner")
					{
						Spawners.Add(found.GameObj);

					}

				}


				for (int i = 0; i < Squads.Count; i++)
				{
					sqd.Add(Squads[i].Res);  

				}


				for (int i = 0; i > Spawners.Count; i++)
				{
						Spawners[i].GetComponent<SpawnerBehaviour>().Spawn(sqd[MathF.Rnd.Next(Squads.Count)]);

				}
			}
		}

		public void OnShutdown(ShutdownContext context)
		{
			
		}

		public void OnUpdate()
		{
			//Only gonna happen when the mode is vertical, becasue it has multiple spawns from 1 spawner

			//	Log.Game.Write(sqd.Count().ToString() + "Spawners:" + Spawners.Count.ToString());
			//GameObject g = Scene.Current.FindComponent<Tag>().GameObj;
			//Log.Game.Write(g.GetComponent<Tag>().id.ToString());
			if(state.gameState == 0)
			{
				
				spawnTimer -= 0.5f * Time.TimeMult;

				if(spawnTimer <= 0)
				{
					
					Spawners[0].GetComponent<SpawnerBehaviour>().Spawn(sqd[MathF.Rnd.Next(sqd.Count)]);
					spawnTimer = stage.spawnIntervals;
				}

			}



		}





	}
}
