﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;

using MFEP.Duality.Plugins.InputPlugin;

namespace The_ESG
{
	[RequiredComponent(typeof(TextRenderer))]
	public class Tutorial : Component, ICmpUpdatable, ICmpInitializable
	{
		TextRenderer text;
		string tutorial;
		float opacity;

		public void OnInit(InitContext context)
		{
			text = this.GameObj.GetComponent<TextRenderer>();
			tutorial = string.Format("[{0}{1}{2}{3}] To Fly! /n" +
									 "[{4}] To Light'em Up!",
			                         InputManager.GetVirtualButton("Fly Forward").AssociatedKeys[0].ToString(),
			                         InputManager.GetVirtualButton("Strafe Left").AssociatedKeys[0].ToString(),
			                         InputManager.GetVirtualButton("Fly Backwards").AssociatedKeys[0].ToString(),
			                         InputManager.GetVirtualButton("Strafe Right").AssociatedKeys[0].ToString(),
			                         InputManager.GetVirtualButton("Fire").AssociatedKeys[0].ToString());

			opacity = 80;
		}

		public void OnShutdown(ShutdownContext context)
		{

		}

		public void OnUpdate()
		{
			opacity -= 1 * Time.TimeMult;

			if (opacity > 0)
			{
				text.Text.SourceText = tutorial;
			}
			if(opacity <= 0)
			{
				text.Text.SourceText = null;
			}
		}
	}
}
