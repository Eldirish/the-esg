﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;

namespace The_ESG
{

	[RequiredComponent(typeof(TextRenderer))]
	public class Scoreboard : Component, ICmpInitializable
	{
		TextRenderer text;
		string board;
		public void OnInit(InitContext context)
		{	

			Log.Game.Write("Score:{0} - Kills:{1} - Fire:{2} - Hits:{3}", ScoreManager.baseScore,
																							ScoreManager.enemiesKilled,
																							ScoreManager.shotsFired,
																							ScoreManager.shotsHit);

			Log.Game.Write(ScoreManager.countScore(ScoreManager.baseScore,ScoreManager.enemiesKilled,ScoreManager.shotsFired,ScoreManager.shotsHit).ToString());
																							
																							



		

			if (ScoreManager.accuracy == 0)
			{
				ScoreManager.accuracy.ToString("Press the Fire Key [J] to shoot!");

			}


			else if(ScoreManager.shotsFired == 0)
		{
				ScoreManager.accuracy.ToString("Press the Fire Key [J] to shoot!");
		}





				board = string.Format("Base Score:   /cFFFF00FF{0}/cFFFFFFFF /n " +
									  "Enemy Kills: /cFFFF00FF{1}/cFFFFFFFF /n " +
									  "Accuracy:    /cFFFF00FF{2}%/cFFFFFFFF ",
				                      ScoreManager.baseScore.ToString(), ScoreManager.enemiesKilled.ToString(),
									  ScoreManager.accuracy.ToString());
			
			 

			text = this.GameObj.GetComponent<TextRenderer>();

			text.Text.SourceText = board;


		}

		public void OnShutdown(ShutdownContext context)
		{
			
		}
	}
}
