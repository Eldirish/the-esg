﻿using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;
using System;

namespace The_ESG
{
	[RequiredComponent(typeof(TextRenderer))]
	public class ShowHP : Component, ICmpUpdatable, ICmpInitializable
	{
		TextRenderer text;
		GameObject player;
		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{
				text = this.GameObj.GetComponent<TextRenderer>();
				if (GenGrid.playerPlaced)
				{
					
					player = Scene.Current.FindComponent<PlayerBehaviour>().GameObj;
				}

			}
		}

		public void OnShutdown(ShutdownContext context)
		{

		}

		public void OnUpdate()
		{
			if (player != null)
			{
				text.Text.SourceText = player.GetComponent<PlayerBehaviour>().hp.ToString();
			}
		}
	}
}
