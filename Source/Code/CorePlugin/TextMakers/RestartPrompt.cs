﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;

using MFEP.Duality.Plugins.InputPlugin;

namespace The_ESG
{
	[RequiredComponent(typeof(TextRenderer))]
	public class RestartPrompt : Component, ICmpInitializable
	{
		TextRenderer text;
		string prompt;

		public void OnInit(InitContext context)
		{
			text = this.GameObj.GetComponent<TextRenderer>();
			prompt = string.Format("Press [{0}] to fight again!", InputManager.GetVirtualButton("Fire").AssociatedKeys[0].ToString());

			text.Text.SourceText = prompt;


		}

		public void OnShutdown(ShutdownContext context)
		{

		}
	}
}