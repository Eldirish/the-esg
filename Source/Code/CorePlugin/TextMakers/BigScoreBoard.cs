﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;

namespace The_ESG
{

	[RequiredComponent(typeof(TextRenderer))]
	public class 
	BigScoreBoard : Component, ICmpInitializable
	{
		TextRenderer text;
		string board;
		public void OnInit(InitContext context)
		{

				board = string.Format("-FINAL SCORE- /n/cFFFF00FF/ac            {0}", ScoreManager.countScore(ScoreManager.baseScore,
																							ScoreManager.enemiesKilled,
																							ScoreManager.shotsFired,
																							ScoreManager.shotsHit));

			text = this.GameObj.GetComponent<TextRenderer>();

			text.Text.SourceText = board;


		}

		public void OnShutdown(ShutdownContext context)
		{

		}
	}
}
