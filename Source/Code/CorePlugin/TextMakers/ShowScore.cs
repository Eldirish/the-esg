﻿using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;
using System;

namespace The_ESG
{
	[RequiredComponent(typeof(TextRenderer))]
	public class ShowScore: Component, ICmpUpdatable, ICmpInitializable
	{
		TextRenderer text;

		public void OnInit(InitContext context)
		{
			text = this.GameObj.GetComponent<TextRenderer>();
		}

		public void OnShutdown(ShutdownContext context)
		{
			
		}

		public void OnUpdate()
		{
			text.Text.SourceText = ScoreManager.baseScore.ToString();
		}
	}
}
