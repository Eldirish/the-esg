﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;

namespace The_ESG
{
	public class SpawnerBehaviour : Component, ICmpInitializable, ICmpUpdatable
	{

		GameObject t;

	
		public void OnInit(InitContext context)
		{

		}

		public void OnShutdown(ShutdownContext context)
		{
			
		}

		public void OnUpdate()
		{

		}


		public void Spawn(Prefab target)
		{
			Vector3 spawnPlace = new Vector3(this.GameObj.Transform.Pos.X, this.GameObj.Transform.Pos.Y, 0);
			 t = target.Instantiate(spawnPlace, 0, 1);
			t.Parent = this.GameObj;
			Scene.Current.AddObject(t);



		}


	}
}
