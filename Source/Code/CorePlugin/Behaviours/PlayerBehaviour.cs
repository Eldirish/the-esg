﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;

namespace The_ESG
{
	[RequiredComponent(typeof(RigidBody))]
	public class PlayerBehaviour : Component, ICmpInitializable, ICmpUpdatable, ICmpCollisionListener
	{
		
		public ContentRef<ShipStats> Stats {get; set;}

		public int hp;
		float dmgCooldown;
		Transform ship;
        GameManager gM;


		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate) 
			{
				hp = Stats.Res.shipMaxHealthPoints;
				ship = this.GameObj.GetComponent<Transform>();
				dmgCooldown = Stats.Res.damageCooldown;

                gM = Scene.Current.FindComponent<GameManager>();
			} 

		}

		public void OnShutdown(ShutdownContext context)
		{

		}

		public void OnCollisionBegin(Component sender, CollisionEventArgs args)
		{
			var hitRigidbody = args as RigidBodyCollisionEventArgs;

			GameObject hitObject = hitRigidbody.CollideWith.GetComponent<Tag>().GameObj;
			if (hp > 0 && dmgCooldown <= 0)
			{ 	if (hitObject.GetComponent<Tag>().id == "_Enemy")
				{
					takeDamage(hitObject.GetComponent<GeneralEnemyBehaviour>().enemyStatsRef.Res.shipRamDamage, Stats.Res.deathSound);
				}
			}

		}

		public void OnCollisionEnd(Component sender, CollisionEventArgs args)
		{
			
		}

		public void OnCollisionSolve(Component sender, CollisionEventArgs args)
		{
			
		}



		public void OnUpdate()
		{

			if (hp > 0 && dmgCooldown > 0){
				dmgCooldown -= 0.5f * Time.TimeMult;

			}

			if (hp <= 0)
			{
				gM.ResolveStage(false);

			}


			if (DualityApp.Keyboard.KeyHit(Duality.Input.Key.L)) gM.ResolveStage(true);

		}


		void takeDamage(int amount, ContentRef<Sound> hurtSound)
		{
			if (hurtSound != null )
			{
				DualityApp.Sound.PlaySound3D(hurtSound.Res, ship.Pos);

			}
			hp -= amount;
			dmgCooldown = Stats.Res.damageCooldown;

		}

	}
}
