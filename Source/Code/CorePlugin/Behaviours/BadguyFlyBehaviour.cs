﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;


namespace The_ESG
{

	[RequiredComponent(typeof(Transform))] [RequiredComponent(typeof(RigidBody))]
	public class BadguyFlyBehaviour : Component, ICmpInitializable, ICmpUpdatable, ICmpCollisionListener
	{
		 Random random = new Random();

		public ContentRef<ShipStats> statsRef 		{ get; set; }
		public ContentRef<StateManager> stateRef 	{ get; set; }
		public ContentRef<Prefab> attackRef 		{ get; set; }
		ShipStats stats;

		Vector3 movVector;

		Transform ship;

		float xModifier = 0;
		private float dirChange;
		private float dirChangeMax;

		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{
				dirChange = 0;
				movVector = new Vector3(0, 0, 0);
				stats = statsRef.Res;
				ship = this.GameObj.GetComponent<Transform>();
			
				xModifier = stats.linearSpeed.X * random.NextFloat(0.4f, 1.2f) * MathF.Rnd.Next(-1,2);
				dirChangeMax = MathF.Rnd.NextFloat(30,70);

				movVector.X = xModifier;
				

				//Log.Game.Write(dirChange.ToString() + "--" +dirChangeMax.ToString() );
			}
		}

		public void OnShutdown(ShutdownContext context)
		{
	
		}

		public void OnUpdate()
		{
			dirChange += 0.5f * Time.TimeMult;

			zigzag(dirChange, dirChangeMax); 
			if (dirChange >= dirChangeMax)
			{
				dirChange = 0;

			}

			ship.MoveBy(movVector);

		}


		public void OnCollisionBegin(Component sender, CollisionEventArgs args)
		{
			
		}

		public void OnCollisionEnd(Component sender, CollisionEventArgs args)
		{
			
		}

		public void OnCollisionSolve(Component sender, CollisionEventArgs args)
		{
			
		}

		#region behaviour methods
		void zigzag(float dirTimer, float dirTimerMax)
		{

			//Log.Game.Write(dirTimerMax.ToString());
			if (movVector.X == 0)
			{
				movVector.Y = stats.linearSpeed.Y + 0.6f * Time.TimeMult;
			}
			else movVector.Y = stats.linearSpeed.Y;

			if (dirTimer >= dirTimerMax)
			{
				movVector.X *= -1;
			}

			if (ship.Pos.X >= 240  || ship.Pos.X <= -260 )
			{
				
				movVector.X *= -1;
			}

			//ship.MoveBy(movVector);

		}



		#endregion






	}
}
