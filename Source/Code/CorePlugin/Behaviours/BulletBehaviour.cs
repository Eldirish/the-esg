﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;


namespace The_ESG
{	[RequiredComponent(typeof(Transform))]
	public class BulletBehaviour : Component, ICmpInitializable, ICmpUpdatable, ICmpCollisionListener
	{



		public ContentRef<WeaponStats> statsRef { get; set;}
		public ContentRef<Prefab> deathPrefRef { get; set; }
		public WeaponStats wStats;
		Transform bullet;

		Vector3 Direction = new Vector3(0, 0, 0);

		float bulletTime;


		public void OnInit(InitContext context)
		{
			AnimSpriteRenderer anim = GameObj.GetComponent<AnimSpriteRenderer>();
			bullet = GameObj.GetComponent<Transform>();
			wStats = statsRef.Res;
			anim.AnimPaused = false;

		}

		public void OnShutdown(ShutdownContext context)
		{
		}
		

		public void OnUpdate()
		{
			bulletTime += 1 * Time.TimeMult;
			Direction = bullet.Forward * wStats.bulletSpeed ;
			bullet.MoveBy(Direction * Time.TimeMult);

			if(bulletTime >= wStats.bulletRange)
			{
			
				Scene.Current.RemoveObject(this.GameObj);
			}


		}

		public void OnCollisionBegin(Component sender, CollisionEventArgs args)
		{
			var hitRigidBody = args as RigidBodyCollisionEventArgs;

			//GameObject hitObject = args.CollideWith;


			if (hitRigidBody.CollideWith.GetComponent<Tag>().wideId == "_Friendly")
			{
				return;
			}

			if (hitRigidBody.CollideWith.GetComponent<Tag>().wideId == "_Solid")
				{
					
					if (hitRigidBody.CollideWith.GetComponent<Tag>().id == "_Enemy")
					{
						ScoreManager.shotsHit += 1;
						
					}
				BulletDestroy(deathPrefRef.Res, null);

				}
			}



		public void OnCollisionEnd(Component sender, CollisionEventArgs args)
		{
			
		}

		public void OnCollisionSolve(Component sender, CollisionEventArgs args)
		{
			
		}

		public void BulletDestroy(Prefab destroyPref, Sound destroySound)
		{
			if (destroyPref != null)
			{
				GameObject destroyObj = destroyPref.Instantiate(this.GameObj.Transform.Pos, this.GameObj.Transform.Angle, 1.5f);
				Scene.Current.AddObject(destroyObj);
			}
			if (destroySound != null)
			{
				DualityApp.Sound.PlaySound3D(destroySound, this.GameObj.Transform.Pos);
			}
			Scene.Current.RemoveObject(this.GameObj);
		}
	}
}