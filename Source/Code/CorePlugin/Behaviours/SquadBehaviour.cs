﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;

namespace The_ESG
{
	public class SquadBehaviour : Component, ICmpInitializable
	{
		[DontSerialize]
		public GameObject spawner;
		[DontSerialize]
		List<GameObject> soldiers;
		Vector3 soldierPos;
		Transform squad;
		[DontSerialize]
		Vector3 position; 

		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{
				soldiers = new List<GameObject>(this.GameObj.Children);
				squad = this.GameObj.GetComponent<Transform>();
				position = squad.Pos;
			

				for (int i = 0; i < soldiers.Count; i++)
				{
					soldierPos = soldiers[i].GetComponent<Transform>().Pos;
					soldierPos.X = MathF.Rnd.NextFloat(position.X - 240, position.X + 240);
					if (soldiers[i].GetComponent<Tag>() == null)
					{
						soldiers[i].AddComponent<Tag>();
						soldiers[i].GetComponent<Tag>().SetId("_Enemy");
						soldiers[i].GetComponent<Tag>().SetWideId("_Solid");
					}
					//Log.Game.Write(soldiers[i].GetComponent<Tag>().ToString());
				}

				//spawner = this.GameObj.Parent;

				//squad.SetTransform(spawner.GetComponent<Transform>());
			}
		}

		public void OnShutdown(ShutdownContext context)
		{

		}
	}
}
