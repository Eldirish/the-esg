﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;

namespace The_ESG
{
	[RequiredComponent(typeof(Transform))]
	public class GeneralEnemyBehaviour: Component, ICmpUpdatable, ICmpInitializable, ICmpCollisionListener
	{
		public ContentRef<ShipStats> enemyStatsRef { get; set;}
		public ContentRef<StateManager> stateRef { get; set; }
		//public ContentRef<Prefab> deathPrefRef { get; set; }
		//ScoreManager scoreBoard;
		ShipStats enemyStats;
		public ContentRef<Prefab> explod { get; set; }
		int hp;

		Transform ship; 

		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{
				//scoreBoard = new ScoreManager();
				enemyStats = enemyStatsRef.Res;

				hp = enemyStats.shipMaxHealthPoints;

				ship = this.GameObj.Transform;
			}


		}

		public void OnShutdown(ShutdownContext context)
		{
			
		}

		public void OnUpdate()
		{
			

			if (hp <= 0)
			{
				Die(enemyStats.scoreWorth, enemyStats.deathPrefabRef.Res, enemyStats.deathSound.Res);
				//Scene.Current.AddObject(explod.Res.Instantiate(this.GameObj.Transform.Pos));



			}
			if (stateRef.Res.gameState == 0)
			{
				if (ship.Pos.Y > DualityApp.TargetResolution.Y + 32)
				{
					Scene.Current.RemoveObject(this.GameObj);

				}
			}

			
		}

		public void OnCollisionBegin(Component sender, CollisionEventArgs args)
		{
			var body = args as RigidBodyCollisionEventArgs;
			GameObject contact = args.CollideWith;


			if (contact.GetComponent<Tag>() != null)
			{
				if (contact.GetComponent<Tag>().id == "_PlayerShot")
				{
					int damage = contact.GetComponent<BulletBehaviour>().wStats.bulletDamage;
					takeDamage(damage);


				}
			}
			else return;

		}

		public void OnCollisionEnd(Component sender, CollisionEventArgs args)
		{
			
		}

		public void OnCollisionSolve(Component sender, CollisionEventArgs args)
		{
			
		}

		#region methods

		void takeDamage(int amount)
		{

			hp -= amount;

		}

		void Die(int scoreValue, Prefab deadPrefab, ContentRef<Sound> deathSound)
		{
			ScoreManager.baseScore += scoreValue;
			ScoreManager.enemiesKilled += 1;

			if (deathSound != null)
			{
				DualityApp.Sound.PlaySound(deathSound);
			}
			if (deadPrefab != null)
			{
				GameObject deathPref = deadPrefab.Instantiate(ship.Pos, ship.Angle, 0.15f);
				Scene.Current.AddObject(deathPref);
			}

			Scene.Current.RemoveObject(this.GameObj);
		}

	}
}
#endregion