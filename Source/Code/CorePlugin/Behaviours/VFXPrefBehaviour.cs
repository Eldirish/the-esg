﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Resources;
using Duality.Components.Renderers;

namespace The_ESG
{
	[RequiredComponent(typeof(AnimSpriteRenderer))]
	public class VFXPrefBehaviour : Component, ICmpInitializable, ICmpUpdatable
	{
		AnimSpriteRenderer anim;
		public void OnInit(InitContext context)
		{
			anim = this.GameObj.GetComponent<AnimSpriteRenderer>();
			anim.AnimPaused = false;


		}

		public void OnShutdown(ShutdownContext context)
		{

		}

		public void OnUpdate()
		{
			if(anim.AnimTime >= anim.AnimDuration)
			{
				Scene.Current.RemoveObject(this.GameObj);

			}
		}
	}
}
