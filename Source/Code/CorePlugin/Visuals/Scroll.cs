﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Components;
using Duality.Components.Renderers;
using Duality.Resources;

namespace The_ESG
{
	public class Scroll: Component, ICmpInitializable, ICmpUpdatable
	{
		SpriteRenderer sprite;
		Camera cam;
		Transform trans;
		Vector3 pos;
		Rect rekt;
		float ayy = 0;

		public float scrollSpeed { get; set;  }

		public void OnInit(InitContext context)
		{
			sprite = this.GameObj.GetComponent<SpriteRenderer>();
			rekt = sprite.Rect;
			trans = this.GameObj.GetComponent<Transform>();

			cam = Scene.Current.FindComponent<Camera>();
		}

		public void OnShutdown(ShutdownContext context)
		{

		}

		public void OnUpdate()
		{
			pos = trans.Pos;
			//Log.Game.Write(this.GameObj.Name + pos.Y.ToString());


				ayy = scrollSpeed * Time.TimeMult;
				trans.MoveBy(new Vector3(0, ayy, 0));

			 if(pos.Y >= 900)
			{
				trans.MoveTo(new Vector3(0,-800, 0));
				//trans

			}





		}
	}
}
