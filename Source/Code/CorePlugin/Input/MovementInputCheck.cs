﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using MFEP.Duality.Plugins.InputPlugin;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Input;
using Duality.Resources;
using Duality.Components.Renderers;


namespace The_ESG
{
	
	[RequiredComponent(typeof(Transform))]
	public class MovementInputCheck : Component, ICmpInitializable, ICmpUpdatable
	{
		bool Forward; 		
		bool Backwards;  	
		bool strafeRight;  
		bool strafeLeft;  	
		bool rotateRight;  	
		bool rotateLeft;  	

		public ContentRef<ShipStats> statsRef { get; set; }
		public ContentRef<StateManager> stateRef { get; set; }
		ShipStats sStats;
		StateManager state;

	    Transform ship;
		AnimSpriteRenderer spriteAnim;

		Vector3 movementVector;
		Vector3 pos;


		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{
				ship = GameObj.GetComponent<Transform>();
				spriteAnim = GameObj.GetComponent<AnimSpriteRenderer>();
				//sStats.ship = GameObj.GetComponent<Transform>();
				movementVector = new Vector3(0, 0, 0);
				sStats = statsRef.Res;
				state = stateRef.Res;
				 pos = ship.Pos;
			}

		}

		public void OnShutdown(ShutdownContext context)
		{

		}

		public void OnUpdate()
		{
			Forward 		= InputManager.IsButtonPressed("Fly Forward");
			Backwards 		= InputManager.IsButtonPressed("Fly Backwards");
			strafeRight 	= InputManager.IsButtonPressed("Strafe Right");
			strafeLeft 		= InputManager.IsButtonPressed("Strafe Left");
			rotateRight 	= InputManager.IsButtonPressed("Rotate Right");
			rotateLeft 		= InputManager.IsButtonPressed("Rotate Left");

			spriteAnim.AnimFirstFrame = 0;

			Vector3 velDiff = movementVector - sStats.maxVelocity;
			Vector3 fricdiff = sStats.friction - movementVector;

			#region control checks
			if(Forward)
			{
				movementVector += ship.Forward * sStats.linearSpeed.Y;

			}
			if (Backwards)
			{
				movementVector -= ship.Forward * sStats.linearSpeed.Y;

			}

			if(strafeRight)
			{
				spriteAnim.AnimFirstFrame = 8;

				if (state.gameState == 1)
				{
					ship.Angle += sStats.rotationSpeed;
				}
				else if (state.gameState == 0)
				{
					if (movementVector.X < 0)
					{
						movementVector.X = 0;
					}
					else 
					{
						movementVector.X += sStats.linearSpeed.X;
					}
				}
			}

			if (strafeLeft)
			{
				spriteAnim.AnimFirstFrame = 4;
				if (state.gameState == 1)
				{
					ship.Angle -= sStats.rotationSpeed;
				}

				else if (state.gameState == 0){

					if (movementVector.X > 0)
					{
						movementVector.X = 0;
					}
					else
					{
						movementVector.X -= sStats.linearSpeed.X;
					}

				}
			}

			if (rotateLeft)
			{
				movementVector += ship.Right * sStats.linearSpeed.Y;
			}

			if (rotateRight)
			{
				movementVector -= ship.Right * sStats.linearSpeed.Y;
			}
			#endregion


			if (movementVector != Vector3.Zero)
			{
				
				movementVector += fricdiff/10 * Time.TimeMult;
			}

			if (ship.Pos.X >= 240)
			{
				ship.MoveTo(new Vector3(240, ship.Pos.Y, 0));


			}
			if (ship.Pos.X < -240)
			{
				ship.MoveTo(new Vector3(-240, ship.Pos.Y, 0));

			}
			if(ship.Pos.Y > 300)
			{
				ship.MoveTo(new Vector3(ship.Pos.X, 300,0));

			}


			if (velDiff != Vector3.Zero)
			{
				ship.MoveBy(movementVector * Time.TimeMult);
			}
			else if (velDiff == Vector3.Zero)
			{
				
				movementVector += velDiff/2 * Time.TimeMult;

			}

			//Log.Game.Write(ship.Pos.X + "--" + DualityApp.TargetResolution.X + "--" + DualityApp.TargetResolution.X * -1);

		}
	}
}