﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using MFEP.Duality.Plugins.InputPlugin;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Input;
using Duality.Resources;
using Duality.Components.Renderers;


namespace The_ESG
{


	[RequiredComponent(typeof(Transform))]
	public class ActionInputCheck : Component, ICmpInitializable, ICmpUpdatable
	{
		//public float attackCooldown;
		//public float attackCooldownDefault { get; set; }

		public ContentRef<WeaponStats> statsRef { get; set; }
		public ContentRef<Prefab> shotPref { get; set; }
		public ContentRef<Sound> shotSound { get; set; }
		WeaponStats wStats;

		Vector3 offset;

		bool attackKey;
		bool abilityKey;
		bool itemkey;


		Transform ship;

		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{
				wStats = statsRef.Res;
				ship = GameObj.GetComponent<Transform>();
				wStats.attackCooldown = wStats.attackCooldownDefault;
				offset = new Vector3(0, -20, 0);

			}


		}

		public void OnShutdown(ShutdownContext context)
		{

		}

		public void OnUpdate()
		{

			attackKey = InputManager.IsButtonPressed("Fire");
			abilityKey = InputManager.IsButtonHit("Ship Ability");
			itemkey = InputManager.IsButtonHit("Use Item");


			wStats.attackCooldown -= 1 * Time.TimeMult;

			if (attackKey && wStats.attackCooldown <= 0)
			{
				Attack(wStats.attackCooldown, shotPref, ship);
				wStats.attackCooldown = wStats.attackCooldownDefault;

			}



		}


		void Attack(float delay, ContentRef<Prefab> bullet, Transform source)
		{
			if (delay <= 0)
			{
																	// offset Works for vertical portion/ LGW
				GameObject shot = bullet.Res.Instantiate(source.Pos + offset, source.Angle, source.Scale);

				Scene.Current.AddObject(shot);
				ScoreManager.shotsFired += 1;

				shot.GetComponent<AnimSpriteRenderer>().AnimLoopMode = AnimSpriteRenderer.LoopMode.Loop;
				DualityApp.Sound.PlaySound(shotSound);

			}

		}


	}
}