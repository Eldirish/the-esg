﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using MFEP.Duality.Plugins.InputPlugin;
using Duality.Components;
using Duality.Components.Physics;
using Duality.Input;
using Duality.Resources;

namespace The_ESG
{
	public class InputMaster : Component, ICmpInitializable, ICmpUpdatable
	{

		public VirtualButton mForward { get; set; }
		public VirtualButton mBackwards { get; set; }
		public VirtualButton strafeRight { get; set; }
		public VirtualButton strafeLeft { get; set; }
		public VirtualButton rotateRight { get; set; }
		public VirtualButton rotateLeft { get; set; }

		public VirtualButton mainAttack { get; set; }
		public VirtualButton shipAbility { get; set; }
		public VirtualButton item { get; set; }

		public bool customSet { get; set;} = false;

		public void OnInit(InitContext context)
		{
			if (context == InitContext.Activate)
			{
				if (!customSet)
				{ //Load default QWERTY

					/*SetForward("Fly Forward", Key.W);
					 * SetBackward("Fly Backwards", Key.S);
					 * 
					 * SetStrafeRight("Strafe Right", Key.D);
						SetStrafeLeft("Strafe Left", Key.A);
						SetRotateRight("Rotate Right", Key.E);
						SetRotateLeft("Rotate Left", Key.Q);

						SetAttack("Fire", Key.J);
						SetAbility("Ship Ability", Key.I);
						SetUseItem("Use Item", Key.P);
					 * 
					 * */

					mForward = new VirtualButton("Fly Forward", Key.W);
					mBackwards = new VirtualButton("Fly Backwards", Key.S);
					strafeRight = new VirtualButton("Strafe Right", Key.D);
					strafeLeft = new VirtualButton("Strafe Left", Key.A);
					rotateRight = new VirtualButton("Rotate Right", Key.E);
					rotateLeft = new VirtualButton("Rotate Left", Key.Q);

					mainAttack = new VirtualButton("Fire", Key.J);
					shipAbility = new VirtualButton("Ship Ability", Key.O);
					item = new VirtualButton("Use Item", Key.P);


				}
				InputManager.RegisterButton(mForward);
				InputManager.RegisterButton(mBackwards);
				InputManager.RegisterButton(strafeRight);
				InputManager.RegisterButton(strafeLeft);
				InputManager.RegisterButton(rotateRight);
				InputManager.RegisterButton(rotateLeft);

				InputManager.RegisterButton(mainAttack);
				InputManager.RegisterButton(shipAbility);
				InputManager.RegisterButton(item);



			}
		}

		public void OnShutdown(ShutdownContext context)
		{
			 
		}

		public void OnUpdate()
		{

		}


		/*public void SetForward (string name, Key key){
			
			mForward.Name = name;
			mForward.AssociateKey(key);
			InputManager.RegisterButton(mForward);
		}
		public void SetBackward(string name, Key key)
		{
			mBackwards.Name = name;
			mBackwards.AssociateKey(key);
			InputManager.RegisterButton(mBackwards);
		}
		public void SetStrafeRight(string name, Key key)
		{
			strafeRight.Name = name;
			strafeRight.AssociateKey(key);
			InputManager.RegisterButton(strafeRight);
		}
		public void SetStrafeLeft(string name, Key key)
		{
			strafeLeft.Name = name;
			strafeLeft.AssociateKey(key);
			InputManager.RegisterButton(strafeLeft);
		}
		public void SetRotateRight(string name, Key key)
		{
			rotateRight.Name = name;
			rotateRight.AssociateKey(key);
			InputManager.RegisterButton(rotateRight);
		}
		public void SetRotateLeft(string name, Key key)
		{
			rotateLeft.Name = name;
			rotateLeft.AssociateKey(key);
			InputManager.RegisterButton(rotateLeft);
		}

		#region action keys
		public void SetAttack(string name, Key key)
		{
			mainAttack.Name = name;
			mainAttack.AssociateKey(key);
			InputManager.RegisterButton(mainAttack);
		}

		public void SetAbility(string name, Key key)
		{
			shipAbility.Name = name;
			shipAbility.AssociateKey(key);
			InputManager.RegisterButton(shipAbility);
		}

		public void SetUseItem(string name, Key key)
		{
			item.Name = name;
			item.AssociateKey(key);
			InputManager.RegisterButton(item);
		}
		#endregion*/
	}

}
