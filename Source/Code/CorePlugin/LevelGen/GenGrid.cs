﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Resources;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices.WindowsRuntime;

namespace The_ESG
{

	public  class GenGrid
	{
		public static bool playerPlaced = false;
		bool bossPlaced = false;



		Cell[,] cellArray;

		public GenGrid(int numberOfCells, int sizeOfCells )
		{
			//List<Cell> cellList = new List<Cell>();
			 cellArray = new Cell[numberOfCells, numberOfCells];

			for (int x = 1; x < numberOfCells * sizeOfCells; x += sizeOfCells)
			{
				for (int y = 1; y < numberOfCells * sizeOfCells; y += sizeOfCells)
				{
					//cellList.Add( new Cell(x,y));
					cellArray[x / sizeOfCells, y / sizeOfCells] = new Cell(x, y);
						
				}

			}

		
		}

		//TODO code the setting of a spawn cell for the boss
		public void DefineSpawns(bool hasBoss, float percEnemySpawn, float percCargoSpawn, float percHazardSpawn, float percClean )
		{
			playerPlaced = false;
			//set the cell where the player will spawn
			int playerSpX = MathF.Rnd.Next(0,cellArray.GetUpperBound(0) - 1);
			int playerSpY = MathF.Rnd.Next(0, cellArray.GetUpperBound(1) -1);
			cellArray[playerSpX,playerSpY].setType(true, false, false, false, false, false);
			playerPlaced = true;

			//set the cell where the boss will spawn
			if (hasBoss)
			{
				//TODO find better way.
				int bossSpX = cellArray.GetUpperBound(0);
				int bossSpY = cellArray.GetUpperBound(1);
				cellArray[bossSpX, bossSpY].setType(false, true, false, false, false, false);
			//	bossPlaced = true;

			}
			else return;


			foreach(Cell cell in cellArray)
			{
				float roll = MathF.Rnd.NextFloat(0.0f,1.0f);

				if (!cell.typeSet)
				{
					//player, boss, enemy, hazard, cargo, clean
					if (roll <= percEnemySpawn) cell.setType(false, false, true, false, false, false);
					if (roll <= percHazardSpawn) cell.setType(false, false, false, true, false, false);
					if (roll <= percCargoSpawn) cell.setType(false, false, false, false, true, false);
					if (roll <= percCargoSpawn) cell.setType(false, false, false, false, false, true);

				}
				else return;


			}

		}

		public void Populate(Prefab player, Prefab eSpawner, Prefab hSpawner, Prefab bSpawner, Prefab cSpawner)
		{
			foreach(Cell cell in cellArray)
			{
				if (!playerPlaced)
				{
					var p = player.Instantiate(new Vector3(cell.worldX, cell.worldY, -200));
					Scene.Current.AddObject(p);
					playerPlaced = true;

					

				}

				if(cell.type == Cell.cellType.EnemySpawn)
				{
					var s = eSpawner.Instantiate((new Vector3(cell.worldX, cell.worldY, 0)));
					Scene.Current.AddObject(s);

				}

				 if (cell.type == Cell.cellType.Hazard)
				{
					var s = hSpawner.Instantiate((new Vector3(cell.worldX, cell.worldY, 0)));
					Scene.Current.AddObject(s);

				}

				 if (cell.type == Cell.cellType.CargoSpawn)
				{
					var s = cSpawner.Instantiate((new Vector3(cell.worldX, cell.worldY, 0)));
					Scene.Current.AddObject(s);

				}

				if (cell.type == Cell.cellType.BossSpawn)
				{
					var s = bSpawner.Instantiate((new Vector3(cell.worldX, cell.worldY, 0)));
					Scene.Current.AddObject(s);
					bossPlaced = true;
				}

				if (cell.type == Cell.cellType.Clean)
				{
					return;

				}

			}




		}



	}
}
