﻿using System;
using System.Collections.Generic;
using System.Linq;

using Duality;
using Duality.Resources;

namespace The_ESG
{
	/// <summary>
	/// This are the "tiles" where the game will place spawners etc, it has 2 coordinate values:
	/// <list type="">
	/// <item>The world coords atributed from the spawner.</item>
	/// <item>The cell coords which are relative to the size of each cell</item>
	/// </list>
	///
	/// For example, a tile might be in the coords (200,178) but have its cellCoords be (3,4), indicating it's the 3rd horizantal 
	/// and 4th vertical in the grid.
	/// </summary>
	public class Cell
	{
		public enum cellType { EnemySpawn, PlayerSpawn, CargoSpawn, BossSpawn, Hazard, Clean } 
		public cellType type { get;  set; }




		public int cellSize { get; set; } //240 to allow 7 32x entities lined up straight.


		public float worldX { get; set; }
		public float worldY { get; set; }
		 
		public int cellX { get; private set; }
		public int cellY { get; private set; }

		public bool typeSet { get; set; }

		Vector2 cellCoords;

		public Cell(float _x, float _y )
		{
			typeSet = false;

			cellSize = 240;

			worldX = _x;
			worldY = _y;

			cellX = (int)worldX / cellSize;
			cellY = (int)worldY / cellSize;

			cellCoords = new Vector2(cellX, cellY);





		}

		public cellType setType(bool playerSp, bool bossSp, bool enemySp, bool hazardSp, bool cargoSp, bool cleanSp)
		{

			if (playerSp) this.type = cellType.PlayerSpawn;
			if (bossSp) this.type = cellType.BossSpawn;
			if (enemySp) this.type = cellType.EnemySpawn;
			if (hazardSp) this.type = cellType.Hazard;
			if (cargoSp) this.type = cellType.CargoSpawn;
			if (cleanSp) this.type = cellType.Clean;

		
			this.typeSet = true;

			return this.type;
		}




	}
}
