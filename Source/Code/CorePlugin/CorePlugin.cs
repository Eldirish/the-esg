﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Duality;

namespace The_ESG
{
	/// <summary>
	/// Defines a Duality core plugin.
	/// </summary>
	public class The_ESGCorePlugin : CorePlugin
	{
		// Override methods here for global logic
	}
}
