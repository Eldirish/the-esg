﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Duality.Editor;

namespace The_ESG.Editor
{
	/// <summary>
	/// Defines a Duality editor plugin.
	/// </summary>
    public class The_ESGEditorPlugin : EditorPlugin
	{
		public override string Id
		{
			get { return "The_ESGEditorPlugin"; }
		}
	}
}
