﻿<root dataType="Struct" type="The_ESG.ShipStats" id="129723834">
  <_x003C_abiltyCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_abiltyCooldownModifier_x003E_k__BackingField>
  <_x003C_damageCooldown_x003E_k__BackingField dataType="Float">0</_x003C_damageCooldown_x003E_k__BackingField>
  <_x003C_damageModifier_x003E_k__BackingField dataType="Float">0</_x003C_damageModifier_x003E_k__BackingField>
  <_x003C_damageMultiplier_x003E_k__BackingField dataType="Float">1</_x003C_damageMultiplier_x003E_k__BackingField>
  <_x003C_deathPrefabRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
    <contentPath dataType="String">Data\Prefabs\VFX\BadGuyFlyDestroy.Prefab.res</contentPath>
  </_x003C_deathPrefabRef_x003E_k__BackingField>
  <_x003C_deathSound_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
    <contentPath dataType="String">Data\Audio\shothit.Sound.res</contentPath>
  </_x003C_deathSound_x003E_k__BackingField>
  <_x003C_friction_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
  <_x003C_linearSpeed_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
    <X dataType="Float">5</X>
    <Y dataType="Float">1.05</Y>
    <Z dataType="Float">0</Z>
  </_x003C_linearSpeed_x003E_k__BackingField>
  <_x003C_maxVelocity_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
  <_x003C_rotationSpeed_x003E_k__BackingField dataType="Float">0.05</_x003C_rotationSpeed_x003E_k__BackingField>
  <_x003C_scoreWorth_x003E_k__BackingField dataType="Int">2</_x003C_scoreWorth_x003E_k__BackingField>
  <_x003C_shipCurrentHealthPoints_x003E_k__BackingField dataType="Int">0</_x003C_shipCurrentHealthPoints_x003E_k__BackingField>
  <_x003C_shipMaxHealthPoints_x003E_k__BackingField dataType="Int">6</_x003C_shipMaxHealthPoints_x003E_k__BackingField>
  <_x003C_shipRamDamage_x003E_k__BackingField dataType="Int">10</_x003C_shipRamDamage_x003E_k__BackingField>
  <_x003C_shotCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_shotCooldownModifier_x003E_k__BackingField>
  <assetInfo />
</root>
<!-- XmlFormatterBase Document Separator -->
