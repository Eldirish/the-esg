﻿<root dataType="Struct" type="The_ESG.LevelList" id="129723834">
  <_x003C_Levels_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Duality.ContentRef`1[[Duality.Resources.Scene]]]]" id="427169525">
    <_items dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Scene]][]" id="1100841590" length="4">
      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Scene]]">
        <contentPath dataType="String">Data\Scenes\Sc_GameOver.Scene.res</contentPath>
      </item>
      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Scene]]">
        <contentPath dataType="String">Data\Scenes\Sc_Levels.Scene.res</contentPath>
      </item>
      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Scene]]">
        <contentPath dataType="String">Data\Scenes\Sc_FreeRoamLevels.Scene.res</contentPath>
      </item>
    </_items>
    <_size dataType="Int">3</_size>
    <_version dataType="Int">8</_version>
  </_x003C_Levels_x003E_k__BackingField>
  <_x003C_Stages_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.Dictionary`2[[Duality.ContentRef`1[[Duality.Resources.Scene]]],[Duality.ContentRef`1[[The_ESG.StageProperties]]]]" id="2035693768" surrogate="true">
    <header />
    <body>
      <keys dataType="Array" type="System.Object[]" id="2696347487">
        <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Scene]]">
          <contentPath dataType="String">Data\Scenes\Sc_GameOver.Scene.res</contentPath>
        </item>
        <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Scene]]">
          <contentPath dataType="String">Data\Scenes\Sc_Levels.Scene.res</contentPath>
        </item>
        <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Scene]]">
          <contentPath dataType="String">Data\Scenes\Sc_FreeRoamLevels.Scene.res</contentPath>
        </item>
      </keys>
      <values dataType="Array" type="System.Object[]" id="847900448">
        <item dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StageProperties]]">
          <contentPath dataType="String">Data\Scripts_Resources\Data\Stages\GameOver.StageProperties.res</contentPath>
        </item>
        <item dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StageProperties]]">
          <contentPath dataType="String">Data\Scripts_Resources\Data\Stages\Stage1V.StageProperties.res</contentPath>
        </item>
        <item dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StageProperties]]">
          <contentPath dataType="String">Data\Scripts_Resources\Data\Stages\Stage1F.StageProperties.res</contentPath>
        </item>
      </values>
    </body>
  </_x003C_Stages_x003E_k__BackingField>
  <assetInfo />
</root>
<!-- XmlFormatterBase Document Separator -->
