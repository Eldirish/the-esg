﻿<root dataType="Struct" type="The_ESG.StageProperties" id="129723834">
  <_x003C_BgMusic_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
    <contentPath dataType="String">Data\Audio\stage1bg.Sound.res</contentPath>
  </_x003C_BgMusic_x003E_k__BackingField>
  <_x003C_cellQuantity_x003E_k__BackingField dataType="Int">40</_x003C_cellQuantity_x003E_k__BackingField>
  <_x003C_maxSpawners_x003E_k__BackingField dataType="Int">40</_x003C_maxSpawners_x003E_k__BackingField>
  <_x003C_maxSquadSpawns_x003E_k__BackingField dataType="Int">20</_x003C_maxSquadSpawns_x003E_k__BackingField>
  <_x003C_spawnIntervals_x003E_k__BackingField dataType="Float">50</_x003C_spawnIntervals_x003E_k__BackingField>
  <_x003C_squadList_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Duality.ContentRef`1[[Duality.Resources.Prefab]]]]" id="427169525">
    <_items dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Prefab]][]" id="1100841590" length="8">
      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
        <contentPath dataType="String">Data\Prefabs\Squads\Squad4Fly.Prefab.res</contentPath>
      </item>
      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
        <contentPath dataType="String">Data\Prefabs\Squads\Squad4Fly.Prefab.res</contentPath>
      </item>
      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
        <contentPath dataType="String">Data\Prefabs\Squads\Squad5FlyTriangle.Prefab.res</contentPath>
      </item>
      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
        <contentPath dataType="String">Data\Prefabs\Squads\Squad5FlyTriangle.Prefab.res</contentPath>
      </item>
      <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
        <contentPath dataType="String">Data\Prefabs\Squads\Squad6FlyLine.Prefab.res</contentPath>
      </item>
    </_items>
    <_size dataType="Int">5</_size>
    <_version dataType="Int">13</_version>
  </_x003C_squadList_x003E_k__BackingField>
  <_x003C_stageIndex_x003E_k__BackingField dataType="Int">1</_x003C_stageIndex_x003E_k__BackingField>
  <_x003C_stageName_x003E_k__BackingField dataType="String">Black Hole 1</_x003C_stageName_x003E_k__BackingField>
  <_x003C_Vertical_x003E_k__BackingField dataType="Bool">true</_x003C_Vertical_x003E_k__BackingField>
  <assetInfo />
</root>
<!-- XmlFormatterBase Document Separator -->
