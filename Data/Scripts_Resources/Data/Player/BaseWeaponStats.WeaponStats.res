﻿<root dataType="Struct" type="The_ESG.WeaponStats" id="129723834">
  <_x003C_attackCooldown_x003E_k__BackingField dataType="Float">10</_x003C_attackCooldown_x003E_k__BackingField>
  <_x003C_attackCooldownDefault_x003E_k__BackingField dataType="Float">10</_x003C_attackCooldownDefault_x003E_k__BackingField>
  <_x003C_bulletCritChance_x003E_k__BackingField dataType="Float">0.2</_x003C_bulletCritChance_x003E_k__BackingField>
  <_x003C_bulletDamage_x003E_k__BackingField dataType="Int">5</_x003C_bulletDamage_x003E_k__BackingField>
  <_x003C_bulletRange_x003E_k__BackingField dataType="Float">70</_x003C_bulletRange_x003E_k__BackingField>
  <_x003C_bulletSpeed_x003E_k__BackingField dataType="Float">13</_x003C_bulletSpeed_x003E_k__BackingField>
  <assetInfo />
</root>
<!-- XmlFormatterBase Document Separator -->
