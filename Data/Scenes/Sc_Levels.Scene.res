﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="3026331258">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2692286820">
        <_items dataType="Array" type="Duality.Component[]" id="418614212">
          <item dataType="Struct" type="Duality.Components.Transform" id="1091678894">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3026331258</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2733766639">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3026331258</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.ActionInputCheck" id="3659035559">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3026331258</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.MovementInputCheck" id="502850072">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3026331258</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.InputMaster" id="3849802971">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3026331258</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1794140486">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3026331258</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="1844732727">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3026331258</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.PlayerBehaviour" id="4252852847">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3026331258</gameobj>
          </item>
        </_items>
        <_size dataType="Int">8</_size>
        <_version dataType="Int">8</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2344534550" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="628847662">
            <item dataType="Type" id="2195067728" value="Duality.Components.Transform" />
            <item dataType="Type" id="1019940718" value="Duality.Components.Renderers.AnimSpriteRenderer" />
            <item dataType="Type" id="3505548076" value="The_ESG.ActionInputCheck" />
            <item dataType="Type" id="1478180626" value="The_ESG.MovementInputCheck" />
            <item dataType="Type" id="947645704" value="The_ESG.InputMaster" />
            <item dataType="Type" id="1338840166" value="Duality.Components.Physics.RigidBody" />
            <item dataType="Type" id="2892965572" value="The_ESG.Tag" />
            <item dataType="Type" id="2160256010" value="The_ESG.PlayerBehaviour" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1858444490">
            <item dataType="ObjectRef">1091678894</item>
            <item dataType="ObjectRef">2733766639</item>
            <item dataType="ObjectRef">3659035559</item>
            <item dataType="ObjectRef">502850072</item>
            <item dataType="ObjectRef">3849802971</item>
            <item dataType="ObjectRef">1794140486</item>
            <item dataType="ObjectRef">1844732727</item>
            <item dataType="ObjectRef">4252852847</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1091678894</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3954075038">/mOO9uz/KUWrHr349OPNrQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">PlayerShip</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3373851232">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="212394504">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="2029667692" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4089438632">
                <_items dataType="Array" type="System.Int32[]" id="3478724268"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2195067728</componentType>
              <prop dataType="MemberInfo" id="2219847582" value="P:Duality.Components.Transform:RelativePos" />
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">3.81469727E-06</X>
                <Y dataType="Float">160.8393</Y>
                <Z dataType="Float">-200</Z>
              </val>
            </item>
          </_items>
          <_size dataType="Int">1</_size>
          <_version dataType="Int">473</_version>
        </changes>
        <obj dataType="ObjectRef">3026331258</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\PlayerShip.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="4191841599">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="693258429">
        <_items dataType="Array" type="Duality.Component[]" id="408546854" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="2257189235">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4191841599</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="3010243068">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4191841599</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="543539107">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4191841599</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2573608120" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="71556567">
            <item dataType="ObjectRef">2195067728</item>
            <item dataType="ObjectRef">2892965572</item>
            <item dataType="Type" id="384930318" value="The_ESG.SpawnerBehaviour" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="4062989248">
            <item dataType="ObjectRef">2257189235</item>
            <item dataType="ObjectRef">3010243068</item>
            <item dataType="ObjectRef">543539107</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2257189235</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2999163381">QS8Zy8PHEkKkqPVsCQkPbA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">EnemySpawner</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1236467863">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1708480468">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="2521394404" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2297764040">
                <_items dataType="Array" type="System.Int32[]" id="3562650220"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2195067728</componentType>
              <prop dataType="MemberInfo" id="1511252702" value="P:Duality.Component:ActiveSingle" />
              <val dataType="Bool">true</val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2166074676">
                <_items dataType="Array" type="System.Int32[]" id="828466504"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2195067728</componentType>
              <prop dataType="ObjectRef">2219847582</prop>
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">-6.09259272</X>
                <Y dataType="Float">-433.7963</Y>
                <Z dataType="Float">0</Z>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1661214498">
                <_items dataType="Array" type="System.Int32[]" id="275589198"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType />
              <prop dataType="MemberInfo" id="2716824192" value="P:Duality.GameObject:Name" />
              <val dataType="String">EnemySpawner</val>
            </item>
          </_items>
          <_size dataType="Int">3</_size>
          <_version dataType="Int">853</_version>
        </changes>
        <obj dataType="ObjectRef">4191841599</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Spawners\EnemySpawner.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="613675579">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3180991305">
        <_items dataType="Array" type="Duality.Component[]" id="1582594958" length="4">
          <item dataType="Struct" type="The_ESG.SpawnManager" id="680061998">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">613675579</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.GameManager" id="2767298731">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">613675579</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="960228160" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="4267236611">
            <item dataType="Type" id="2348171558" value="The_ESG.SpawnManager" />
            <item dataType="Type" id="2892778170" value="The_ESG.GameManager" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="3207915448">
            <item dataType="ObjectRef">680061998</item>
            <item dataType="ObjectRef">2767298731</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3049188905">3bct2r4ff0W86uGtR04qmQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">LevelManager</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3773690219">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2722125876">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="348869796" length="8">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1555687368">
                <_items dataType="Array" type="System.Int32[]" id="4243534444"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2892778170</componentType>
              <prop dataType="MemberInfo" id="3283311326" value="P:The_ESG.GameManager:stateRef" />
              <val dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3237942836">
                <_items dataType="ObjectRef">275589198</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2892778170</componentType>
              <prop dataType="MemberInfo" id="4062274338" value="P:The_ESG.GameManager:bossSpawnTemplate" />
              <val dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                <contentPath dataType="String">Data\Prefabs\Spawners\BossSpawner.Prefab.res</contentPath>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="237744512">
                <_items dataType="ObjectRef">275589198</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2892778170</componentType>
              <prop dataType="MemberInfo" id="1153160070" value="P:The_ESG.GameManager:cargoSpawnTemplate" />
              <val dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                <contentPath dataType="String">Data\Prefabs\Spawners\CargoSpawner.Prefab.res</contentPath>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3795521708">
                <_items dataType="ObjectRef">275589198</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2892778170</componentType>
              <prop dataType="MemberInfo" id="1116825354" value="P:The_ESG.GameManager:enemySpawnTemplate" />
              <val dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                <contentPath dataType="String">Data\Prefabs\Spawners\EnemySpawner.Prefab.res</contentPath>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="904019064">
                <_items dataType="ObjectRef">275589198</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2892778170</componentType>
              <prop dataType="MemberInfo" id="2690235438" value="P:The_ESG.GameManager:hazardSpawnTemplate" />
              <val dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                <contentPath dataType="String">Data\Prefabs\Spawners\HazardSpawner.Prefab.res</contentPath>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3251487076">
                <_items dataType="ObjectRef">275589198</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2892778170</componentType>
              <prop dataType="MemberInfo" id="3743049138" value="P:The_ESG.GameManager:playerTemplate" />
              <val dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                <contentPath dataType="String">Data\Prefabs\PlayerShip.Prefab.res</contentPath>
              </val>
            </item>
          </_items>
          <_size dataType="Int">6</_size>
          <_version dataType="Int">10</_version>
        </changes>
        <obj dataType="ObjectRef">613675579</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\LevelManager.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2816164109">
      <active dataType="Bool">true</active>
      <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="2832357311">
        <_items dataType="Array" type="Duality.GameObject[]" id="580627886" length="4">
          <item dataType="Struct" type="Duality.GameObject" id="3473009256">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3830541812">
              <_items dataType="Array" type="Duality.Component[]" id="1172042916" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="1538356892">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">3473009256</gameobj>
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="920670782">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">3473009256</gameobj>
                </item>
                <item dataType="Struct" type="The_ESG.Tutorial" id="4041238795">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">3473009256</gameobj>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">7</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3332621046" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="1512967646">
                  <item dataType="ObjectRef">2195067728</item>
                  <item dataType="Type" id="1700685072" value="Duality.Components.Renderers.TextRenderer" />
                  <item dataType="Type" id="4134211822" value="The_ESG.Tutorial" />
                </keys>
                <values dataType="Array" type="System.Object[]" id="1594105098">
                  <item dataType="ObjectRef">1538356892</item>
                  <item dataType="ObjectRef">920670782</item>
                  <item dataType="ObjectRef">4041238795</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">1538356892</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="215625262">vqc7yjrbWk2EEN4ql1CNjA==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Info text</name>
            <parent dataType="ObjectRef">2816164109</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="4021675183">
            <active dataType="Bool">true</active>
            <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="2506275743">
              <_items dataType="Array" type="Duality.GameObject[]" id="1751316846" length="4">
                <item dataType="Struct" type="Duality.GameObject" id="172733585">
                  <active dataType="Bool">true</active>
                  <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="1201575969">
                    <_items dataType="Array" type="Duality.GameObject[]" id="2812344942" length="4">
                      <item dataType="Struct" type="Duality.GameObject" id="3694630143">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1978057295">
                          <_items dataType="Array" type="Duality.Component[]" id="3442137646" length="4">
                            <item dataType="Struct" type="Duality.Components.Transform" id="1759977779">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3694630143</gameobj>
                            </item>
                            <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="1142291669">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3694630143</gameobj>
                            </item>
                            <item dataType="Struct" type="The_ESG.ShowHP" id="3422617165">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3694630143</gameobj>
                            </item>
                          </_items>
                          <_size dataType="Int">3</_size>
                          <_version dataType="Int">3</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="324353120" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="238648165">
                              <item dataType="ObjectRef">2195067728</item>
                              <item dataType="ObjectRef">1700685072</item>
                              <item dataType="Type" id="3018760086" value="The_ESG.ShowHP" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="2801605224">
                              <item dataType="ObjectRef">1759977779</item>
                              <item dataType="ObjectRef">1142291669</item>
                              <item dataType="ObjectRef">3422617165</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform dataType="ObjectRef">1759977779</compTransform>
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="4263626927">ptUzOgQ51EGBpd+aZy2MjQ==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">Text</name>
                        <parent dataType="ObjectRef">172733585</parent>
                        <prefabLink />
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">1</_version>
                  </children>
                  <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2780804384">
                    <_items dataType="Array" type="Duality.Component[]" id="3286490667" length="4">
                      <item dataType="Struct" type="Duality.Components.Transform" id="2533048517">
                        <active dataType="Bool">true</active>
                        <gameobj dataType="ObjectRef">172733585</gameobj>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="4175136262">
                        <active dataType="Bool">true</active>
                        <gameobj dataType="ObjectRef">172733585</gameobj>
                      </item>
                    </_items>
                    <_size dataType="Int">2</_size>
                    <_version dataType="Int">4</_version>
                  </compList>
                  <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1821744563" surrogate="true">
                    <header />
                    <body>
                      <keys dataType="Array" type="System.Object[]" id="4047762340">
                        <item dataType="ObjectRef">2195067728</item>
                        <item dataType="ObjectRef">1019940718</item>
                      </keys>
                      <values dataType="Array" type="System.Object[]" id="2998255382">
                        <item dataType="ObjectRef">2533048517</item>
                        <item dataType="ObjectRef">4175136262</item>
                      </values>
                    </body>
                  </compMap>
                  <compTransform dataType="ObjectRef">2533048517</compTransform>
                  <identifier dataType="Struct" type="System.Guid" surrogate="true">
                    <header>
                      <data dataType="Array" type="System.Byte[]" id="3449754272">6vWXbdy1x0GIZ9LoKYoqvA==</data>
                    </header>
                    <body />
                  </identifier>
                  <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                  <name dataType="String">HP</name>
                  <parent dataType="ObjectRef">4021675183</parent>
                  <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2339863030">
                    <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="3318114173">
                      <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="4128193318" length="8">
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1541204892">
                            <_items dataType="Array" type="System.Int32[]" id="2404981188"></_items>
                            <_size dataType="Int">0</_size>
                            <_version dataType="Int">1</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">2195067728</componentType>
                          <prop dataType="MemberInfo" id="3129066518" value="P:Duality.Components.Transform:RelativeScale" />
                          <val dataType="Float">0.299232155</val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2638376456">
                            <_items dataType="Array" type="System.Int32[]" id="241534616">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">2195067728</componentType>
                          <prop dataType="ObjectRef">3129066518</prop>
                          <val dataType="Float">6.68377352</val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="744731570">
                            <_items dataType="Array" type="System.Int32[]" id="3249297546">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">1700685072</componentType>
                          <prop dataType="MemberInfo" id="2538417716" value="P:Duality.Components.Renderers.TextRenderer:Text" />
                          <val dataType="Struct" type="Duality.Drawing.FormattedText" id="3879716238">
                            <flowAreas />
                            <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="2541876238">
                              <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                                <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                              </item>
                            </fonts>
                            <icons />
                            <lineAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                            <maxHeight dataType="Int">0</maxHeight>
                            <maxWidth dataType="Int">0</maxWidth>
                            <sourceText dataType="String">2</sourceText>
                            <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                          </val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3635483072">
                            <_items dataType="Array" type="System.Int32[]" id="960525072"></_items>
                            <_size dataType="Int">0</_size>
                            <_version dataType="Int">1</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">2195067728</componentType>
                          <prop dataType="ObjectRef">2219847582</prop>
                          <val dataType="Struct" type="Duality.Vector3">
                            <X dataType="Float">0.833333</X>
                            <Y dataType="Float">255.833313</Y>
                            <Z dataType="Float">0</Z>
                          </val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="837715722">
                            <_items dataType="Array" type="System.Int32[]" id="4177131266">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">2195067728</componentType>
                          <prop dataType="ObjectRef">2219847582</prop>
                          <val dataType="Struct" type="Duality.Vector3">
                            <X dataType="Float">-3.507018</X>
                            <Y dataType="Float">-1.76907849</Y>
                            <Z dataType="Float">0</Z>
                          </val>
                        </item>
                      </_items>
                      <_size dataType="Int">5</_size>
                      <_version dataType="Int">293</_version>
                    </changes>
                    <obj dataType="ObjectRef">172733585</obj>
                    <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Graphics\Sprites\UI\HP.Prefab.res</contentPath>
                    </prefab>
                  </prefabLink>
                </item>
                <item dataType="Struct" type="Duality.GameObject" id="3297887951">
                  <active dataType="Bool">true</active>
                  <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="3158685183">
                    <_items dataType="Array" type="Duality.GameObject[]" id="2169797422" length="4">
                      <item dataType="Struct" type="Duality.GameObject" id="4184254650">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3713161054">
                          <_items dataType="Array" type="Duality.Component[]" id="2565601040" length="4">
                            <item dataType="Struct" type="Duality.Components.Transform" id="2249602286">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">4184254650</gameobj>
                            </item>
                            <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="1631916176">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">4184254650</gameobj>
                            </item>
                            <item dataType="Struct" type="The_ESG.ShowScore" id="3123138786">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">4184254650</gameobj>
                            </item>
                          </_items>
                          <_size dataType="Int">3</_size>
                          <_version dataType="Int">3</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="629521162" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="1069475964">
                              <item dataType="ObjectRef">2195067728</item>
                              <item dataType="ObjectRef">1700685072</item>
                              <item dataType="Type" id="3776526404" value="The_ESG.ShowScore" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="1562135702">
                              <item dataType="ObjectRef">2249602286</item>
                              <item dataType="ObjectRef">1631916176</item>
                              <item dataType="ObjectRef">3123138786</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform dataType="ObjectRef">2249602286</compTransform>
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="178662952">8eNIGC4DKEuo+OJIDF62Yw==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">Text</name>
                        <parent dataType="ObjectRef">3297887951</parent>
                        <prefabLink />
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">1</_version>
                  </children>
                  <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1398645088">
                    <_items dataType="Array" type="Duality.Component[]" id="867670581" length="4">
                      <item dataType="Struct" type="Duality.Components.Transform" id="1363235587">
                        <active dataType="Bool">true</active>
                        <gameobj dataType="ObjectRef">3297887951</gameobj>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3005323332">
                        <active dataType="Bool">true</active>
                        <gameobj dataType="ObjectRef">3297887951</gameobj>
                      </item>
                    </_items>
                    <_size dataType="Int">2</_size>
                    <_version dataType="Int">8</_version>
                  </compList>
                  <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="30875565" surrogate="true">
                    <header />
                    <body>
                      <keys dataType="Array" type="System.Object[]" id="2039011364">
                        <item dataType="ObjectRef">2195067728</item>
                        <item dataType="ObjectRef">1019940718</item>
                      </keys>
                      <values dataType="Array" type="System.Object[]" id="2203683094">
                        <item dataType="ObjectRef">1363235587</item>
                        <item dataType="ObjectRef">3005323332</item>
                      </values>
                    </body>
                  </compMap>
                  <compTransform dataType="ObjectRef">1363235587</compTransform>
                  <identifier dataType="Struct" type="System.Guid" surrogate="true">
                    <header>
                      <data dataType="Array" type="System.Byte[]" id="3636085280">WIAK15vr5ka0evhwJUo2cQ==</data>
                    </header>
                    <body />
                  </identifier>
                  <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                  <name dataType="String">Score</name>
                  <parent dataType="ObjectRef">4021675183</parent>
                  <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="512688246">
                    <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1568914339">
                      <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="4131480422" length="4">
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1614247708">
                            <_items dataType="Array" type="System.Int32[]" id="2070106052">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">2195067728</componentType>
                          <prop dataType="ObjectRef">3129066518</prop>
                          <val dataType="Float">5.52157164</val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1115305494">
                            <_items dataType="Array" type="System.Int32[]" id="600236470">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">1700685072</componentType>
                          <prop dataType="ObjectRef">2538417716</prop>
                          <val dataType="Struct" type="Duality.Drawing.FormattedText" id="2986110088">
                            <flowAreas />
                            <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="4233514648">
                              <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                                <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                              </item>
                            </fonts>
                            <icons />
                            <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                            <maxHeight dataType="Int">0</maxHeight>
                            <maxWidth dataType="Int">0</maxWidth>
                            <sourceText dataType="String">2</sourceText>
                            <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                          </val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="377134002">
                            <_items dataType="Array" type="System.Int32[]" id="1605621258">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">2195067728</componentType>
                          <prop dataType="ObjectRef">2219847582</prop>
                          <val dataType="Struct" type="Duality.Vector3">
                            <X dataType="Float">-0.000101725258</X>
                            <Y dataType="Float">16.6630039</Y>
                            <Z dataType="Float">0</Z>
                          </val>
                        </item>
                      </_items>
                      <_size dataType="Int">3</_size>
                      <_version dataType="Int">215</_version>
                    </changes>
                    <obj dataType="ObjectRef">3297887951</obj>
                    <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Graphics\Sprites\UI\Score.Prefab.res</contentPath>
                    </prefab>
                  </prefabLink>
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </children>
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1150111776">
              <_items dataType="Array" type="Duality.Component[]" id="515026453" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="2087022819">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">4021675183</gameobj>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2105775885" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="1009185700">
                  <item dataType="ObjectRef">2195067728</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="2679750422">
                  <item dataType="ObjectRef">2087022819</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">2087022819</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="3160806048">66z6hE/6EUi2GpHRhBFoVQ==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">UI</name>
            <parent dataType="ObjectRef">2816164109</parent>
            <prefabLink />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">8</_version>
      </children>
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="769068512">
        <_items dataType="Array" type="Duality.Component[]" id="2006157941" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="881511745">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2816164109</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="3353439916">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2816164109</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">6</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="685540717" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3308652324">
            <item dataType="ObjectRef">2195067728</item>
            <item dataType="Type" id="671754948" value="Duality.Components.Camera" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="2471008534">
            <item dataType="ObjectRef">881511745</item>
            <item dataType="ObjectRef">3353439916</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">881511745</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2651535648">0QndFwZ4xEiYD8jM8f5nGA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Camera</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2284883830">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2378142051">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1645545190" length="8">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3819749660">
                <_items dataType="Array" type="System.Int32[]" id="1063055300">1, 1, 0, 0</_items>
                <_size dataType="Int">2</_size>
                <_version dataType="Int">3</_version>
              </childIndex>
              <componentType />
              <prop dataType="MemberInfo" id="3151910422" value="P:Duality.GameObject:PrefabLink" />
              <val dataType="Struct" type="Duality.Resources.PrefabLink" id="3889867400">
                <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1872715416">
                  <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1979840044" length="4">
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3702822440">
                        <_items dataType="Array" type="System.Int32[]" id="2894819756">0, 0, 0, 0</_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">2</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">2195067728</componentType>
                      <prop dataType="ObjectRef">3129066518</prop>
                      <val dataType="Float">5.52157164</val>
                    </item>
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1691676318">
                        <_items dataType="Array" type="System.Int32[]" id="2253217770">0, 0, 0, 0</_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">2</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">1700685072</componentType>
                      <prop dataType="ObjectRef">2538417716</prop>
                      <val dataType="Struct" type="Duality.Drawing.FormattedText" id="3310376724">
                        <flowAreas />
                        <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="3497341512">
                          <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                            <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                          </item>
                        </fonts>
                        <icons />
                        <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                        <maxHeight dataType="Int">0</maxHeight>
                        <maxWidth dataType="Int">0</maxWidth>
                        <sourceText dataType="String">2</sourceText>
                        <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                      </val>
                    </item>
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3817602082">
                        <_items dataType="Array" type="System.Int32[]" id="1108496558">0, 0, 0, 0</_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">2</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">2195067728</componentType>
                      <prop dataType="ObjectRef">2219847582</prop>
                      <val dataType="Struct" type="Duality.Vector3">
                        <X dataType="Float">-0.000101725258</X>
                        <Y dataType="Float">16.6630039</Y>
                        <Z dataType="Float">0</Z>
                      </val>
                    </item>
                  </_items>
                  <_size dataType="Int">3</_size>
                  <_version dataType="Int">215</_version>
                </changes>
                <obj dataType="ObjectRef">3297887951</obj>
                <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\UI\Score.Prefab.res</contentPath>
                </prefab>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="8768434">
                <_items dataType="ObjectRef">3478724268</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">671754948</componentType>
              <prop dataType="MemberInfo" id="4052603828" value="P:Duality.Components.Camera:VisibilityMask" />
              <val dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="634390670">
                <_items dataType="ObjectRef">3478724268</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2195067728</componentType>
              <prop dataType="MemberInfo" id="4021274688" value="P:Duality.Components.Transform:RelativeAngle" />
              <val dataType="Float">0</val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1462759434">
                <_items dataType="Array" type="System.Int32[]" id="1879731842">1, 0, 0, 0</_items>
                <_size dataType="Int">2</_size>
                <_version dataType="Int">3</_version>
              </childIndex>
              <componentType />
              <prop dataType="ObjectRef">3151910422</prop>
              <val dataType="Struct" type="Duality.Resources.PrefabLink" id="3679984908">
                <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2417934292">
                  <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1464405220" length="8">
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2716981448">
                        <_items dataType="Array" type="System.Int32[]" id="64895596"></_items>
                        <_size dataType="Int">0</_size>
                        <_version dataType="Int">1</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">2195067728</componentType>
                      <prop dataType="ObjectRef">3129066518</prop>
                      <val dataType="Float">0.299232155</val>
                    </item>
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1398366942">
                        <_items dataType="Array" type="System.Int32[]" id="3974848394">0, 0, 0, 0</_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">2</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">2195067728</componentType>
                      <prop dataType="ObjectRef">3129066518</prop>
                      <val dataType="Float">6.68377352</val>
                    </item>
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3326143796">
                        <_items dataType="Array" type="System.Int32[]" id="3294913864">0, 0, 0, 0</_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">2</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">1700685072</componentType>
                      <prop dataType="ObjectRef">2538417716</prop>
                      <val dataType="Struct" type="Duality.Drawing.FormattedText" id="875241250">
                        <flowAreas />
                        <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="4025706574">
                          <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                            <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                          </item>
                        </fonts>
                        <icons />
                        <lineAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                        <maxHeight dataType="Int">0</maxHeight>
                        <maxWidth dataType="Int">0</maxWidth>
                        <sourceText dataType="String">2</sourceText>
                        <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                      </val>
                    </item>
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2720510592">
                        <_items dataType="Array" type="System.Int32[]" id="1607394420"></_items>
                        <_size dataType="Int">0</_size>
                        <_version dataType="Int">1</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">2195067728</componentType>
                      <prop dataType="ObjectRef">2219847582</prop>
                      <val dataType="Struct" type="Duality.Vector3">
                        <X dataType="Float">0.833333</X>
                        <Y dataType="Float">255.833313</Y>
                        <Z dataType="Float">0</Z>
                      </val>
                    </item>
                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="907562886">
                        <_items dataType="Array" type="System.Int32[]" id="1915778722">0, 0, 0, 0</_items>
                        <_size dataType="Int">1</_size>
                        <_version dataType="Int">2</_version>
                      </childIndex>
                      <componentType dataType="ObjectRef">2195067728</componentType>
                      <prop dataType="ObjectRef">2219847582</prop>
                      <val dataType="Struct" type="Duality.Vector3">
                        <X dataType="Float">-3.507018</X>
                        <Y dataType="Float">-1.76907849</Y>
                        <Z dataType="Float">0</Z>
                      </val>
                    </item>
                  </_items>
                  <_size dataType="Int">5</_size>
                  <_version dataType="Int">293</_version>
                </changes>
                <obj dataType="ObjectRef">172733585</obj>
                <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\UI\HP.Prefab.res</contentPath>
                </prefab>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="830019814">
                <_items dataType="Array" type="System.Int32[]" id="3741449830">0, 0, 0, 0</_items>
                <_size dataType="Int">1</_size>
                <_version dataType="Int">2</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2195067728</componentType>
              <prop dataType="ObjectRef">2219847582</prop>
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">-6.982048</X>
                <Y dataType="Float">-241.726288</Y>
                <Z dataType="Float">580</Z>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3058397816">
                <_items dataType="ObjectRef">3478724268</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">671754948</componentType>
              <prop dataType="MemberInfo" id="146235650" value="P:Duality.Components.Camera:FocusDist" />
              <val dataType="Float">600</val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="552692900">
                <_items dataType="ObjectRef">828466504</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">2195067728</componentType>
              <prop dataType="ObjectRef">2219847582</prop>
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">0</X>
                <Y dataType="Float">0</Y>
                <Z dataType="Float">-600</Z>
              </val>
            </item>
          </_items>
          <_size dataType="Int">7</_size>
          <_version dataType="Int">10333</_version>
        </changes>
        <obj dataType="ObjectRef">2816164109</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Cameras\MainCam.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3784219187">
      <active dataType="Bool">true</active>
      <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="56010753">
        <_items dataType="Array" type="Duality.GameObject[]" id="2114115374" length="4">
          <item dataType="Struct" type="Duality.GameObject" id="2944414077">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2145572221">
              <_items dataType="Array" type="Duality.Component[]" id="1013809958" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="1009761713">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">2944414077</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">-50</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">-50</Z>
                  </posAbs>
                  <scale dataType="Float">2</scale>
                  <scaleAbs dataType="Float">2</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="291613349">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">2944414077</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="WrapBoth" value="3" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Graphics\BGs\faintstars.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="The_ESG.Scroll" id="1274097159">
                  <_x003C_scrollSpeed_x003E_k__BackingField dataType="Float">8</_x003C_scrollSpeed_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <ayy dataType="Float">0</ayy>
                  <cam dataType="ObjectRef">3353439916</cam>
                  <gameobj dataType="ObjectRef">2944414077</gameobj>
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">-300</Z>
                  </pos>
                  <rekt dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rekt>
                  <sprite dataType="ObjectRef">291613349</sprite>
                  <trans dataType="ObjectRef">1009761713</trans>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1912073656" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3915751959">
                  <item dataType="ObjectRef">2195067728</item>
                  <item dataType="Type" id="2391989006" value="Duality.Components.Renderers.SpriteRenderer" />
                  <item dataType="Type" id="1145733450" value="The_ESG.Scroll" />
                </keys>
                <values dataType="Array" type="System.Object[]" id="2954441920">
                  <item dataType="ObjectRef">1009761713</item>
                  <item dataType="ObjectRef">291613349</item>
                  <item dataType="ObjectRef">1274097159</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">1009761713</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2925297589">C3FVatXNT0Cs3gggNS5tOQ==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">BgLightstar</name>
            <parent dataType="ObjectRef">3784219187</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="1153122337">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3841876497">
              <_items dataType="Array" type="Duality.Component[]" id="718183662" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="3513437269">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">1153122337</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-500</Y>
                    <Z dataType="Float">-50</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-500</Y>
                    <Z dataType="Float">-50</Z>
                  </posAbs>
                  <scale dataType="Float">2</scale>
                  <scaleAbs dataType="Float">2</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="2795288905">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">255</B>
                    <G dataType="Byte">255</G>
                    <R dataType="Byte">255</R>
                  </colorTint>
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">1153122337</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="WrapBoth" value="3" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Graphics\BGs\faintstars.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="The_ESG.Scroll" id="3777772715">
                  <_x003C_scrollSpeed_x003E_k__BackingField dataType="Float">8</_x003C_scrollSpeed_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <ayy dataType="Float">0</ayy>
                  <cam dataType="ObjectRef">3353439916</cam>
                  <gameobj dataType="ObjectRef">1153122337</gameobj>
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-700</Y>
                    <Z dataType="Float">-300</Z>
                  </pos>
                  <rekt dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rekt>
                  <sprite dataType="ObjectRef">2795288905</sprite>
                  <trans dataType="ObjectRef">3513437269</trans>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3026932128" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="428914619">
                  <item dataType="ObjectRef">2195067728</item>
                  <item dataType="ObjectRef">2391989006</item>
                  <item dataType="ObjectRef">1145733450</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="2690443304">
                  <item dataType="ObjectRef">3513437269</item>
                  <item dataType="ObjectRef">2795288905</item>
                  <item dataType="ObjectRef">3777772715</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3513437269</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2905918001">DAbl8V5DPEaFzPa5BMWXEQ==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">BgLightstar2</name>
            <parent dataType="ObjectRef">3784219187</parent>
            <prefabLink />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </children>
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3042176352">
        <_items dataType="Array" type="Duality.Component[]" id="2486387659" length="0" />
        <_size dataType="Int">0</_size>
        <_version dataType="Int">0</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2818873939" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="786271268" length="0" />
          <values dataType="Array" type="System.Object[]" id="3834091798" length="0" />
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3805986336">k9rBcU+Xt0C06wfKO+5WFw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BgLightstar</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3311421608">
      <active dataType="Bool">true</active>
      <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="2466590830">
        <_items dataType="Array" type="Duality.GameObject[]" id="2569098832" length="4">
          <item dataType="Struct" type="Duality.GameObject" id="3516702904">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2111088776">
              <_items dataType="Array" type="Duality.Component[]" id="3483903852" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="1582050540">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3516702904</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">-30</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">0</Y>
                    <Z dataType="Float">-30</Z>
                  </posAbs>
                  <scale dataType="Float">1.5</scale>
                  <scaleAbs dataType="Float">1.5</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="863902176">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">93</B>
                    <G dataType="Byte">132</G>
                    <R dataType="Byte">208</R>
                  </colorTint>
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">3516702904</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Graphics\BGs\medium stars.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="The_ESG.Scroll" id="1846385986">
                  <_x003C_scrollSpeed_x003E_k__BackingField dataType="Float">5</_x003C_scrollSpeed_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <ayy dataType="Float">0</ayy>
                  <cam dataType="ObjectRef">3353439916</cam>
                  <gameobj dataType="ObjectRef">3516702904</gameobj>
                  <pos dataType="Struct" type="Duality.Vector3" />
                  <rekt dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rekt>
                  <sprite dataType="ObjectRef">863902176</sprite>
                  <trans dataType="ObjectRef">1582050540</trans>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3231966174" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3339839818">
                  <item dataType="ObjectRef">2195067728</item>
                  <item dataType="ObjectRef">2391989006</item>
                  <item dataType="ObjectRef">1145733450</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="64174746">
                  <item dataType="ObjectRef">1582050540</item>
                  <item dataType="ObjectRef">863902176</item>
                  <item dataType="ObjectRef">1846385986</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">1582050540</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="271812266">yu2AGhz5iE2VbDIwP6V2+g==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Bg1</name>
            <parent dataType="ObjectRef">3311421608</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="2789673878">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="431339926">
              <_items dataType="Array" type="Duality.Component[]" id="1675564064" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="855021514">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">2789673878</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-500</Y>
                    <Z dataType="Float">-30</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-500</Y>
                    <Z dataType="Float">-30</Z>
                  </posAbs>
                  <scale dataType="Float">1.5</scale>
                  <scaleAbs dataType="Float">1.5</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="136873150">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">122</B>
                    <G dataType="Byte">171</G>
                    <R dataType="Byte">245</R>
                  </colorTint>
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">2789673878</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Graphics\BGs\medium stars.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="The_ESG.Scroll" id="1119356960">
                  <_x003C_scrollSpeed_x003E_k__BackingField dataType="Float">5</_x003C_scrollSpeed_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <ayy dataType="Float">0</ayy>
                  <cam dataType="ObjectRef">3353439916</cam>
                  <gameobj dataType="ObjectRef">2789673878</gameobj>
                  <pos dataType="Struct" type="Duality.Vector3" />
                  <rekt dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rekt>
                  <sprite dataType="ObjectRef">136873150</sprite>
                  <trans dataType="ObjectRef">855021514</trans>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2605892314" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="2678639972">
                  <item dataType="ObjectRef">2195067728</item>
                  <item dataType="ObjectRef">2391989006</item>
                  <item dataType="ObjectRef">1145733450</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="32084502">
                  <item dataType="ObjectRef">855021514</item>
                  <item dataType="ObjectRef">136873150</item>
                  <item dataType="ObjectRef">1119356960</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">855021514</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="146779744">SEF2rjF5QEqUJnHz3xfwng==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Bg2</name>
            <parent dataType="ObjectRef">3311421608</parent>
            <prefabLink />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </children>
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3177705418">
        <_items dataType="Array" type="Duality.Component[]" id="2226284012" length="0" />
        <_size dataType="Int">0</_size>
        <_version dataType="Int">0</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3042773086" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3643116192" length="0" />
          <values dataType="Array" type="System.Object[]" id="2017604750" length="0" />
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1567875772">cAfhFoEMBE+TC98IwXgc3A==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BgMediumStar</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1416978651">
      <active dataType="Bool">true</active>
      <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="1275288233">
        <_items dataType="Array" type="Duality.GameObject[]" id="3794187278" length="4">
          <item dataType="Struct" type="Duality.GameObject" id="1163692934">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3100349626">
              <_items dataType="Array" type="Duality.Component[]" id="2363991552" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="3524007866">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">1163692934</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3" />
                  <posAbs dataType="Struct" type="Duality.Vector3" />
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="2805859502">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">227</B>
                    <G dataType="Byte">197</G>
                    <R dataType="Byte">181</R>
                  </colorTint>
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">1163692934</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Graphics\BGs\heavystars.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="The_ESG.Scroll" id="3788343312">
                  <_x003C_scrollSpeed_x003E_k__BackingField dataType="Float">2</_x003C_scrollSpeed_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <ayy dataType="Float">0</ayy>
                  <cam dataType="ObjectRef">3353439916</cam>
                  <gameobj dataType="ObjectRef">1163692934</gameobj>
                  <pos dataType="Struct" type="Duality.Vector3" />
                  <rekt dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rekt>
                  <sprite dataType="ObjectRef">2805859502</sprite>
                  <trans dataType="ObjectRef">3524007866</trans>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="132313018" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="919663872">
                  <item dataType="ObjectRef">2195067728</item>
                  <item dataType="ObjectRef">2391989006</item>
                  <item dataType="ObjectRef">1145733450</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="1573913038">
                  <item dataType="ObjectRef">3524007866</item>
                  <item dataType="ObjectRef">2805859502</item>
                  <item dataType="ObjectRef">3788343312</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">3524007866</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="3622213532">ucIcmEDjgUmE/D49uh23vg==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Bg1</name>
            <parent dataType="ObjectRef">1416978651</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="372064111">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1702201951">
              <_items dataType="Array" type="Duality.Component[]" id="811965550" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="2732379043">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">372064111</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-700</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0</X>
                    <Y dataType="Float">-700</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.SpriteRenderer" id="2014230679">
                  <active dataType="Bool">true</active>
                  <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                    <A dataType="Byte">255</A>
                    <B dataType="Byte">222</B>
                    <G dataType="Byte">229</G>
                    <R dataType="Byte">137</R>
                  </colorTint>
                  <customMat />
                  <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                  <gameobj dataType="ObjectRef">372064111</gameobj>
                  <offset dataType="Int">0</offset>
                  <pixelGrid dataType="Bool">false</pixelGrid>
                  <rect dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rect>
                  <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                  <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                    <contentPath dataType="String">Data\Graphics\BGs\heavystars.Material.res</contentPath>
                  </sharedMat>
                  <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                </item>
                <item dataType="Struct" type="The_ESG.Scroll" id="2996714489">
                  <_x003C_scrollSpeed_x003E_k__BackingField dataType="Float">2</_x003C_scrollSpeed_x003E_k__BackingField>
                  <active dataType="Bool">true</active>
                  <ayy dataType="Float">0</ayy>
                  <cam dataType="ObjectRef">3353439916</cam>
                  <gameobj dataType="ObjectRef">372064111</gameobj>
                  <pos dataType="Struct" type="Duality.Vector3" />
                  <rekt dataType="Struct" type="Duality.Rect">
                    <H dataType="Float">1000</H>
                    <W dataType="Float">1000</W>
                    <X dataType="Float">-500</X>
                    <Y dataType="Float">-500</Y>
                  </rekt>
                  <sprite dataType="ObjectRef">2014230679</sprite>
                  <trans dataType="ObjectRef">2732379043</trans>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1671529248" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3726301525">
                  <item dataType="ObjectRef">2195067728</item>
                  <item dataType="ObjectRef">2391989006</item>
                  <item dataType="ObjectRef">1145733450</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="88385864">
                  <item dataType="ObjectRef">2732379043</item>
                  <item dataType="ObjectRef">2014230679</item>
                  <item dataType="ObjectRef">2996714489</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">2732379043</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="789347167">C3vlIchDLkSjaXQvY0ngSg==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Bg2</name>
            <parent dataType="ObjectRef">1416978651</parent>
            <prefabLink />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </children>
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3564229056">
        <_items dataType="Array" type="Duality.Component[]" id="1659629603" length="0" />
        <_size dataType="Int">0</_size>
        <_version dataType="Int">0</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="592239755" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1904440500" length="0" />
          <values dataType="Array" type="System.Object[]" id="2642882550" length="0" />
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2566742800">6ibC7rfT50adBWmvlYB01g==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BgHeavyStar</name>
      <parent />
      <prefabLink />
    </item>
    <item dataType="ObjectRef">3473009256</item>
    <item dataType="ObjectRef">4021675183</item>
    <item dataType="ObjectRef">2944414077</item>
    <item dataType="ObjectRef">1153122337</item>
    <item dataType="ObjectRef">3516702904</item>
    <item dataType="ObjectRef">2789673878</item>
    <item dataType="ObjectRef">1163692934</item>
    <item dataType="ObjectRef">372064111</item>
    <item dataType="ObjectRef">172733585</item>
    <item dataType="ObjectRef">3297887951</item>
    <item dataType="ObjectRef">3694630143</item>
    <item dataType="ObjectRef">4184254650</item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
