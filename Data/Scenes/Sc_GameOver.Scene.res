﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="134680343">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2191850261">
        <_items dataType="Array" type="Duality.Component[]" id="554840182" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="2494995275">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">134680343</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="1877309165">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">134680343</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3614556872" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2365248191">
            <item dataType="Type" id="327903662" value="Duality.Components.Transform" />
            <item dataType="Type" id="3474262730" value="Duality.Components.Renderers.TextRenderer" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1312315872">
            <item dataType="ObjectRef">2494995275</item>
            <item dataType="ObjectRef">1877309165</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2494995275</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="4198974573">d0fHl8A7dkibzKSbIPozFg==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">game over text</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="4102502431">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1932610180">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="3583263812" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1557139528">
                <_items dataType="Array" type="System.Int32[]" id="177949804"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">3474262730</componentType>
              <prop dataType="MemberInfo" id="2644023518" value="P:Duality.Components.Renderers.TextRenderer:Text" />
              <val dataType="Struct" type="Duality.Drawing.FormattedText" id="2209650356">
                <flowAreas />
                <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="3352429128">
                  <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                    <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                  </item>
                </fonts>
                <icons />
                <lineAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                <maxHeight dataType="Int">50</maxHeight>
                <maxWidth dataType="Int">100</maxWidth>
                <sourceText dataType="String">Game_Over</sourceText>
                <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="387574818">
                <_items dataType="Array" type="System.Int32[]" id="3774873038"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">327903662</componentType>
              <prop dataType="MemberInfo" id="2387217920" value="P:Duality.Components.Transform:RelativePos" />
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">0.6811876</X>
                <Y dataType="Float">-222.338318</Y>
                <Z dataType="Float">0</Z>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2010545542">
                <_items dataType="ObjectRef">3774873038</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">327903662</componentType>
              <prop dataType="MemberInfo" id="1506145068" value="P:Duality.Components.Transform:RelativeScale" />
              <val dataType="Float">5.52834463</val>
            </item>
          </_items>
          <_size dataType="Int">3</_size>
          <_version dataType="Int">323</_version>
        </changes>
        <obj dataType="ObjectRef">134680343</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\text\game over text.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1688372978">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2145077276">
        <_items dataType="Array" type="Duality.Component[]" id="4106653124" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="4048687910">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1688372978</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="3431001800">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1688372978</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Scoreboard" id="391583339">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1688372978</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="4009003030" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="380388022">
            <item dataType="ObjectRef">327903662</item>
            <item dataType="ObjectRef">3474262730</item>
            <item dataType="Type" id="2832209760" value="The_ESG.Scoreboard" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="2634599066">
            <item dataType="ObjectRef">4048687910</item>
            <item dataType="ObjectRef">3431001800</item>
            <item dataType="ObjectRef">391583339</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">4048687910</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="632790358">zMB9uN+HMUa+3lvyQm1bDw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">smallScore</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3612856712">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="3056473752">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="3340310060" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1105188392">
                <_items dataType="Array" type="System.Int32[]" id="3401380268"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">327903662</componentType>
              <prop dataType="ObjectRef">2387217920</prop>
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">-28.12061</X>
                <Y dataType="Float">-86.13767</Y>
                <Z dataType="Float">0</Z>
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3604180638">
                <_items dataType="ObjectRef">3401380268</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">327903662</componentType>
              <prop dataType="ObjectRef">1506145068</prop>
              <val dataType="Float">1.60764444</val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1676023572">
                <_items dataType="ObjectRef">3401380268</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">3474262730</componentType>
              <prop dataType="ObjectRef">2644023518</prop>
              <val dataType="Struct" type="Duality.Drawing.FormattedText" id="541621282">
                <flowAreas />
                <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="2776567982">
                  <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                    <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                  </item>
                </fonts>
                <icons />
                <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                <maxHeight dataType="Int">100</maxHeight>
                <maxWidth dataType="Int">0</maxWidth>
                <sourceText dataType="String">Base Score:   /cFFFF00FF0/cFFFFFFFF /n Enemy Kills: /cFFFF00FF0/cFFFFFFFF /n Accuracy:    /cFFFF00FF0%/cFFFFFFFF </sourceText>
                <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
              </val>
            </item>
          </_items>
          <_size dataType="Int">3</_size>
          <_version dataType="Int">841</_version>
        </changes>
        <obj dataType="ObjectRef">1688372978</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\text\smallScore.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2189228467">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2788631937">
        <_items dataType="Array" type="Duality.Component[]" id="2218852142" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="254576103">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2189228467</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="2726504274">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2189228467</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1519506784" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2555765323">
            <item dataType="ObjectRef">327903662</item>
            <item dataType="Type" id="2867958" value="Duality.Components.Camera" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="1904938824">
            <item dataType="ObjectRef">254576103</item>
            <item dataType="ObjectRef">2726504274</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">254576103</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="927722241">V7xAlTwXzkWGBAAcPvGtKg==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">GameOverCam</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2138342099">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2824300836">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1707169476" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2425022280">
                <_items dataType="Array" type="System.Int32[]" id="866414700"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">327903662</componentType>
              <prop dataType="MemberInfo" id="3615993054" value="P:Duality.Components.Transform:RelativeAngle" />
              <val dataType="Float">0</val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3326039988">
                <_items dataType="ObjectRef">866414700</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">327903662</componentType>
              <prop dataType="ObjectRef">2387217920</prop>
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">3.66407132</X>
                <Y dataType="Float">-21.5302811</Y>
                <Z dataType="Float">-479.6855</Z>
              </val>
            </item>
          </_items>
          <_size dataType="Int">2</_size>
          <_version dataType="Int">4422</_version>
        </changes>
        <obj dataType="ObjectRef">2189228467</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Cameras\GameOverCam.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3866748968">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2550378478">
        <_items dataType="Array" type="Duality.Component[]" id="392392272" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="1932096604">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3866748968</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="1314410494">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3866748968</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.BigScoreBoard" id="3248502721">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3866748968</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1559914442" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="722698092">
            <item dataType="ObjectRef">327903662</item>
            <item dataType="ObjectRef">3474262730</item>
            <item dataType="Type" id="2310621028" value="The_ESG.BigScoreBoard" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="3366954038">
            <item dataType="ObjectRef">1932096604</item>
            <item dataType="ObjectRef">1314410494</item>
            <item dataType="ObjectRef">3248502721</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1932096604</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1400388152">X6vE4L3SKEO3UtTZZNcmXw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BigScore</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1137556446">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1453133472">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="4131183836" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="416138696">
                <_items dataType="ObjectRef">177949804</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">3474262730</componentType>
              <prop dataType="ObjectRef">2644023518</prop>
              <val dataType="Struct" type="Duality.Drawing.FormattedText" id="2703032030">
                <flowAreas />
                <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="2515554954">
                  <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                    <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                  </item>
                </fonts>
                <icons />
                <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                <maxHeight dataType="Int">0</maxHeight>
                <maxWidth dataType="Int">0</maxWidth>
                <sourceText dataType="String">-FINAL SCORE- /n/cFFFF00FF/ac      0</sourceText>
                <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
              </val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2067647540">
                <_items dataType="ObjectRef">3774873038</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">327903662</componentType>
              <prop dataType="ObjectRef">2387217920</prop>
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">-1.52900028</X>
                <Y dataType="Float">136.461533</Y>
                <Z dataType="Float">0</Z>
              </val>
            </item>
          </_items>
          <_size dataType="Int">2</_size>
          <_version dataType="Int">354</_version>
        </changes>
        <obj dataType="ObjectRef">3866748968</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\text\BigScore.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2442658934">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3302224352">
        <_items dataType="Array" type="Duality.Component[]" id="2841824220" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="508006570">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2442658934</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2150094315">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2442658934</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">4</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2495394702" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="84984114">
            <item dataType="ObjectRef">327903662</item>
            <item dataType="Type" id="1497398224" value="Duality.Components.Renderers.AnimSpriteRenderer" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="2853526346">
            <item dataType="ObjectRef">508006570</item>
            <item dataType="ObjectRef">2150094315</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">508006570</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2323411842">NUuHJB/fi0+ysmp1I4UpyA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">ScoreEmblem</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2673159420">
        <changes />
        <obj dataType="ObjectRef">2442658934</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Graphics\Sprites\UI\ScoreEmblem.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="2464690886">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1845121040">
        <_items dataType="Array" type="Duality.Component[]" id="40525628" length="4">
          <item dataType="Struct" type="The_ESG.SpawnManager" id="2531077305">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2464690886</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.GameManager" id="323346742">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">2464690886</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="737735918" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2047129698">
            <item dataType="Type" id="741771152" value="The_ESG.SpawnManager" />
            <item dataType="Type" id="373048046" value="The_ESG.GameManager" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="2877110666">
            <item dataType="ObjectRef">2531077305</item>
            <item dataType="ObjectRef">323346742</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1319308690">qH5u5YYJnkynbn5jGaWb8A==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">LevelManager</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3224815340">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="831807352">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1379293036" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2620065192">
                <_items dataType="Array" type="System.Int32[]" id="2074289836"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">373048046</componentType>
              <prop dataType="MemberInfo" id="1499821982" value="P:The_ESG.GameManager:stateRef" />
              <val dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
              </val>
            </item>
          </_items>
          <_size dataType="Int">1</_size>
          <_version dataType="Int">1</_version>
        </changes>
        <obj dataType="ObjectRef">2464690886</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\LevelManager.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3349985280">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="967577414">
        <_items dataType="Array" type="Duality.Component[]" id="2558475776" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="1415332916">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3349985280</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="797646806">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3349985280</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.RestartPrompt" id="233238536">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3349985280</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3996772282" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3706884276">
            <item dataType="ObjectRef">327903662</item>
            <item dataType="ObjectRef">3474262730</item>
            <item dataType="Type" id="1826164132" value="The_ESG.RestartPrompt" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="3577855990">
            <item dataType="ObjectRef">1415332916</item>
            <item dataType="ObjectRef">797646806</item>
            <item dataType="ObjectRef">233238536</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1415332916</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1479618320">P/BNOeEADEiHTP6GTmW8Mw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">RestartPrompt</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2705719622">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="4253015040">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="3834405020" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3723963592">
                <_items dataType="Array" type="System.Int32[]" id="4036473452"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType />
              <prop dataType="MemberInfo" id="179755742" value="P:Duality.GameObject:Name" />
              <val dataType="String">RestartPrompt</val>
            </item>
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3707371828">
                <_items dataType="ObjectRef">4036473452</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">3474262730</componentType>
              <prop dataType="ObjectRef">2644023518</prop>
              <val dataType="Struct" type="Duality.Drawing.FormattedText" id="3670306594">
                <flowAreas />
                <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="3021626446">
                  <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                    <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                  </item>
                </fonts>
                <icons />
                <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                <maxHeight dataType="Int">0</maxHeight>
                <maxWidth dataType="Int">0</maxWidth>
                <sourceText dataType="String"></sourceText>
                <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
              </val>
            </item>
          </_items>
          <_size dataType="Int">2</_size>
          <_version dataType="Int">4</_version>
        </changes>
        <obj dataType="ObjectRef">3349985280</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\text\RestartPrompt.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
