﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="1681476083">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3409814849">
        <_items dataType="Array" type="Duality.Component[]" id="3879524782" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="4041791015">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1681476083</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1388911464">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1681476083</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.VFXPrefBehaviour" id="4187116646">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1681476083</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3490277856" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="505170571">
            <item dataType="Type" id="394828406" value="Duality.Components.Transform" />
            <item dataType="Type" id="502810906" value="Duality.Components.Renderers.AnimSpriteRenderer" />
            <item dataType="Type" id="590235030" value="The_ESG.VFXPrefBehaviour" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="287289544">
            <item dataType="ObjectRef">4041791015</item>
            <item dataType="ObjectRef">1388911464</item>
            <item dataType="ObjectRef">4187116646</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">4041791015</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1371453505">lAKRFahvxEue7h76CUecow==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BaseShotDestroy</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="4112081811">
        <changes />
        <obj dataType="ObjectRef">1681476083</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\VFX\BaseShotDestroy.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1795300544">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3895231622">
        <_items dataType="Array" type="Duality.Component[]" id="3333165952" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="4155615476">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1795300544</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1502735925">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1795300544</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.VFXPrefBehaviour" id="5973811">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1795300544</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1726391098" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2336505588">
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">502810906</item>
            <item dataType="ObjectRef">590235030</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1825281782">
            <item dataType="ObjectRef">4155615476</item>
            <item dataType="ObjectRef">1502735925</item>
            <item dataType="ObjectRef">5973811</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">4155615476</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1445075920">Llcriyk0Qkue0NTESBJsrA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BadGuyFlyDestroy</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1409384454">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1573009152">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="3625620124" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="76756168">
                <_items dataType="Array" type="System.Int32[]" id="3982033516"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">394828406</componentType>
              <prop dataType="MemberInfo" id="685918942" value="P:Duality.Components.Transform:RelativeScale" />
              <val dataType="Float">0.15</val>
            </item>
          </_items>
          <_size dataType="Int">1</_size>
          <_version dataType="Int">7</_version>
        </changes>
        <obj dataType="ObjectRef">1795300544</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\VFX\BadGuyFlyDestroy.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3900100685">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1372443775">
        <_items dataType="Array" type="Duality.Component[]" id="3852596014" length="8">
          <item dataType="Struct" type="Duality.Components.Transform" id="1965448321">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3900100685</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3607536066">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3900100685</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2667909913">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3900100685</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.BulletBehaviour" id="1980062367">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3900100685</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="2718502154">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3900100685</gameobj>
          </item>
        </_items>
        <_size dataType="Int">5</_size>
        <_version dataType="Int">5</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3289717088" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="709085109">
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">502810906</item>
            <item dataType="Type" id="657501942" value="Duality.Components.Physics.RigidBody" />
            <item dataType="Type" id="3099757082" value="The_ESG.BulletBehaviour" />
            <item dataType="Type" id="587461654" value="The_ESG.Tag" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="861940552">
            <item dataType="ObjectRef">1965448321</item>
            <item dataType="ObjectRef">3607536066</item>
            <item dataType="ObjectRef">2667909913</item>
            <item dataType="ObjectRef">1980062367</item>
            <item dataType="ObjectRef">2718502154</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1965448321</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1609582335">V5NXRCFCQkOJBO7hKezxWA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">PlayerBaseShot</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3302658861">
        <changes />
        <obj dataType="ObjectRef">3900100685</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\PlayerShot.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1317530207">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1965408669">
        <_items dataType="Array" type="Duality.Component[]" id="1723421414" length="8">
          <item dataType="Struct" type="Duality.Components.Transform" id="3677845139">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1317530207</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1024965588">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1317530207</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="85339435">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1317530207</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="877246548">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1317530207</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="4106329089">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1317530207</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="135931676">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1317530207</gameobj>
          </item>
        </_items>
        <_size dataType="Int">6</_size>
        <_version dataType="Int">6</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2312709368" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="357967607">
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">502810906</item>
            <item dataType="ObjectRef">657501942</item>
            <item dataType="Type" id="2930256526" value="The_ESG.BadguyFlyBehaviour" />
            <item dataType="Type" id="2344335434" value="The_ESG.GeneralEnemyBehaviour" />
            <item dataType="ObjectRef">587461654</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3088848448">
            <item dataType="ObjectRef">3677845139</item>
            <item dataType="ObjectRef">1024965588</item>
            <item dataType="ObjectRef">85339435</item>
            <item dataType="ObjectRef">877246548</item>
            <item dataType="ObjectRef">4106329089</item>
            <item dataType="ObjectRef">135931676</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3677845139</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="2740065109">na1tLeppzkeNxQoJUhW7DA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BadGuyFly</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2830917175">
        <changes />
        <obj dataType="ObjectRef">1317530207</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="4129436644">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2318315738">
        <_items dataType="Array" type="Duality.Component[]" id="3645957376" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="2194784280">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4129436644</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3836872025">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4129436644</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.VFXPrefBehaviour" id="2340109911">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">4129436644</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2608188090" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3473514784">
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">502810906</item>
            <item dataType="ObjectRef">590235030</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3315330958">
            <item dataType="ObjectRef">2194784280</item>
            <item dataType="ObjectRef">3836872025</item>
            <item dataType="ObjectRef">2340109911</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2194784280</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="4151306300">CHRT3nHaSUe+dvL4ZqFKpQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BaseShotDestroy</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3197789146">
        <changes />
        <obj dataType="ObjectRef">4129436644</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\VFX\BaseShotDestroy.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3763058635">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2764089529">
        <_items dataType="Array" type="Duality.Component[]" id="3140802254" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="1828406271">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3763058635</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3470494016">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3763058635</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.VFXPrefBehaviour" id="1973731902">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3763058635</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3928992512" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3596594963">
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">502810906</item>
            <item dataType="ObjectRef">590235030</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3588524280">
            <item dataType="ObjectRef">1828406271</item>
            <item dataType="ObjectRef">3470494016</item>
            <item dataType="ObjectRef">1973731902</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">1828406271</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1487994361">H/HQEJsDKUahwvM64Yp3/Q==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">normalexplosion</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3535757627">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1387618068">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="2668175460" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4225470152">
                <_items dataType="ObjectRef">3982033516</_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">394828406</componentType>
              <prop dataType="ObjectRef">685918942</prop>
              <val dataType="Float">0.5</val>
            </item>
          </_items>
          <_size dataType="Int">1</_size>
          <_version dataType="Int">5</_version>
        </changes>
        <obj dataType="ObjectRef">3763058635</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\VFX\normalexplosion.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="619577769">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1134340139">
        <_items dataType="Array" type="Duality.Component[]" id="4010623990">
          <item dataType="Struct" type="Duality.Components.Transform" id="2979892701">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">619577769</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="327013150">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">619577769</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.ActionInputCheck" id="1252282070">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">619577769</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.MovementInputCheck" id="2391063879">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">619577769</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.InputMaster" id="1443049482">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">619577769</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3682354293">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">619577769</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="3732946534">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">619577769</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.PlayerBehaviour" id="1846099358">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">619577769</gameobj>
          </item>
        </_items>
        <_size dataType="Int">8</_size>
        <_version dataType="Int">8</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2638614600" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3502915329">
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">502810906</item>
            <item dataType="Type" id="4149522734" value="The_ESG.ActionInputCheck" />
            <item dataType="Type" id="2601484490" value="The_ESG.MovementInputCheck" />
            <item dataType="Type" id="265909406" value="The_ESG.InputMaster" />
            <item dataType="ObjectRef">657501942</item>
            <item dataType="ObjectRef">587461654</item>
            <item dataType="Type" id="1303033050" value="The_ESG.PlayerBehaviour" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="3135509344">
            <item dataType="ObjectRef">2979892701</item>
            <item dataType="ObjectRef">327013150</item>
            <item dataType="ObjectRef">1252282070</item>
            <item dataType="ObjectRef">2391063879</item>
            <item dataType="ObjectRef">1443049482</item>
            <item dataType="ObjectRef">3682354293</item>
            <item dataType="ObjectRef">3732946534</item>
            <item dataType="ObjectRef">1846099358</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2979892701</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3146619731">VEMRnf/LFEKLOHJ61F6e6Q==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">PlayerShip</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3145139233">
        <changes />
        <obj dataType="ObjectRef">619577769</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\PlayerShip.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1267139041">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2593222435">
        <_items dataType="Array" type="Duality.Component[]" id="3180624998" length="4">
          <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="1913803845">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1267139041</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Transform" id="3627453973">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1267139041</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="85540510">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1267139041</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="564200568" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1813594697">
            <item dataType="Type" id="2471871886" value="The_ESG.SpawnerBehaviour" />
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">587461654</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="3259709760">
            <item dataType="ObjectRef">1913803845</item>
            <item dataType="ObjectRef">3627453973</item>
            <item dataType="ObjectRef">85540510</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3627453973</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1830933099">7Ue2mjH4w0aukCnCi6FBzw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">BossSpawner</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2916739977">
        <changes />
        <obj dataType="ObjectRef">1267139041</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Spawners\BossSpawner.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1161973303">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2949049461">
        <_items dataType="Array" type="Duality.Component[]" id="1644781686" length="4">
          <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="1808638107">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1161973303</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="4275342068">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1161973303</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Transform" id="3522288235">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1161973303</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="143643336" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="681486559">
            <item dataType="ObjectRef">2471871886</item>
            <item dataType="ObjectRef">587461654</item>
            <item dataType="ObjectRef">394828406</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="1904849184">
            <item dataType="ObjectRef">1808638107</item>
            <item dataType="ObjectRef">4275342068</item>
            <item dataType="ObjectRef">3522288235</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3522288235</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1766776653">OweAWsJCy0mEhF2SHyNdxw==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">CargoSpawner</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3363051199">
        <changes />
        <obj dataType="ObjectRef">1161973303</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Spawners\CargoSpawner.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="278749232">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2750358422">
        <_items dataType="Array" type="Duality.Component[]" id="2238769184" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="2639064164">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">278749232</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="3392117997">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">278749232</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="925414036">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">278749232</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3678299866" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="1486644068">
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">587461654</item>
            <item dataType="ObjectRef">2471871886</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="302860822">
            <item dataType="ObjectRef">2639064164</item>
            <item dataType="ObjectRef">3392117997</item>
            <item dataType="ObjectRef">925414036</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2639064164</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="251830368">oyMF9pjUdEO5rzJwQGuN5w==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">EnemySpawner</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1908265014">
        <changes />
        <obj dataType="ObjectRef">278749232</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Spawners\EnemySpawner.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="1441642761">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="107782731">
        <_items dataType="Array" type="Duality.Component[]" id="4177674998" length="4">
          <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="2088307565">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1441642761</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Transform" id="3801957693">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1441642761</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.Tag" id="260044230">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">1441642761</gameobj>
          </item>
        </_items>
        <_size dataType="Int">3</_size>
        <_version dataType="Int">3</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="498372424" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3573012577">
            <item dataType="ObjectRef">2471871886</item>
            <item dataType="ObjectRef">394828406</item>
            <item dataType="ObjectRef">587461654</item>
          </keys>
          <values dataType="Array" type="System.Object[]" id="998285344">
            <item dataType="ObjectRef">2088307565</item>
            <item dataType="ObjectRef">3801957693</item>
            <item dataType="ObjectRef">260044230</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">3801957693</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3939206387">3PgUz/kk6EGhSr4OLq3fPQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">HazardSpawner</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2954799873">
        <changes />
        <obj dataType="ObjectRef">1441642761</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Spawners\HazardSpawner.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="3075477849">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1587326459">
        <_items dataType="Array" type="Duality.Component[]" id="3731651158" length="4">
          <item dataType="Struct" type="The_ESG.SpawnManager" id="3141864268">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3075477849</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.GameManager" id="934133705">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">3075477849</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3509553064" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="3513778705">
            <item dataType="Type" id="1740233966" value="The_ESG.SpawnManager" />
            <item dataType="Type" id="3078664650" value="The_ESG.GameManager" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="344232352">
            <item dataType="ObjectRef">3141864268</item>
            <item dataType="ObjectRef">934133705</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="3276449667">J1nb/4vdSkiawZWTux3zNA==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">LevelManager</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1359129073">
        <changes />
        <obj dataType="ObjectRef">3075477849</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\LevelManager.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
