﻿<root dataType="Struct" type="Duality.Resources.Scene" id="129723834">
  <assetInfo />
  <globalGravity dataType="Struct" type="Duality.Vector2">
    <X dataType="Float">0</X>
    <Y dataType="Float">33</Y>
  </globalGravity>
  <serializeObj dataType="Array" type="Duality.GameObject[]" id="427169525">
    <item dataType="Struct" type="Duality.GameObject" id="53705674">
      <active dataType="Bool">true</active>
      <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="1615116436">
        <_items dataType="Array" type="Duality.GameObject[]" id="1419986788" length="4">
          <item dataType="Struct" type="Duality.GameObject" id="2383789083">
            <active dataType="Bool">true</active>
            <children />
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2026623927">
              <_items dataType="Array" type="Duality.Component[]" id="2164922766" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="449136719">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">2383789083</gameobj>
                </item>
                <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="4126417905">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">2383789083</gameobj>
                </item>
                <item dataType="Struct" type="The_ESG.Tutorial" id="2952018622">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">2383789083</gameobj>
                </item>
              </_items>
              <_size dataType="Int">3</_size>
              <_version dataType="Int">3</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3486801216" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="3852154877">
                  <item dataType="Type" id="3262778662" value="Duality.Components.Transform" />
                  <item dataType="Type" id="2196054714" value="Duality.Components.Renderers.TextRenderer" />
                  <item dataType="Type" id="4160509478" value="The_ESG.Tutorial" />
                </keys>
                <values dataType="Array" type="System.Object[]" id="188179384">
                  <item dataType="ObjectRef">449136719</item>
                  <item dataType="ObjectRef">4126417905</item>
                  <item dataType="ObjectRef">2952018622</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">449136719</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="3479028439">2vH9xejI502wbYO5v8N6Jg==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">Info text</name>
            <parent dataType="ObjectRef">53705674</parent>
            <prefabLink />
          </item>
          <item dataType="Struct" type="Duality.GameObject" id="3998568463">
            <active dataType="Bool">true</active>
            <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="1939733043">
              <_items dataType="Array" type="Duality.GameObject[]" id="1666984486" length="4">
                <item dataType="Struct" type="Duality.GameObject" id="282075689">
                  <active dataType="Bool">true</active>
                  <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="4152444457">
                    <_items dataType="Array" type="Duality.GameObject[]" id="3611007502" length="4">
                      <item dataType="Struct" type="Duality.GameObject" id="3508843014">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2099738682">
                          <_items dataType="Array" type="Duality.Component[]" id="4117947648" length="4">
                            <item dataType="Struct" type="Duality.Components.Transform" id="1574190650">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3508843014</gameobj>
                            </item>
                            <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="956504540">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3508843014</gameobj>
                            </item>
                            <item dataType="Struct" type="The_ESG.ShowHP" id="3236830036">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3508843014</gameobj>
                            </item>
                          </_items>
                          <_size dataType="Int">3</_size>
                          <_version dataType="Int">3</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2866793146" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="2754345856">
                              <item dataType="ObjectRef">3262778662</item>
                              <item dataType="ObjectRef">2196054714</item>
                              <item dataType="Type" id="3365056924" value="The_ESG.ShowHP" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="3904214222">
                              <item dataType="ObjectRef">1574190650</item>
                              <item dataType="ObjectRef">956504540</item>
                              <item dataType="ObjectRef">3236830036</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform dataType="ObjectRef">1574190650</compTransform>
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="3051433244">DftmuUISlkmlbXrSaTyknQ==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">Text</name>
                        <parent dataType="ObjectRef">282075689</parent>
                        <prefabLink />
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">1</_version>
                  </children>
                  <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3945960384">
                    <_items dataType="Array" type="Duality.Component[]" id="2613943203" length="4">
                      <item dataType="Struct" type="Duality.Components.Transform" id="2642390621">
                        <active dataType="Bool">true</active>
                        <gameobj dataType="ObjectRef">282075689</gameobj>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="4284478366">
                        <active dataType="Bool">true</active>
                        <gameobj dataType="ObjectRef">282075689</gameobj>
                      </item>
                    </_items>
                    <_size dataType="Int">2</_size>
                    <_version dataType="Int">2</_version>
                  </compList>
                  <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2392058379" surrogate="true">
                    <header />
                    <body>
                      <keys dataType="Array" type="System.Object[]" id="3479114932">
                        <item dataType="ObjectRef">3262778662</item>
                        <item dataType="Type" id="1910545828" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                      </keys>
                      <values dataType="Array" type="System.Object[]" id="2809425910">
                        <item dataType="ObjectRef">2642390621</item>
                        <item dataType="ObjectRef">4284478366</item>
                      </values>
                    </body>
                  </compMap>
                  <compTransform dataType="ObjectRef">2642390621</compTransform>
                  <identifier dataType="Struct" type="System.Guid" surrogate="true">
                    <header>
                      <data dataType="Array" type="System.Byte[]" id="2308490000">Hm5CjZIVUky4w4isKQOxZQ==</data>
                    </header>
                    <body />
                  </identifier>
                  <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                  <name dataType="String">HP</name>
                  <parent dataType="ObjectRef">3998568463</parent>
                  <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3061462342">
                    <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="3000652229">
                      <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="3768624854" length="8">
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3893441532">
                            <_items dataType="Array" type="System.Int32[]" id="1894349636"></_items>
                            <_size dataType="Int">0</_size>
                            <_version dataType="Int">1</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">3262778662</componentType>
                          <prop dataType="MemberInfo" id="2486230934" value="P:Duality.Components.Transform:RelativeScale" />
                          <val dataType="Float">0.299232155</val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2067784360">
                            <_items dataType="Array" type="System.Int32[]" id="2110135384">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">3262778662</componentType>
                          <prop dataType="ObjectRef">2486230934</prop>
                          <val dataType="Float">6.68377352</val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1134683378">
                            <_items dataType="Array" type="System.Int32[]" id="2232560554">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">2196054714</componentType>
                          <prop dataType="MemberInfo" id="881523284" value="P:Duality.Components.Renderers.TextRenderer:Text" />
                          <val dataType="Struct" type="Duality.Drawing.FormattedText" id="2079889934">
                            <flowAreas />
                            <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="113469998">
                              <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                                <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                              </item>
                            </fonts>
                            <icons />
                            <lineAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                            <maxHeight dataType="Int">0</maxHeight>
                            <maxWidth dataType="Int">0</maxWidth>
                            <sourceText dataType="String">2</sourceText>
                            <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                          </val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1027009632">
                            <_items dataType="Array" type="System.Int32[]" id="2596650320"></_items>
                            <_size dataType="Int">0</_size>
                            <_version dataType="Int">1</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">3262778662</componentType>
                          <prop dataType="MemberInfo" id="796504778" value="P:Duality.Components.Transform:RelativePos" />
                          <val dataType="Struct" type="Duality.Vector3">
                            <X dataType="Float">0.833333</X>
                            <Y dataType="Float">255.833313</Y>
                            <Z dataType="Float">0</Z>
                          </val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="359721196">
                            <_items dataType="Array" type="System.Int32[]" id="3980943956">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">3262778662</componentType>
                          <prop dataType="ObjectRef">796504778</prop>
                          <val dataType="Struct" type="Duality.Vector3">
                            <X dataType="Float">-3.507018</X>
                            <Y dataType="Float">-1.76907849</Y>
                            <Z dataType="Float">0</Z>
                          </val>
                        </item>
                      </_items>
                      <_size dataType="Int">5</_size>
                      <_version dataType="Int">293</_version>
                    </changes>
                    <obj dataType="ObjectRef">282075689</obj>
                    <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Graphics\Sprites\UI\HP.Prefab.res</contentPath>
                    </prefab>
                  </prefabLink>
                </item>
                <item dataType="Struct" type="Duality.GameObject" id="571316281">
                  <active dataType="Bool">true</active>
                  <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="2724539705">
                    <_items dataType="Array" type="Duality.GameObject[]" id="3155991246" length="4">
                      <item dataType="Struct" type="Duality.GameObject" id="3640559475">
                        <active dataType="Bool">true</active>
                        <children />
                        <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="4141398419">
                          <_items dataType="Array" type="Duality.Component[]" id="1753412838" length="4">
                            <item dataType="Struct" type="Duality.Components.Transform" id="1705907111">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3640559475</gameobj>
                            </item>
                            <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="1088221001">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3640559475</gameobj>
                            </item>
                            <item dataType="Struct" type="The_ESG.ShowScore" id="2579443611">
                              <active dataType="Bool">true</active>
                              <gameobj dataType="ObjectRef">3640559475</gameobj>
                            </item>
                          </_items>
                          <_size dataType="Int">3</_size>
                          <_version dataType="Int">3</_version>
                        </compList>
                        <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3989058296" surrogate="true">
                          <header />
                          <body>
                            <keys dataType="Array" type="System.Object[]" id="131570681">
                              <item dataType="ObjectRef">3262778662</item>
                              <item dataType="ObjectRef">2196054714</item>
                              <item dataType="Type" id="2484542030" value="The_ESG.ShowScore" />
                            </keys>
                            <values dataType="Array" type="System.Object[]" id="4248488064">
                              <item dataType="ObjectRef">1705907111</item>
                              <item dataType="ObjectRef">1088221001</item>
                              <item dataType="ObjectRef">2579443611</item>
                            </values>
                          </body>
                        </compMap>
                        <compTransform dataType="ObjectRef">1705907111</compTransform>
                        <identifier dataType="Struct" type="System.Guid" surrogate="true">
                          <header>
                            <data dataType="Array" type="System.Byte[]" id="1882360315">ocHCzzyjWkasubQ3Kd8DiA==</data>
                          </header>
                          <body />
                        </identifier>
                        <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                        <name dataType="String">Text</name>
                        <parent dataType="ObjectRef">571316281</parent>
                        <prefabLink />
                      </item>
                    </_items>
                    <_size dataType="Int">1</_size>
                    <_version dataType="Int">1</_version>
                  </children>
                  <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2118328064">
                    <_items dataType="Array" type="Duality.Component[]" id="390757011" length="4">
                      <item dataType="Struct" type="Duality.Components.Transform" id="2931631213">
                        <active dataType="Bool">true</active>
                        <gameobj dataType="ObjectRef">571316281</gameobj>
                      </item>
                      <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="278751662">
                        <active dataType="Bool">true</active>
                        <gameobj dataType="ObjectRef">571316281</gameobj>
                      </item>
                    </_items>
                    <_size dataType="Int">2</_size>
                    <_version dataType="Int">2</_version>
                  </compList>
                  <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="75509435" surrogate="true">
                    <header />
                    <body>
                      <keys dataType="Array" type="System.Object[]" id="455415060">
                        <item dataType="ObjectRef">3262778662</item>
                        <item dataType="ObjectRef">1910545828</item>
                      </keys>
                      <values dataType="Array" type="System.Object[]" id="3460466998">
                        <item dataType="ObjectRef">2931631213</item>
                        <item dataType="ObjectRef">278751662</item>
                      </values>
                    </body>
                  </compMap>
                  <compTransform dataType="ObjectRef">2931631213</compTransform>
                  <identifier dataType="Struct" type="System.Guid" surrogate="true">
                    <header>
                      <data dataType="Array" type="System.Byte[]" id="705804208">KtkgES1u/EGwz137eJqi7w==</data>
                    </header>
                    <body />
                  </identifier>
                  <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                  <name dataType="String">Score</name>
                  <parent dataType="ObjectRef">3998568463</parent>
                  <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1116110502">
                    <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1877043157">
                      <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="2676671478" length="4">
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3796315964">
                            <_items dataType="Array" type="System.Int32[]" id="3679373124">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">3262778662</componentType>
                          <prop dataType="ObjectRef">2486230934</prop>
                          <val dataType="Float">5.52157164</val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="816569238">
                            <_items dataType="Array" type="System.Int32[]" id="3358280982">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">2196054714</componentType>
                          <prop dataType="ObjectRef">881523284</prop>
                          <val dataType="Struct" type="Duality.Drawing.FormattedText" id="1034086888">
                            <flowAreas />
                            <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="4274999768">
                              <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                                <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                              </item>
                            </fonts>
                            <icons />
                            <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                            <maxHeight dataType="Int">0</maxHeight>
                            <maxWidth dataType="Int">0</maxWidth>
                            <sourceText dataType="String">2</sourceText>
                            <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                          </val>
                        </item>
                        <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                          <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4149772402">
                            <_items dataType="Array" type="System.Int32[]" id="989080682">0, 0, 0, 0</_items>
                            <_size dataType="Int">1</_size>
                            <_version dataType="Int">2</_version>
                          </childIndex>
                          <componentType dataType="ObjectRef">3262778662</componentType>
                          <prop dataType="ObjectRef">796504778</prop>
                          <val dataType="Struct" type="Duality.Vector3">
                            <X dataType="Float">-0.000101725258</X>
                            <Y dataType="Float">16.6630039</Y>
                            <Z dataType="Float">0</Z>
                          </val>
                        </item>
                      </_items>
                      <_size dataType="Int">3</_size>
                      <_version dataType="Int">215</_version>
                    </changes>
                    <obj dataType="ObjectRef">571316281</obj>
                    <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                      <contentPath dataType="String">Data\Graphics\Sprites\UI\Score.Prefab.res</contentPath>
                    </prefab>
                  </prefabLink>
                </item>
              </_items>
              <_size dataType="Int">2</_size>
              <_version dataType="Int">2</_version>
            </children>
            <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="916254904">
              <_items dataType="Array" type="Duality.Component[]" id="2297643097" length="4">
                <item dataType="Struct" type="Duality.Components.Transform" id="2063916099">
                  <active dataType="Bool">true</active>
                  <gameobj dataType="ObjectRef">3998568463</gameobj>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">1</_version>
            </compList>
            <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="740357209" surrogate="true">
              <header />
              <body>
                <keys dataType="Array" type="System.Object[]" id="1563338388">
                  <item dataType="ObjectRef">3262778662</item>
                </keys>
                <values dataType="Array" type="System.Object[]" id="2369959990">
                  <item dataType="ObjectRef">2063916099</item>
                </values>
              </body>
            </compMap>
            <compTransform dataType="ObjectRef">2063916099</compTransform>
            <identifier dataType="Struct" type="System.Guid" surrogate="true">
              <header>
                <data dataType="Array" type="System.Byte[]" id="2273637168">cxif1cp3TEiXXEyvaFWdMA==</data>
              </header>
              <body />
            </identifier>
            <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
            <name dataType="String">UI</name>
            <parent dataType="ObjectRef">53705674</parent>
            <prefabLink />
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </children>
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3581518902">
        <_items dataType="Array" type="Duality.Component[]" id="2688318782" length="4">
          <item dataType="Struct" type="Duality.Components.Transform" id="2414020606">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">53705674</gameobj>
          </item>
          <item dataType="Struct" type="Duality.Components.Camera" id="590981481">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">53705674</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="974594352" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="737350088">
            <item dataType="ObjectRef">3262778662</item>
            <item dataType="Type" id="738645612" value="Duality.Components.Camera" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="2035543774">
            <item dataType="ObjectRef">2414020606</item>
            <item dataType="ObjectRef">590981481</item>
          </values>
        </body>
      </compMap>
      <compTransform dataType="ObjectRef">2414020606</compTransform>
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="1038542900">IDiuQ50yt0O5QXI1SL8FXQ==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">Camera</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1837074850">
        <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="553820866">
          <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1879185936" length="4">
            <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
              <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1802396208">
                <_items dataType="Array" type="System.Int32[]" id="2892471996"></_items>
                <_size dataType="Int">0</_size>
                <_version dataType="Int">1</_version>
              </childIndex>
              <componentType dataType="ObjectRef">3262778662</componentType>
              <prop dataType="ObjectRef">796504778</prop>
              <val dataType="Struct" type="Duality.Vector3">
                <X dataType="Float">100</X>
                <Y dataType="Float">-5.266133</Y>
                <Z dataType="Float">-600</Z>
              </val>
            </item>
          </_items>
          <_size dataType="Int">1</_size>
          <_version dataType="Int">13</_version>
        </changes>
        <obj dataType="ObjectRef">53705674</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\Cameras\MainCam.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="Struct" type="Duality.GameObject" id="297565808">
      <active dataType="Bool">true</active>
      <children />
      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1898165846">
        <_items dataType="Array" type="Duality.Component[]" id="2384031776" length="4">
          <item dataType="Struct" type="The_ESG.SpawnManager" id="363952227">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">297565808</gameobj>
          </item>
          <item dataType="Struct" type="The_ESG.GameManager" id="2451188960">
            <active dataType="Bool">true</active>
            <gameobj dataType="ObjectRef">297565808</gameobj>
          </item>
        </_items>
        <_size dataType="Int">2</_size>
        <_version dataType="Int">2</_version>
      </compList>
      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3105099482" surrogate="true">
        <header />
        <body>
          <keys dataType="Array" type="System.Object[]" id="2739639844">
            <item dataType="Type" id="3321308868" value="The_ESG.SpawnManager" />
            <item dataType="Type" id="3027780502" value="The_ESG.GameManager" />
          </keys>
          <values dataType="Array" type="System.Object[]" id="2983978262">
            <item dataType="ObjectRef">363952227</item>
            <item dataType="ObjectRef">2451188960</item>
          </values>
        </body>
      </compMap>
      <compTransform />
      <identifier dataType="Struct" type="System.Guid" surrogate="true">
        <header>
          <data dataType="Array" type="System.Byte[]" id="4181787680">fTyjbR/Pp0KNT7MqIt4rqg==</data>
        </header>
        <body />
      </identifier>
      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
      <name dataType="String">LevelManager</name>
      <parent />
      <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3811449462">
        <changes />
        <obj dataType="ObjectRef">297565808</obj>
        <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
          <contentPath dataType="String">Data\Prefabs\LevelManager.Prefab.res</contentPath>
        </prefab>
      </prefabLink>
    </item>
    <item dataType="ObjectRef">2383789083</item>
    <item dataType="ObjectRef">3998568463</item>
    <item dataType="ObjectRef">282075689</item>
    <item dataType="ObjectRef">3508843014</item>
  </serializeObj>
  <visibilityStrategy dataType="Struct" type="Duality.Components.DefaultRendererVisibilityStrategy" id="2035693768" />
</root>
<!-- XmlFormatterBase Document Separator -->
