﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">5</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">1</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">64</H>
        <W dataType="Float">64</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">0</Y>
      </item>
    </_items>
    <_size dataType="Int">5</_size>
    <_version dataType="Int">5</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAUAAAABACAYAAABr564eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABgxSURBVHhe7V1pr23HUd1/hJ+CxGxEHCu2YxJPwXYcO04cBhHmSEwSSPwFPjAkRCHOCA+RxA4YQhyDGfzIs4ktMMm7555777PfI5Ei8fGmV51afVfX6b3PtM9wn3tJS7u7urq6qvfedfZ8uoaGhoaGhoaGhhFx449/uGBDQ0PD2wae+M6dg4lwUfsmOP63nyvY0NDQsDE0adWSl8iZBC0R1kC908/8dHd65a65RLVJAvN+506XNjQ0NGwAJi0hZa5RT5KekDKDXk5UFS6dwEK/gg0NDQ0bQ5JWd/bJn7DkBZ78zV3dd278kmvNwxNRcUSG5cmX7+6m//getqled/Lcuzs7Ovz8O2y8IYS+eYyGhoYtgwlh0U56uwGJKyU9S4BYIgGSEZKgIi1hTb75/m7yn48VbTgthm2wdooMqH6iJlHXuECfvKGhYU0g6eEoxXbS2dJbLgAZuQiqu4z+PoFEN3n5EUuCyqP//mBvomEScvJozfqxfP3bH+5AlO0IMNHbrN+AndyuoNx57uKGhoZVUEtMXrcdEMta+6IESbAvdJ0qc63dIiQPoyIlLtKS2PXJR6BjSRDyoJMTm5M2WT8/ev3JC1tHT3eT//oAdagX64VMwXmTBGq6DQ0NSwA7D07z/FSPO1SRmM7+7EdNhh3W23I7bUCGnQ/LmAS9Xal2lN5jd0CycDJ5WAIhAcyLc5a0pr/QXb/+NOuqY+3g5JXH52xPrj7aHf3PU4We02wEfa0bFZgrsLhG+dy7q7oNDVsHN0jyssD9zTuixFAkp7RTmQ6WlJ395R2FvrYzCYI8Ojz71E/Z0sly1gN3DU8Y5reXc31y9bGOR3k+P0rK2K461q+w89qTmjSV1pfw8Y21OuHzZXMJOyd/dede5q/hNodvaJk1SHveuYPc6ocI+KZHgJQlMikwpmLHVbkytBW2sDz7CztiyXKnysyHXSH51Ufz2eONCa7GQufojadoozt640Pat2arGJsQmdlRYJ4wl6ef/xmbR6y/hobR4TtlsaNG9OmInGWTHyrUT78jaUkAxI6qS5bJKEs2ClJHZdiBcXqNsXD65nL3Zn+ALyDu2DKelXkyO03G9cBU77PDNr1u6F4UR6dWVlCGdr+T7C0NDSMCG5aTO67KjGcf//FZ+5/+yFybyWV5CHB/5nj25z+G5fn02XtsiR1r+sIDHR7fwI6GnRZLXN/yxzkuZK8+3h2//GiWLSL0MAaIMuhJ0L0sATm5LtSG2mLZbyTkZHycYu9JYDXZxvQkaD4BKC8g5s3KDXsEN6A+XnZILHmnVU7/7j7bGEXWp+8W9wf4wGtyIK7jMfGBiAM7Ix76Rd2TQrGjol6VSQKs6ZAqR3n6T/dbv5Mvvcv8I1AO3GgOxYZx+vf3WSKbfPMxk0/+4xHzw3xJSRBL3KGlr0tw3aTIfuYPwTqYfKFvlqC9bPW9AANHvvnqR7r/P/kjo8pvR/jGpMwbVuCc7qGj5qvX5+JL6zfz9HOz6zGgX+syucsKe/uC+5F9dt9y/fifH+qOXnvCnn3LukmW2xPTDlkwtmmdsj5dyugHxgSSvMMRGGR6Q4Xtq0D7Jl748NLDGKfwD0dhfsRLnl8//vk5vQqjjsUQZDOm0+OqfEbzGUunycQf8wlx4AcD9Z1DHVG+9ZX7kfzOb115j15DyUkRvB2gG5MS8foKs3Jsd1rfQ4b7WPjKMuQaZy1eLaON5cRsJ9i0+i6AsXgEyDhQ1jj4CMf06/fbzRHIIqnPPkPs041y+AH/QLw+pnK5g2ztykVwPdq3hDr92nt5eovxuSQt+SDx4Zm/KJe6km197UbEc/1/802RWp9eWeqb14XHslT8o8EdKBxBwrvxtYf5Ll+eZFzcxekF5N/9h8ctMV72JMgJd1qcmANZUTYfoMpA6LGP93erhwX65n5mX0m7aP+F2brW+FBXGcqcmxA7qDatvktgvJgElZANMerHPnjsI5evldcPI9UG58KX1s5rjaifffInOWdZB+VFoD7sMG7Ijv/lIU0yYzImLyszZhxhqjywr415J88H49gJdHAu33z5g93N5x7UlVHlW5+4Yy4JgpcJKY7i2hGIOcDKqSU8Jduhz75uy60fDuATfRwiYycp13rU0fgDffTdAWPGJKj+oqxyHDFRV9tq1DaWtV8kdX0ujJTrUtudFsciuJ7ZwFEt66fP3DGUiCJND0eEeBC60tZH08HYTnvzI7YvwZiD8hzsBDJwN33+Zzue7roTmcG5gjc/c1f3vReesn63Xv+o2bwsSP7njUhXzKLkR6qezI9bPxx4nBYryPVJ3yFjOVLb2F/bVS608fYBjOsPM1uMk2t26jsrpyMvHn0ZX3rYTkupS8b44vYQ13ukHiEuQ+jCB/gOLgPX5Zi5jmWyqYlIE5LK7bodH272B5ojtU9BJEy+Owy6XMdheYjsbzHgB2mnj7/Qed6q9wk0wqmKwwVVH8mTgYCHjuRzvlOIWJZNejWyL2y5TR/lMAB/4Fsk/cd60zoY58PX7ZwM/bgMnJsHyqJ8bPgY2edEbpvGGJNyle2AuuxLuZaHZJTD11XPHkKMuU4byfZSCQn9K3eF2Z/Utky7ubLekR+ZcxC4M+igPnGcPGOS15ydI/TY583PvrO7ce1DsGkrBDxUJH8HrxetS5kPG+NQQH+Up8+Uj4LQd42H1KSgOz302UfrwtrYJt8GinFmzy+aX3yUBWQcYxHzwTnpmycl21GGf+A6l04sRsZX7sfWnsbQpFRNTvRJ3urQfuRcPxJ9JXku6leV7QWYMDjPw01MJAhZxcleQp99cSqMlQ6Z00c7PDBmxqsb7rqkDc4HxjhEMHZSY4j1PurOrXaw5PqnPLF4ud1fc3JvxgNs8kdNST+VjGPbxFg8HY5jwze/C7zWfHg/2jUZliRuSsid2XN8sECv83k/e9ibMiFkNTnZXf/O0xYX3gdGfQGrNsC9gJOkd3kRTHBwKaIfbdz80nuLo8BDRfLV4q7FsyllPny0w4DHnGP3dTTnu9aHqD8a7Bftok5S1+vu1XiATdjui4F+qX99umNQfyRI3kmGfNN5QF95eNhkWHrdODf+tcfzIzCU+YcRst+BTGA1mg4+fcWyM+ppe1HeO7jRgOLYyqSNtz6Vft2vXbz/Bx4aELNe+xuT4SjQxjoE0Bf3yxh97quvQtrmztXXnuierQ/YiOQnrkCOr37QLx6VHX3rCVuyfRvkmCRl7qdHsx6QAHHUy1NoHFFCNv3Gg72J0Pw6/UVLRHZUWP+ayypkfyy1rDLWlR7FnoAJc9rKqDi4NDGxsHEZToMZc94YtkDOKcbaBzzGSPOJcW+S6JTRDuzrWFrn543ATeF2sm1hef0vkD7qclvk3Oj4JF+Rq3FZuH4Ze1riB55vhOhpcA+r7Uu+KQJCL+qqjOU5nWWR5mv8gylOFogVEpxbmbR16KfBjBs+j5UEIhG7z4ePuht4bDnGSK7nbcWtdivjm19jgfYCizF9G5yj+ryIm85VTILTFx/qTr5yjx2tRX9B/R7hIlAv0Owcfau4OcFlTELanonkB1/DGyN97NOhHEvVYT2TH0wgCZUljvtJfJ2s5MTGpC3YpcNYHhrg31gx18gNnnOxSzA2Euug5tvYjHYxrvtAn4xjIdqNxPvL9KNG9XUZ1uJbdS5tbP8wQfKxuo60Dc/l4pW9VPao54E219dlN/3qvRa/UBOOyiNzO64NhrvDqqfsa4NcSVnU4xdjLH4eGeO0PtWzHOVRIZM259A6pC3YFcd9tMMB/Bsr5hoPIAEa9WsoINZH9HVMwr6O5yx8AjdBsFUbLxP+FA89O2u+D5HrU/uvmvxI2kBiO/3ind3Js/eaf9pO31H3WDz6GVCv8Bx32v01uIUJx6m6BY/PfrW7dfo73XdPf6+7cfax7vTsN1L9t+3yVmrX/lqvkTpRl2NZ3Y84bS4QC4iv2qDucuOo4MSBdGRdquP4jpw7bM4fGhh3LY6xKPPho+4eHmeOVwn/4Oe6OzKJ/hJrtqtjBZo/68L7Z3scr0a0rfItvyGaLaG2rTOH0Z7GpLF52aOfAXXqsB1L/3/fOFZNRjIJFTpIeCkmS3aMzcvd90/+0JLi2dlvdsc3fmWub4XVMSrMOQOxYKkJcCvgxHHAilNLk3bA0y/cSce35/wGSD5a3PCbK3gs0p7Mh4+6P3i8eV2Ti9b52HMjY7tnqwN9yXiEq2Rssh3muvq0DDkPaouyRXME3Zp/PbyIzT4L/47qa2Gqp+QfnDur/gjndI9u/LKVEdMy9H5zdgKH2kjTSXM1+4r2l+/mV6cytwKZPFsBwamVSBt4DCEeuh4aGDN8rMWyKWHX58PGOgQMxQx5lNUoG/0cazYg45gsJ442J7SlxJdVMN4iRl/7yJhrNsCoHwmd5Fcm6rg+KfJqHOsgjVckG4zhd4BVrnWVpyO/38pHfYk8CiyOBpXsB56c/XphK1DHUub2vSAF0eEbf2nC88oRp5amruTL8D4wNjC+LA//48pcl7TDucA4hwL4wnhJrjcsKdO58BjmdGqMOtoX1D8N2gS0oXZY5xEhjiCOX3rfYCJU3/vIuZB+tk1L3ai6NUIHfilrMv8sFspFfAq21dr5QDM+/ErfXFYkG6mr3GJIzEmPUBmJU+G0ZB+1ne0Jo7xWN+4U+HILnL/1vL1HaCuBK3RZ6oq8+bl3zexdgi/CYAPSeGUlbkSdD4xxCPBYi3jdPyN9j2XqMqahOaIt9mNZ2/TxDnAdeF/Yy3V9/Y1jq+8ss87yMmT/SLZF/T5qX/oa/VQ5H2yOgMx1tG7UxEe7FTLZsJzbsH4TmdjMfh9ET7eLwp6wTx7JH5jd5A9PVBbAW1futYkdmLgq0cdpR5M+MT7C4QL+csdZNeYh6nyA+wR96LtWpnFTFsvarnUldwD2qxFjab1vB18E9HEbRT2uw751SrnstIOEfo1sX8YO9P1udD4yremRaNcYFYyXbVo3++4fkmHNdqLKCx3c6fX9F3GZ/Rpch3rWd8VT4JoOqDH4aFsEBuFra7defIITWazgIUKPfZD8vvfGxxiAj3DYwEajp8IgV+iqZD+fD7O9T9AH96dYV8Y/mb0pQf9jOW4D2r4pxRf3dnmgD8m6HgGCsF97xQ11UuVDjH24XHY70f5kTUf9BxccAeY2rSc79gEEfPofr7jFcRJjAoplu8Pric2SXA3eZnMA4vGYYEs51BZZPBC9E/hgFsibzy5/PRDt1MXRo02EH1HC5mUBNpwYL2LRWPsY9WRO3Pp+gPH572dg9M3JeibiYVnjAmuyPspYxlgXusebAXZAvgdMH7CsraOavEbqxD7L9AXRr8Y+XZ8TmxdwHSRblmx6xomJaC4x+SMwFmNiNQlSTh1PgIUdIX0iazpKH2VHSBNl//rGlXN2xZ7C1pWRVxwY23CrHnIkv8t2BAikGOauIWFFYOWGFVOQ7XFO1j21GxMYH76ov/TP27IOyJjJWKdM50bnpzYXcr0vy7xecCzAlj5+RX/oo/pO2TKk/qr9SPoTyfbkd+RG85JsWpLRMYQxAc0lpcmNtB+f/v5cEqzQ2r5/8gdWFhtqk+VijMCo45HsEGmylBa4HQ1eubvjtUEl7lbhYWfs7NBn38sKbHAgr5UhJl8Zmb7CCxn0OCe0Ae4b8EGPAMH4EQIvWxvjYV1lIOLWWCGLc8R2/b5djduC2zc/+KWXyasf4Pa5k6++LCJ9ISGDz5wb5TpINu1OMP8C4Kj8CEJMNCrX+vn07NeYBJnoasx3gkP/OGZNVqtn7g2YQD0aBBGg/TvcF++cvbrzt/afnWgzffJ2ADa8eD0JRLyvfPSHjCjH9sS1N9ptgT4pFfjjnCSzjRDLk79+p9VJyCNVHnWkb++YYyKO4WXzBeuISy1jWdlhRyfHVfa1wWd+uUZoMQ0h9Z3b97wOu7ZUn4SabLRe6GGe8LYHeDOd4pL/d/q7xSMw0ifa0Xq0H9uKMvzHg9C1h8C3Dp+8mRNXL35BA639dgQmPFA3TGWhd5kAf/n/tNPn7ytihIzLSJVHHWwXtEFu83IA7Po4RT36RFLGnVZlsS3s2CtRx6xRdVD2GLLM6xbTEJIu902XFLJsv/KxUnIu8Xh5If1u8TmOEkUey5Fsq1HbcxzLzsXW4I5U+XYAJn+IlxXuv+0k02fvyXUQGyGXSuhSB2XK+5KF6Puo4wJ2SakXY2OpvqpvQ21RtizV7iLCP/qo/V1uMQ0h6c7ti143O5Orj+ZTYVD9dMbkpOWF5FsjOF0ObbTTR9XTflmOf4aDz8vORUPDSsBG5RvXjHIKVtkg846JGxt6p1XbWaYdcJc3hDAOxqSv/KsH+kQ5lmT0HWU8QoLlukeBtD1E+GFzWXk+c5M5S7aZ8GKZ/vUlItZj2yBxJIjHZlI5MupGmeqxnOv6Nx3rzkVDQy+wURkl8clOYnWWIUc9vMWR26kfmHV3BR/PxrePCPgNOtTpI+qkxqZ1fj4L9VoSpGxRgqQN+qRkkuvjukhjKs0Hlu0Pkr794ZhsjP7Nv5yApLyI1B3qE3WU1g6o3OcBc7XRfDQ09IIbGXYO3xCNvuHN1f3Ir9rPdGbJNHPXkLHNNyxZjmSbkrGgneVItvXp0L7xX99nvjjVP+M2kMbVxEcWyTD+/y/kA+8LF7qBgzr8nD6+KO0+uJczPz0Zm52Ibc9TQwM3sCIJkNgpuCGDqLMNj9PoqRva2L7LU14FxnQWPtVIHWfuhzaNOVJt8PGa2E67+KIxr2PJZ+85lnFMpPGN8V3gREs2gbkd+uDAH5urvEbVzfr8urN9UTrRx7PxAS/nxNvQsHPEHfLs4+UzkFiyThkIWe4T3rxAeR9J0P2Z+RVO65WUm54wvg6p8UQbZNTVPrCZykb8QxvqLjf6M5ru/eZIY1sy8mRjyUWWLEfmNvb3pZYZI+Xa3sfaWGDeNvzyhLGhYe/ARokdkxsxNlbUfYPNOy6oOy+WkEFf+7F9V6AfSvgTZULvOQPqkDMGcCJfkqbM6P/pAXluS6e7bO+z7x8rVb84rmutj2QzM/mQfekhdajHvoypsOfM8VbqUWZ9FPgRCH8ANVrsDQ0bwzdG20GwEaMcbnoUJFC2L/iGt05UZxfAeEp8kBdxIJ5UN3+cuUxoG/WnX5392bjawJKEnGT76adnR5HOYgwg6RqPX3xQ9bIuOAaSP5H0m+U5PbkeRy6q16g6Fivh8Wm8RewNDXsFNkIe7WEn4ca5LKAbuU/k607/PnujAP74B1ntw6wK9zc/CkQ97Mici9qRMNqgI7Ko4yOUcD0y6499WgwguTnnEiBBud6U8CVpyaxCbddllhMea43n/PR9Q8NegQ2SO7ovveXywXfqYsfi3zRgSfhOaDHHJXSx5FwoeRMIOzqWQpP7smAEZPG0eIwkiMRDAj4PCxMg/fC+WnbNCwSdTP4P8dEr7+8m6cfHbXuvEphXnBpj7CG9hoadgTvrpjvhIcLvONqNAsJjnUtevCnCywARkOE/e7HzwubpM/kI0dq8rKTcLcyQ+nb+nx1mh7qbADbdntU9uSxMgJwf9pWlUSFy00fig++Tlx/h/xhjHJJjWF/EB53jFx7I/nDZ0NCwJchO65Ii4deS1WAy8lNr28ldV/uqjaI9ggljSGcVeIx9CbCgtuNBac6P91dqm/XzsvmMJXj8jQfs8R+8Z27/y+K2waPXnuyOXn+y868SzfRffJDt1DU2NDTsCEg4Tks+kX1AG48UwfBaW+4fGRHare+68KRkiQlLICQXS0bO2GYUG2pLmdtw9DrBXfGXHu6O3ngK/Wu2Tcb+HqPx+Ov2B2qFHsoNDQ07BJKO75QuWQ7sJzs27axry7guPDEViYpgosGrfvqRhMCsB+KoUF6TU9uZeIi60rewO7n6mNnBEaB8fCPH6n0yGxoadgTZGTdOgPrXn+CukfyIycpbLsA2XPNDMpymo7DpxfU4TWS5Lv1o14g7x0ymfODb+85xCK6Tx25oaNgRPFmtfdTGZyCRELDUL2+Tu4YnKmOEtJ379UfQ/PejuTkS0je/7gbyeijmwuXV/n0Q3ZxwGxoadgAmAF+6dHmgj17Yd1tK1zwcIIExcfPL4IgB8iFIoiqonx6Tu7uk9x4G9FbRb2hoGAFIUNh5nS5dDbSBBBA+MGvlQwR9lKS90Ne+JIV+TKjhxoZxGayq39DQMAK445PrAP2YAJwb2dsF4Fvw2fwewlCSQl+yoaHhbQZNAJclCUSfL4vfDQ0NDQ0NDQ0NDQ0NDQ23PbruB+ocz/BK5M5rAAAAAElFTkSuQmCC</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
