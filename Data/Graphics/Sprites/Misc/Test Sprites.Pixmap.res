﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">12</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">12</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="256">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">320</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">128</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">160</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">192</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">224</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">256</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">288</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">320</X>
        <Y dataType="Float">352</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">352</X>
        <Y dataType="Float">352</Y>
      </item>
    </_items>
    <_size dataType="Int">144</_size>
    <_version dataType="Int">159</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAYAAAAGACAYAAACkx7W/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAd3SURBVHhe7dwxkuTGEQXQPQ1lM3gs3Uq8B4+gQ+gIcmSuOlHABKb4C8AMquHsexHfICo7c6y/Hn8M/HxTrkq/nZGr0m9nZJa0ewvALT///Pc/pqZ2ttXRvsCW/PXbH1OTbrwy8vP3f/13ampnWz3Fz//9559/S31vzwDfF0v8TmpnWx3F0n5n6mY7HcUSv5Pa2VZP4R8A4G1iid9J7Wyro1jS70zdbKejWOJ3Ujvb6in8AwC8TSzxO6mdbXW0vX9KKu7vJO1eMxJL/E5qZ1t927Lr4B+AWXeAX1Qs8TupnW31JbHI76R2ttWXxBK/k9rZVt8Wy39LvbcxgO+JJX4ntbOtviSW+J3Uzrb6kljid1I72+pblj2p+LdsMzUMcGZfGh9JJX4n6cZRUonfSbpxlFTid5JurPmKWPp9aq6NAxyLZd1/u5vRnUoq6/7b3YzuVFJZ99/uZnTnla+Ihd+n5to4wLFYzP23uxndqaRi7r/dzehOJRVz/+1uRndeuWqZT4XfZ5utHwEc+VsxP5W6XemL+als9/tifirr/ati2Y9S8+1nAGOxnJ9I3a6kcn4i2/1Uzk9kvX9VLPpRar79DGAslvMTqduVVM5PZLufyvmJrPevikU/Ss23nwGMxXJ+InW7ksr5iWz3Uzk/kfX+FctsKvpRtt/UjwFGYjk/kbpdSeX8RLb7qZyfyHp/5OPvq6SSP0u/4xWAT1JRLEml/dWkvVeTSvurSXuvJpX2V5P2dul9vKVS/272e18BiGKRz0jtbicOHZZ4/7bP2dz6NhIL/B2pW+3kJ9v3w/Lu3/a5MrefeQXgk1jc/bejjObrex04MSruJf3bPmdz69tILOvK0VvK2Xy918HV9t+xsCtXZipX5yq7WYAPsbj7b0cZzdf3OnBiVNxL+rd9zubWt5FY1pWjt5Sz+Xqvg6tY0E+kbrc/AaCJxd1/O8povr7XgROj4l7Sv+1zNre+jXz8NhV2/+0oo/n9jVc2sZyfSN1ufwJAE4u7/3aU0Xx9rwMnRsW9pH/b52xufTsTi7v/dpTRfH2vA51Yzk+kbrc/AaCJxd1/O8povr7XgROj4l7Sv+1zNre+nYnF3X87ymi+vteBTiznJ1K3258A0MTi7r8dZTRf3+vAiVFxL+nf9jmbW9/OxOLuvx1lNF/f60AnlvMTqdvtTwBoYnnPSO1uJw7F8p6R2t1OHIrlPSO1u534ZPseS/pd2d0F+BDLe0ZqdztxKJb3jNTuduJQLO8Zqd3tRLS9x8Ku7GeOkn67pZsF+CSW94zU7nbiUCzvGand7cShWN4zUrvbiUPbXCzw72a/9xWAKJb3jNTuduJQLO8Zqd3txKFY3jNSu9uJS7b5JanUz9LveAXgUCzvGand7cShWN4zUrvbiUOxvGekdrcTX7b8NpX8KNtv6scAVyylkcp7Rrb9dWhgeU/lPSPb/jo0sLyn8p6RbX8d+oaP36fC37KfewXgsljcM1M32qkoFvfM1I12KorFPTN1o536tlj8W+q9jQF8TSztlJrtk+b6rLMjsbRTarZPmuuzzo7E0k6p2T5prs86e8ey46D87+4HflH7ErmSvfQ+ykiaPcpeeh9lJM0eZS+9j3JX2rkFAAAAAAAAAAAAAAAAAAAA+KWk//1An3dK9/oAMNlSsOl/8rZlm6nhN1h2p//J25ZtpoYBmCeWfp+aa+PTxdLvU3NtHIBZYuH3qbk2Pl0s/D4118YBmCUWfp+aa+PTxcLvU3NtHIBZYuH3qbk2Pl0s/D4118YBmCUWfp+aa+PTxcLvU3NtHIBZYuH3qbk2Pl0s/D4118YBmCUWfp+aa+PTxcLvU3NtHIBZlnJNpb9lm6nhN1h2p9Lfss3UMABz7Ut2lHdK9/oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPDjx/8Blp52m70UmvAAAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
