﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="2028884352">
    <active dataType="Bool">true</active>
    <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="4100708055">
      <_items dataType="Array" type="Duality.GameObject[]" id="58411022" length="4">
        <item dataType="Struct" type="Duality.GameObject" id="3646317754">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1791027166">
            <_items dataType="Array" type="Duality.Component[]" id="4208832784" length="4">
              <item dataType="Struct" type="Duality.Components.Transform" id="1711665390">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">3646317754</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="Struct" type="Duality.Components.Transform" id="94231988">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">2028884352</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">80.83333</X>
                    <Y dataType="Float">254.999969</Y>
                    <Z dataType="Float">0</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">80.83333</X>
                    <Y dataType="Float">254.999969</Y>
                    <Z dataType="Float">0</Z>
                  </posAbs>
                  <scale dataType="Float">0.3</scale>
                  <scaleAbs dataType="Float">0.3</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.481374</X>
                  <Y dataType="Float">48.14443</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">82.77774</X>
                  <Y dataType="Float">269.4433</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">2.16853166</scale>
                <scaleAbs dataType="Float">0.650559545</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="1093979280">
                <active dataType="Bool">true</active>
                <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customMat />
                <gameobj dataType="ObjectRef">3646317754</gameobj>
                <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                <offset dataType="Int">0</offset>
                <text dataType="Struct" type="Duality.Drawing.FormattedText" id="3305850272">
                  <flowAreas />
                  <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="1722409692">
                    <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                      <contentPath dataType="String">Default:Font:GenericMonospace10</contentPath>
                    </item>
                  </fonts>
                  <icons />
                  <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                  <maxHeight dataType="Int">0</maxHeight>
                  <maxWidth dataType="Int">0</maxWidth>
                  <sourceText dataType="String"></sourceText>
                  <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                </text>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="The_ESG.ShowScore" id="2585201890">
                <active dataType="Bool">true</active>
                <gameobj dataType="ObjectRef">3646317754</gameobj>
                <text dataType="ObjectRef">1093979280</text>
              </item>
            </_items>
            <_size dataType="Int">3</_size>
            <_version dataType="Int">3</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="4206434570" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="913417212">
                <item dataType="Type" id="1369684804" value="Duality.Components.Transform" />
                <item dataType="Type" id="1432057494" value="Duality.Components.Renderers.TextRenderer" />
                <item dataType="Type" id="1924483840" value="The_ESG.ShowScore" />
              </keys>
              <values dataType="Array" type="System.Object[]" id="451616662">
                <item dataType="ObjectRef">1711665390</item>
                <item dataType="ObjectRef">1093979280</item>
                <item dataType="ObjectRef">2585201890</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">1711665390</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="1744712360">OiqOV9j3PE6O5Xmjm4luvA==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">Text</name>
          <parent dataType="ObjectRef">2028884352</parent>
          <prefabLink />
        </item>
      </_items>
      <_size dataType="Int">1</_size>
      <_version dataType="Int">1</_version>
    </children>
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2934650304">
      <_items dataType="Array" type="Duality.Component[]" id="3412349789" length="4">
        <item dataType="ObjectRef">94231988</item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1736319733">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">5</animDuration>
          <animFirstFrame dataType="Int">2</animFirstFrame>
          <animFrameCount dataType="Int">0</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">true</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">2028884352</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">256</H>
            <W dataType="Float">256</W>
            <X dataType="Float">-128</X>
            <Y dataType="Float">-128</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Graphics\Sprites\UI\UIsht.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
      </_items>
      <_size dataType="Int">2</_size>
      <_version dataType="Int">2</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3727966453" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="699846836">
          <item dataType="ObjectRef">1369684804</item>
          <item dataType="Type" id="3617590692" value="Duality.Components.Renderers.AnimSpriteRenderer" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1389150198">
          <item dataType="ObjectRef">94231988</item>
          <item dataType="ObjectRef">1736319733</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">94231988</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3983929104">Kh/hYw9ifU6lyXkXpsMm1w==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Score</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
