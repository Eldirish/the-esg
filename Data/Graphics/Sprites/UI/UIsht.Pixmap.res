﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">1</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">3</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="4">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">80</H>
        <W dataType="Float">80</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">80</H>
        <W dataType="Float">80</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">80</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">80</H>
        <W dataType="Float">80</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">160</Y>
      </item>
    </_items>
    <_size dataType="Int">3</_size>
    <_version dataType="Int">5</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAFAAAADwCAYAAACXDeNDAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAe3SURBVHhe7Z3NjxRFGMb7RAgrxoMH44kIhogKZg037wKznFRigomeNTEmRkRvRONhFXcWDl69yM5gICEuZAXlwxD5ikhgTUxMDPHmjfAPjP3U1Nvzds3bHzM9vSvTzy95Mt1Vb1V3/aaagSWZjQghhBBCCCGEEEIIIYQQQggha81jr59hMlKIKu4xQymW6IvcgKe/eeCy5+TDwkitrtdtOjNHbg+lTI2kbG1YJ8m75zDSL07gJxcUaHlqYGMTuPCmMkCBDPIDenOL3bP7F7s/NDHiYGSB2L543dteutFqd6/GEi83Ma1jnZ+1D/jJBQV6AN6FuXZ3JXxnmhTtA35yQYEeYE3YtGgf8JMLCvQAa8KmRfuAn1xQoAdYEzYt2gf85IICFMqnjjVh06J9wE8uKAgGNFqiOBhLIHfhYPeNLFAG4hV/H7Imn/Zg3dqDjzeVAQp8kkFNF6ji3BQihVvfPR3ta3duNVkg1g8PpeVp4sEUGK8fHsaCAruX97aXVuN4IyNCgRRYKXPtzi8jCZQ/KDcfOBc9fvBK9OLh8xQYC5z99PvEjcTEd7qP7CfeueVCgUur4kTFGwtABwrkb94Q2Fpc+rXZAk/cgQdxUkqg/IcMBuK8sQLjdYsD+IAXHMOTCTp8QW9m/r4bgAmaKhA/zsf63f/yxT7EDTyZoMOnLzBOkwUiTqB3IfKQTLRA/BBx84GzPbwT1uRNCATCgxaYiwjEM4+BmGD/se4pa/ImRAQWfoAIFJgOBVYMBVYMBVbMoyUQ1ykTa2xNeeQE3ty4sZcXCsyLF3hv06bev99u6f29YYMLjtFGgUXxAiEsFIhQYBjMGwSSRJoWiNdEoI417wRTSaD8MKF1rHsBwb+JJxGZr7XYXYGUMHmPcJhkrgnen0R+mFBZIM6H3vmKwbwIJGhhobxQoj7GWJnHukZmjJ1mBevG3PAwlkAtsY5gbkiAEJFmyZPoGgRj67w/ROSNLFAGYvviGDl45uHE4t6c+ftOQiiqbJzAeA7MZV2jarBmeXxxPJJAkYgJ6gpurKpAeZPrjJJXWmAise5AQt5ja0U/wtacNca5KUQKa4x79OQRlg+HrA8QBO3Wh4jMgzmDa9SSdcffiHssRKCOJVELC+MElv3zaRoQgYlEL1JkirBQIF4TYV6aRObD3FMPFumjF+4CQZBlCUTQb43zcfM2Almszns734ogqOgR/mzbq+Z4pNHEcpzAEvEjSAqIKRtCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghpDIzR24neerza9Hs/Eo01+4OpS6sa+kUYY2xUgtKXvKNaPHF3jdi3tQEY10TsWrTWejg1RqrMzxurHSilv51kVqefF9ojsT/bVpffffy4Pzk4UHSdWFiGW9Y7VlB/ZYvrjhvKYGQJ8kWmHVT+oazasIMj2kdXfrYri2Rxc4nLsl8Rk2Son478KLSl+gPUvKQ9OCyFxzlxqyF+raFzqF9RzsfpfvKJOv6ci3pz6rLj3gSR3FsgShuLXQ+GAyOd8eIW70o+9qdo8n5YueQ7kNG242xkNTuyxOka1VbWOOP9bpDT3CXKTCdghvCq5OQV5edlMwkmEsS9hUlHKPnsuaz6vV5P6EnuCspMDvpnWJfeJzE835ptZeL3Ec5MYOgXzLc76VlC/SNJXZg0Y1I8urkRq30a4Ye4wq7PJzbjvRn1wWuhgVKYfrPwDBlF2HcdPwBkUS3B9nz9Ym3rfbykWuXvVekuFa7grtMgYMUTGp8AORFxMjuMj+cvNzVS7t7yKCvfy96jBzjVR9Lfz+jSAyTHqtdwZ0pMH/3DbK33fnQas8LFgcpl0+91JOFiqhEQCwX5/d/e9JFS5Txuj48rzPaFdzl78Chj/vqwUJ7vaj34K8Zt2iItEThGDVSp9ulHmOlDnPiGBL7InHfk713RLuCuyGBSDio7I4sEyxaxCByLkJ0DaSIbN2HWj1WgvM6d6H4yRSoJdYZS5AWoHeV9Mvu0tJ0P9qta006gaeBQN25FsGCLZHyiIbtaLPEyjzWNeqK+IK7lMD1iEgMI5K0LAQi9SNszbmGSQSuS3bNn46OL70ZyS5CQnFhdO3y8ivRa8cXzLnXKutOLCLJjR+fczIhSu9COUbf3avPpMYQBYT8cX2LkyiPqTy2aPN9vpqYQNCtle2JRJHn23wVyUVLpLwxgTAdQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCyERYvbQ7FeKBjJvnZl3wnaiWKLz+eXUHXt3XvvvjpC8vUw0W+M/qVry6b+W9d2l7dOenXe4cEVE+ydcf+37XrsXevbgjuntxZ3Lu5/ZXm0Kw67BQ+VZyHEOkFnV9eTaRIl/AjWO0oQ/HUo+xUoc5cYxrTC3+sU3EIHIuQiBJ2vRXwOs+1OqxEpxPtcB4gfEju80UJAJ+v/BCsvt0P9rQp6XpfrT7uf3VphQsUCSKjFCU7DDdjjZLrMzTCHkCFopcW37eiYSIMCJJy0IgUj/Ct88/m8zXOLBovRuRUFwYXduoXZcFBEj4e0QqAiH8PSIVgSD+HpGKaImUNyYQpkMIIYQQQgiZOqLoP20liTvte9T/AAAAAElFTkSuQmCC</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
