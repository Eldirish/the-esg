﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">3</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">2</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="8">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">32</Y>
      </item>
    </_items>
    <_size dataType="Int">6</_size>
    <_version dataType="Int">22</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAGAAAABACAYAAADlNHIOAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAceSURBVHhe7VvNb1VFFB+goEZaoLT0taFW2tdq+Wr58uPxTQAtSC1oISIgIVhDImqMJMYFsHJFWHSHEly4IGnigg0rEnculJi40ujOZf0TFK7zOzNneu7t63v3vvv6+Mj5Jadz58yZMzO/M3PufbfvGYXikWPz0cis3uXkmfWR1yoaBgTASmTJj9pf0yA0HAhA8cBMEJpWXfUt2YDASfF+nqxoiklLmVfIE2CryQCkHr/YNgHiyQ/keMeEb8mJhcvGUUhCkpOKtY3tumCmr++EPjMWrbwYW4RVJceqiORcfb0iEADk/47N982i1uyk8aYpdV01t5tNhGD+Zsvzg5PkOxd4QUlSSv3LzLWT/Wby1MCsdkt+dOvi5cxBkGNhISibCtfNwuf3hGOdArG5oJ4mCIAd05RWujks7p4K46FkKQcEAGNBQDxKDsTAQTpduQIRHEJALgT66W/dNUTa4NrrnYf0CH54IdCBQFxbcVaVEa1pPU07kecCnWuqjHs2ANwHwqkI43od1ZOAfkfvFPU93hbfsAgCBCesVpAjJhkl69a++XCWjomrJQALm0dQUH8s5nDhNNVxAvwuQ30WMA7SngXNo6OZTkwsiAuWFNFeEQgA+mBsW42aum6QHuOyzAXbh+TT59yJWLH2Lt1X3vvcBaKwfe6+1RAIQQliQTxkevcCVzqyZ9nd/nC9d5EJ1B+TP1Z0Rxu6rt53zejEv85CAOR7oX59q0bKpjLUoawES765uaIYTgHSEHYuyMvyGQF2sB8cicy6w5VPTzXQxOEAhPAiofu9tCCyhFDJNpg47HANW98/C4Iv7MSzW6fCqcBYO078h2tJeiD/zrkzBhvi7LqJcCOX84EP6lwFJ1bP5POlw4489mHFW1UG7NheSi2IESIXA0Kw+1GyDdrlsfeSBTFfH5WmzLLui6THWMMHf8Z1IF0EwEx/MYgAmDOdZDJrPtjNaWBtSZZvicyLeyODcWUAINXAdmlsqyEshAV16Cn12JswyqQd9GwHJxmQHI/yKPtqL14hIxB+6Y0XSBjIu829N0zb4HeoRi09V83ebffNhY03zMduV5NdNcAO6aPffzCDrDsUGf8JGXPxlo1BjBC5G9AGoqUNB0XawEkGBF+9238y3aU/TffuyJVb75qXtk+Z8VN/mC9P3zJnXi+QPf74p5XYXDtfjn2YSo3htx3RvB4qbS5vf7Xsro5VrG0QALaYm59fTaBFYSI8Gb6row06toEe7Xwq2AZO0oJ38fLiJKph7KFRtxt5bKSfQ7s+IZtnW7YYPK1ADxvYD1t7vFrgmzH+pAUCwGvBNcjn9UgiWwrjBicSJWPjWw9pnuiDG7ftQ/OyTZnmIBEWDMcQdohJlUtBOU8AYWj1UhTko1CKzNCReCrCmO8f+56IWdE9YRb33KWFMnEbLBErt2VfvLU3m8bcWKUTjnw5rgyAHZvGRwlYO5ondNwH0tQ5mf8EwCGCIJ2jzhJsPPlsg/5DPURmJlwb76ebLPvFzpJjd2z6xQwfeWDwVNQ6QM/qpIeAQORs2PMcYJAG1p6C1rPH9S+88jfVIcmbuG2PyZp9LljoJ9efh3yAHMEpCIFjHgR6PAFxEKDnE8HtqF8anblRpoX1FwIAXyC03NhrRx6YJX33iSS04amF7aCzriL/GJsKtk/w1bnpR7q26thriXIolNwGgT2XrcN/5dr5BPmBhknmAVjHJbfLxY/tu2BsEOAjE6yv8MkWvvr2z+wuHnv93n+IoHPv3AmvkilnWxs5BzhJC9uHnoAg7YPuZKV5j4QA8LwgPv351pyQrweYZB6IAwCRBEF/YNtJ6p8H2EG4wbZsds/kcuw9+3+lU4KXfj98dZlugJyzeR7Iv42AHcv4FFX1tNSKsHAskOscANQlOajLZ/Q8QBDYrxy7f8M35uYH9sOXncP1z25RDpYnBHa5U8DjAk5FTDhK1FmkHuLTR13gSQzE8zXPaXSozRw7eCV2Oqg9Q+5/IsC7jeXro32kP7+zK6a3Qjl8HhDIp1ocseB4ebrAxEqppG8gnm7iFQqFQqFQKBQKhUKhUCgUCkUDIb7eMddbx1hbmn/jKVKCyed/doj37hKz2jUI9QORii+6Jr5pHJM5bBR1ABGa/NItrvl/sFKXsFPUAUSo3NkgHsK/D4CUs0NnRT4EYvFPb0lutd8HcF8rihwI5Dbo9wGKBAKxLEwuky/TTzk7OFHUjhixIFuSm9z93C5t4ERRO4hIEMvkVvt9ANtqAOoDIpJ3tdzZIFlT0PwjRizIZnLlTZhtuJ1t0N+5UdQKIhLESnJxzUHgdtnG7ejv3ChqgnwPlCRfBgAig4A6+un7oDpA/j6ASZak87UMDuyfum8nP2LEiJZ1JlwGBnV0UtQJnIogguCYSL2mnvmDJF1iLr1CoVAoFAqFQqFQKBSPE4z5HwA1nGDxZiX8AAAAAElFTkSuQmCC</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">2</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
