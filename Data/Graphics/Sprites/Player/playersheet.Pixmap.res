﻿<root dataType="Struct" type="Duality.Resources.Pixmap" id="129723834">
  <animCols dataType="Int">4</animCols>
  <animFrameBorder dataType="Int">0</animFrameBorder>
  <animRows dataType="Int">10</animRows>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId dataType="String">BasicPixmapAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="1100841590">
      <item dataType="String">{Name}.png</item>
    </sourceFileHint>
  </assetInfo>
  <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="2035693768">
    <_items dataType="Array" type="Duality.Rect[]" id="2696347487" length="64">
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">0</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">32</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">64</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">96</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">128</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">160</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">192</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">224</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">256</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">0</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">32</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">64</X>
        <Y dataType="Float">288</Y>
      </item>
      <item dataType="Struct" type="Duality.Rect">
        <H dataType="Float">32</H>
        <W dataType="Float">32</W>
        <X dataType="Float">96</X>
        <Y dataType="Float">288</Y>
      </item>
    </_items>
    <_size dataType="Int">40</_size>
    <_version dataType="Int">50</_version>
  </atlas>
  <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="876525375">
    <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="295733828" length="4">
      <item dataType="Struct" type="Duality.Drawing.PixelData" id="2142472772" custom="true">
        <body>
          <version dataType="Int">4</version>
          <formatId dataType="String">image/png</formatId>
          <pixelData dataType="Array" type="System.Byte[]" id="2753792580">iVBORw0KGgoAAAANSUhEUgAAAIAAAAFACAYAAAB0oXTLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAB+JSURBVHhe7V1rryVHdT0iUQJEMk6MgwyDMfbYMbbHg2f8GmODjY3Mw1ixMEEZEl7GBoMfM+OZsccRIkRWhBSkSJH4ECHlG98i5UOUBMRDxI/wCCExiSECpMzTJH8hinTTa9dedXfV6fs6px59b9WS9q3u6upevdfeVd2nb9c5s63gg3f/JooVtRhSr206dhoY/JWX9q2ZANzWk2BnQgLMIO++4Ndd7QBdDrajomNnQYL7NyifcUF+zxXnrQZ/qJNtPQF2LFZWHppJkG0SSL0GX+qHNlrfsZNw4HVSrHxeA80ksMHHNrTRth07DBLczSSAWsdOgb3OM+A/1HWUMJsQaGtvEju2P1xw9R6AQR9NgqENk8Dt2rETIEFdL/g0jgTYx+3asS1xlSsYSH/tt8FGoOMkQJvoXmBFj9WxXfBbrvCBHAv+7Io/DMxus0nAcjeO2DFtMPAwBt1aEPh3/mVoJhHi/ZgEsJ4IE8QHr1n9Jw+CZYNve/Zc4O/9O2e2TpOAZpPAJoJydtSG/yfPcC23gY+DD/MJgKB//ORqAnB5JAFg9pjg4E1i/6fRNCDP7xmYscDTghGAgbfBXyMBaDg2Ofr/DKYDCQST4PTB+cDhsz1KSYB4FIh6PxOA+9AQdBw7Cn5PgClg98XuSd///uDVPgkQKAmWPvyhbZQAtq3dl8cEB7jec9t54OyYCj5w56+g8EmAZbks4MneYFz3CcDAMxE0Abgv9+O6Db5ydUwNNglQwhhAv8wE4CjA3h8lQLxvD/42AZMApoO0D+aePXvE7rnnHrEHHnhAjOvcbgO/C0fQ5R787QsJKq/n6OkIPEcAWR7quN0kQMcOgU+AjUaAngA7EPYfQxwBYAw8123w+yPfHQbeD+DjHEcBOwKw92M72un/FTp2AjgCoHfz87x9yBM/7OEo0P8NvAPwvoul8PcADHKcBLYebZkEun/HdgSDz17NBLDG4MeG9tgP+/ck2L6YCz57d5wQY/VYZhK4w3VsN8g/buw/ilBHwzq2jdWjPffV+o5tCAmeDfLNvy31AANrbbbrN/B3NTmwrNaxTWGDuNlALrJPxzaCBJZv9PCtIrVSED78W5n/zlbrKID42l7jWh9cYirwt4dbXDEmeBAMqcmENc6hGH/rkI93fKnzwF53xzdA6gr1Qn8OPA8YuAvxNw3/GR/LMM4lsInhqrIhOIcbXvtKX1eIv13sc0UQALuOz/5alxO1+ZuHFxw9zj4KZgAyTw0P+GGF+ZuG73EIPBOAScCAoE2mmT+b5pfWHUkRiD9mEgS8DZwvCMIxlgCybrj7KJAWo8GPAwCTJKiUAFKXNwGbhVxfN5sA5mYsdRCC4Ffgbwt3uUKEjIPPeQAwW492GYPgz2ML/B1LIBCX5ieC0EwQGIAoCMsH4lXyb0g5ruflpJQRfj4U0v8TdGwVb3SFF5bmxWcARoJAS5wEq7ycikb+iBuXBpsEbveOrSIQlRYIT9sgAcxN2aLfCeD2Z/Bj/hFuuUdYTQBYx2axVu+H+SDYQKyRADAEQpJguWf1q7y2XIObN4d9FFgcoaA60xfLPvhMAA0AxLb7yH46FDMBcNyFRwDLuwE/eXsCLA4voAg6BJ/r6wWAxvYMxpLBB0JeGEeAgZv3GZ5T+TEjuSfAYhDRICx7FddFeIo/kgAMhh8x1JYIPiAzj3zgLX+UAORlAqB+Se5mIeJRXL88CB4EQROAiTLX3tmykATglLSAX7nJZXs/1vuU9CUQf28ghGYQODeQgWECxEmQaHqYO94wAnAuIucl2uRjux789JCgcpiH6CK+9kAmALcn7PkWwsPez+DbBLCJ14OfFj4B4hEAhmUGIWcC4NgxN3lhaHOJa9uREjrX3ycBex6DwV4o29QyzAyW48f8cQK4ph05ILN9EACOAuyFvARw9lCOL4Czr6NZ/p4AhXDH66WQAFjRaajnnEC0yzQjeE1+bFPryIGLXOFHASs+g4+Sy2jrdkmKNfmxTa0jE3yAaRQ/rsv09E2OGXPZdW3TkQkiMoZ43gzSWIfeaYKfOhgb8qONa9qRAzawPthmuI8tKeyUMBr4o4SDdRRALHpp4Wvzd3S0Del1/C+blqV7ovD17weog/hOP14vgeD6X4G/WYwJHgRDb9hyIj6H0vxNQ8TGmz5YRoV+T4B/FYv1GeHOwSYA3jwqx980AqHtOwP8XK7rORGcw5tf/atSV5C/aQQPYrAOs+v6j5uc2PAc0KgjMfgz8lZ8efSrvbGE+GPnYIf/EufQJPjrolZsBJ8JwOfyaKOWC1M4hybhhDevfI9ZzgCw9/M8LK9NAHMOHQkx1+ti8WkZk2A0+LCeAHkhgvK6u5b4NLRBW90vFTY8B5sE2r4jAQLhY/H5fh7e1LX1Jggr+j0Dy2LT52C4O5bEXPCt+BTdGrdZ0+MsDD7rx4OfNc+Bk1QGw3a01+8U6FgQQfBtQOeEN+LD5toOpjOOF4UPfny5kfOw52Cnj2F7x9Zg/7MHsTedACYIcVtp72wRuHPRx89zx7UJQGMSYLuzji3AP2dH8MdE98LbBIDoKvxo+8EWHAV878dxRkcAngdKBj9Mgo7NgiMAE2Bs2IWhPhCfCTAY28Qf13BcHHyL8AkQn8t65+ANbTq2hjgJIDoDYG0t8W0bPjiS9s62CjkPTPjk8XzwB8P5yXnYEcCaa9exVQT3AoPoMCxzVJAA2ARgAAaToAzG/biutiXwPJgACD6OOZoA5hwQfJlM6tp1LIEggFjmEBwkAIUf6uL2XB5sIfAn7e0ogHWeSzACDIZpYwh+T4AEsD8cbYMPo8iYowfRKTy22cCn+CFpmwQoYTwPJADnKXLOoBn+YR0JIEHl8M8EYO8T0YdlbjcJkAxMAphNRjkHTQKOAr33p4dPAArP3saexxEgVwIQ57vCnwcsPg+cA9q4ph1Lw14KmATodRScvdAGP8XQvw6CBIjPoSdABui8f5kOBtF5/WfPY+8308VyIkiA+BywvcCrae3A/ow8AgyRbQBoFF+/TyAnhGe9c5BWHcngAw8bSwLWo61+n0BOCM965yCtOpJBnshRZGtjdWjvdsuGgC8+B2xX60gEERRJgB6GSwE/FdBsnbbPiSmcQ3OgqN6QDEyKke054XnAD7N1sD5FLD/mRB+sNKZwDh0lcdGe82bv+9O3zD79jQOze7901eyq971udt5F8hOyxQDOz528S+wDX96jtTsc9j+ECAIF2HfwDb5e22TB/e+9DIXwDLyBsV7bZIHlv+ORK+G7cGOZ9Tn5q8K+I4Dy7qff4gXAst2WIwko/v/912dWvvijj6x87Ks3ef7PfO22GeqwDW1yBIH+kwN8X/jZ+2cwLNttOTtBTfibLTis4nMUkDpuR1u3S1KIwBQ55o+3Y4fEEN94/N952xsQeDEsow7bMvpfHeIgP1793lNX+R743k+53oltOQOAY//9K14hxwfv2w9dOtt7/+t9AmBbTn4El/yXnP9rrnaALsu2JhIAr4hh3RpfG8sdgI34MwagNn91+Ne7KII11Em9vq3jdkmK1vnr4oOXu2GOAlgRuI5taKNtk6J1/qpY67sBsIySz+G5HW11nyRonX8KcM4Nwxudp82JMLShCG7XJGidvzrEqfWcp4kI6QVonb887CtgMF77rLNwNBYBbXgtpC3y8zGt81cFv/qNjow5z/fvaHabFYHlVt4PbJ2/GuLfCYQj1gLH9fVrb0aIeD+KAFtPiNb5q4F3uTCcrHUeDtGs43c+fXh+OpYKYfexIlgh7F3yIvxiE+B/55/9cVi3AH9V+H/yDNcy63jsPMwLANEx84YB4PKIADB7THCAC5z2m763yi/CK79fLsifwv+p/NNIHl/yxMYcp3kBVIT3/9VfhM6vIQANxyaHeWS6FL+cQyV+8X15/6sjEAGvdsUnjs+2KEUAijAmgG63+9DgNI4di9/5JwAOg5h4SRFwonKygyNchm0kgG1r9+UxObkTwy/ROv8kYGffQgQso4QTMK57Aeg4hVABuC/347p1fuyHnVvnnwSsCChhdMAvUwAYBeC6ESDedzPOt84/CVAEGOcCwuAM5t3BOCE0npHL7dbxXTiCLm/G+db5pwpxCsMYDJkOx9kDZHmo43YjQCq0zl8dXoCNekAmAVrnrwv7jxE4iGyH0XGuW+dTPvJsnX8S4PUQH2fYC2wPYPZjO9rpc/VkaJ2/KtgDkN1wEI7ahxzxww72glT/Bm2dvyrsF0PAQToZi2Dr0ZYi6P4Lo3X+qqDzcMwKYI3Ox4b22A/7LypC6/xTwJzzWKbFDsf1WKYI7nBbRuv81SH/uLD/KEEdDevYNlaP9txX6xdB6/zVISdvnbx59Uc46Ji12S75FdlVcbCstghk34b5JwHrxGYdWWSftTB6rPN3vVLm6N/8yYulxLrB6D4LYvRYBfmnBf7rVE3At4rUsmL3Ja9BIVycnMrJoWpZUZt/CvBD2+7bL/BfDuHrXILkhJ+A+qGv7A2mhqPUAOWE58LMZHxDia0r4H9ViJMMNsTHV7WwzlzzckGOD7Hj3oc6BgENM0GOP+KncBfwvzqco8/smwsA6koEQBJt4MIyuH/3z6929RX4XZVA6ppIADiKz7hYvu/E5X4IRl2RABh+cN92n3wnqdQVCEDAjw8E+qGgFH91iINwliLY9ZGekRqt808CgcP2USgefmC7/sp3LrTOXxXiIJyF4xSAImAdhjaZZr60zl8VgfNjJiLgbVgVQfZKh9b5q2LUeThq12EiQnoBWuevDrm+bVYAXgvVUqB1/jrQ3/0XR2Ln+R48zNajXSoRWuefAgLnaH4iBM2IQAESiRDw0hrirwf9pe/AcVjguL4XH4tAW0aEjfjt9PAa/ME5ZOCfAuYcgomzFH6TATA3RVuZEz93PBiF30oCpOYPfM/HXw9rZT9MnMVkSDs5cg0BYHBeRMAz9E0+Kt2IP/heggr8uf2fAkIndKYrlr34FECdR7bbfWS/wXnfC9T5TfaA8DgT4vfcefmrQ06cTsF5rq8nAI3tKcICzvtjNcpfHXLCyFw4AeO6OAyD8yMCSJvBIAL3g23R+db5J4FVp+3y4GwgggpAoebaO1sE8TFa46+P+Hvz4CDnxXFuHNZhFCAWYZnpUa3zTwniFIc5ZDyclx4wLFOAeBh0uyZB6/zV4QWAs7YHwLDM7M8kQOv8daFz3b0IyHoYhcAynUc7WMqhr3X+qUBmu8BJ9gL2AA6BnD2jc+lTo3X+urjj9VKIAHA2NtRDAA5/qWfEts5fHfLurToZi0DnUXIZbd0uadA6/xTgHaTR+biOT77cbskwyjVWt0P5q0McxBDHmyEa65D5xvksAWiYvzqsY95ZM9zFlhrB8RvknxRqO9w6f4edn49Zuhde7r6ZoRTIX4O7OvQ/Wz7z4/XcIB8np3J+olp2IPADPPdHH5XPfMX4pwB/s6PfjFH65kf4ID6+I4DrKNVywydd/P0IajsePgC79rlvzKgVAJal+Q0f0FQCiJMU/sBDb5I6CBCJkgvCQa4r3nWh1NXil5qy/NXhnH1m9UsiUIfPw6UCQH4s1+Y/X+sK8leHOA+HsQzDsgigoqBRRrTOXx2BwxQDhqdh2F5yfn6D/FUhDsJZeeY9OI+Sz8QpiLbLgdb5q0McHRNA1h9ydWiXqRe0zl8d6woQi+B2SYrW+asjcH5MAGzntVAtJVrnrw4vAN+H5/twVoBIhJRonb8iXiXfiCfOifOwkYkRFICfiZN9dWrr/BPAquOYF0cB9N14CgDD0GhFcLsvjcnwy9T08vxV4Ryh+HR8DQEoghFgWREC/uC7ASrwB9xl+Ktj1XlbwnmUIz0AZcJeIDwyNbwif0X/q8M5HZsRAM5aAbCeMgDB9wJU4J/jLstfHfPOswcMzvOOlyLgszCW8YvZ2QLQFn91yOwX7zidHxEAzkMECoD6BHPjW+evDhEAJnPhrAiDAHCYItjsx3qin0xvnb86xBn0AMyFs/PiKACM7TI43zr/JCA9gNlP560A7AWwDM63zl8dMrQh+20PoPMwtLnEtc2B1vmrQwSAMfMhRCyAa5oFrfPXxT5XiAC8GRrrAa5ZerTOPxWIANbpyPncArTOXx0yGTIWAevYppYTrfNXhTgIZ2l0nqZtcqF1/uoQJ/FRB2YdZx3auKZZ0Dp/XdziCjophufc5lk3LQta558aYqdLO946f9vY4Lf7s6M2f21Ixkf/4RqrywXh4rxEzg2E3f/ey7A9N2rz14O+4OivexftOW+2+/YLgjqs5MLuS9xUdP5INcT/2F9fH9RhJRfoP7nu/dJVs7cfujSow8pORnDjA+fZA6KboVwQoW0C0EwAsvLDz5Fgl/K/OsR5vPKEZRsA1I0IkxoB/11Pr/58PepMEHJBOMjvqgSl+KtDAoDPvliG+PeduFzqUTciTGqIyOTfd9sFPgFq8GOmgMwWGJYL8VeHOAlnKQIsXh8sF1rnrwed7Ro8BcOj0BFBsqB1/qrQ38IXZ+kwnKcAKI0AyUVonX8KcM7rC490PraMIrTOXx1zmU9jD6BlEqF1/qoQZ3jts87CxgRI/J+x1vmrInA+FoDvxuEtWVuPdtxXf4N/UbTOXxVzzlsB6Lg1K4Jv52wRtM5fFYHzc07ReU6PWkcEHEd/jXsr2BK/n69fgd9yJ+SvA/1vnpw0rm2bTgDaiADS3tmGWJQfQWAg4rbS3tmGqO3/FOCedQ8G5+MbHO9QLAAmTerEydH2g22yFyzMz+8OGG0/WG7+RP7XBXsABUD2j4mA+kAEFQBBwHZ8XobZfXBcHHw9bHd+GLYvyj8JWBHovAgxOISStpYA3E4BfHtnG6J1/kmAIsDgBJzBMnuFOGQFgGF5MLThfjCuq20KrfNPCYEDWKZjgQDqPOri9lwebBG0zl8X+gPI3hk6D8P8OBjmx2GeHAzr2GYd1x9eXgit808J4hSHPwogvWDoARABy9xuBEiF1vmrwwsA52HIeDge94BMArTOXxd2KKQIyHg6Lj1BewCdTzn0tc4/Cehv4svsWAiAbLc9gNlvfko1KVrnrwr9LXzJbjjIXhAb6tFOf2s/GVrnnwK847AxEViPtvpb+ynROn91yJMxOmptrA7t3W7J0Dp/dYhTEAFZjqEQZp22ddo+JVrnnwTomDeIQVFGtqfGHEdj/JNDbadb528X+L1+/G5/zfn5PIf+/QDz67khfJyUyrmBpfk/+ujFMi+R/EiEFiDO4rqHUjPfr2MlMwL+D31lrwShFv8Vd75W6pgEWNnpCATYte81VQNA4QsGIOCXmvn1HQ1xlg7vuU8eedgA5BYh4L/iXRf6OpRqORHwu6qi/NUhzvKzLgIvI8Azc6Lkwhy/1FXiP5915firQ5yFAOaBhyyjXtdzonX+ehibHw+nRwTJgtb5p4DAYTwDh2Gdz8PRRi0HWuevB2a/CKCvO48JYERIitb5p4BR5wsK0Dp/VYhTvPaNCWBF0PYp0Tp/dQQ3PtZ5vguHd+IzCtA6fz3Yr4aNBaDjdmKEF8D91v7SaJ1/CvDOc5gLBIDzg8nUbMyNUyGwn9t9aWyKXzhr87NMy18V4ggEiJ2fE4DGIGC7s2WwJf4gCbHd2TLYEr9YWv7q8NkPh0d7AJyF4yjpfCjCMliIn98TgP3dYRZGbf+rwwsQD4FYDwSA07GhzXJYmF+SAG2WQ23/q0MEwA8hiwD6GVicHwx3xiKC7QFpBWidvy44R54CwHmIMCoARVAhMEsGbXCARdE6/ySgv4Ad9AKsw3kIEvSAwThFKpUArfNPAlYElDA4TwHs/DiUZvhLIkDr/JMARYAx+ykARWAvyJH9rfNPBnwDhgLA4DwMPQDGa6Rrmhat808FgQBwmj0BzhcQoHX+6ggEYPZj6KPz+lv7udA6f3WIk1YEWqHsb52/OsRJOBs7byZJ5kTr/NUhztLoPA3b1XIh4GuQvzrkoxDMOm7r0MY1zYLW+auDDsqzcpitg+lv7eeC52mUfzKYc3ywkmidv6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6OjY2fiiz/6iLcq+IO/vXJ28BtvFvv958r/MgL4rZUGhD/0vVvFSvv/uZN3zY792ztwDisPf/e68vpD8A9/7TKUKwP5ysFvv6loEBh08MP0XHRrfoDrs9+8RQJQw//P/th9Myq41colAYhs8GEMQomTQPZ/6tnrfPDJjzpsK4GBb44fI2EJQOMH/vkKlF5/tTJJoEO+OM0spBAlRHjkBzdK9oOTPZDnUiIBwPHgs9f6DkANPvndq4ok4MAVBJ/8atoqIz7+rMv+iFiC8OGvX6qt8uHQjw548WFIApwL7PhP366t8iEefpmEqENy5sbHvn+pTwD6zfXBtFVGPPzCfhHAkqNEUB782n5tlQ/kt8KT/8gP8ifAp/71agkA+Wml+B//D+c/ee0ykiM7bA+k+DAI8ti38wtAfnLSeSw/+o9v01b5wB5IDbhcin/w23dAcFMHlEjO7ECWk5QCUIQSCWD5KQLP5ZHnb9ZW+cBLQOw/DB8Jc+P4z24N+LFMHYokwJ/89D75+GMN5EiAB755rbbKB/KDkyUEAD/uT3Jj4Jr90Y/fLUGg3ygHkxu03EAC0meU1AD2iRcKfBQdCGcnvnPv7MNf3SvkPBGUJW6CwP/wt24WfjoO7lL8wKef2+/56f9gujUvwGM/BpIfsXjkuzdoq8x46Os3zh557kCQiR///mWzwz8pEwDwf+Jbe+ReAIZzOPHzO6R3lsDBf1i9D+AIcPA7b9St+THw+QSwI0CJS5CA10GKzxMYTFvkBS4D5IcAPI9SCQA/4a/lL/UgCLD8LA9++2KpLwJ7HaL4D/7LlcVOgPcBlv/Ii28rNgLAzwefv8Yn4cPfuAmjkW7ND/B/9nvXCz/iACuZgE4AF3AR/+i/y52pbs2PEy/cM3v8hVv9x8FjL95elB9cSDjww0rc/FqA/ws/e790AvB/8vmrOSpriwJAEuCajwAUJR6A3gbHj/zT7SLCMy/dr1vKAP7C7yd+7D6S4dlESZD/6RfvFv8f+/4BWUcSFIWeiFhJaALMjj/3bhFgMN1SBuqzJD7LkiDv0ZfkX9HDp5/9UhZPgNrApeDId+6UsiQ0AIGVBDn1XiywpsBLAcqS0ABwBBArCXLyZry5wBO8FFRKgNnnf/EelCv4VFISyi+Pfpvs+VMBP45WSgD/n8meAJWAm09aDWjv7wnQKjQBxDo6Ojo6Ojo6Ojo6Ojp2PPhACNbRCB47dbW34ydvRvBXUNr6jkKwoltDQDQ42nJ5RBwrLJ84tT9Y11KsIwOswGoi+qFz18jy42dcyZ6ZAspDLh9kHP/oqRtmJgm4TUrU7/hLBB0s4SQ4dLj1QUDAD728Z3b4v6+dHfmfayUIh0+9VdqkSADy4HhmWUxHmZWnTt4qiSDncvaa2eODob0mQPEXVrIDDkVWxEkKfvTUjb7XQWwEHoZRgCMAg7QMeAwcj8cFB3xlIh4/7c4JdU+dPSDncnhISJsESJDkACEmYsA4Nx9WAsolbwLjfTxYTn4Nwow9+8nTtwj/0dM3yPqhM9dKeeTcXik1WLr3YsD+apJkGGUkyYaS/h89t1/qEHjWHT97o2uHZNQk4LGSQcmEEG/k6CvZxV7NLs3vA4EhdhD2yLm3isjgVIFlGXUwrdO9FwPvKTCko0fz2LjUPHH2utnRM9ezDmWgCfY7/Eu3zxNn9slxUlyOBJbo+H/eJiUMgdBlbZkeEBWOYEgD19GfuMDn5Acnev6RU/tmh8/sFVGP/dKJf/zsTcKPZYiM5WMvXz87ena/XCKWgSTacFz28qMvD+XLLvHApSOCGHxWWzlx8rYZLgtod/zlG2ZPnln9qJgEGPLlgBp8nAydf+yHA/HQC3MBwSAXgs/rYE5+y4kSvQ/LEBnD7RNn94nYUnfmJr88mB5hMSCw6OXkQgl+uRwMycFLAtqdOOs7AkpZfvq0m0X91Jlb5DjJ7gOQAOxtOCEa1lGfc4ImetXhl2QWjuX3AcrBj+PjDhsjwLEzQ6k9EHzHhptBPQdZx7aj54YRYOi1x07LNj3KYmCCP3nyFllmOXBJgmgCrDx5zo1ET598hx8dMRLwMqHnqEddErjhO/bSfO9/8heuLmcCDMcXDnwVjOWHKMd/7nrBoy/KNU/3SAMcDzzskU+ddlzklzvtU054rFvRYYuC+8MQcL3ZE3/104cEGEP9UC8JMrSV7SjRxn4qSQIQyUkMgh/56WrmodRt2jI9eHxwIdO1NwzZ7hICQzIdT+bwABwLduTUdb6HrcWPgGD5yJnrlhZeeflRU47Nmzq9RxCzicFzYjseA2USDAenBXfgrM8N8oATjtFhXn9hSR0egGMZ8wHHMi4Plp83iE+cWX1EuyjIh+PhkwAM6wiu+irtVI8ZOiJGAQ797Ai0ZAAZhnpYrWcAEPzY0OP4WRwOox7LmUYAOfaR0+4m8MmTB4JHsORHiUDpx7ilzgP72gdAODbuL8CjD318O16e0Bb+y8fH4VML1tFJdgww9JoeIKJzuEOvg7MQJCVwPBwfBk7hHy4HWJegD0mBZQTn6LnVIToFhmN5biYBgg8ddJtvg21684n2UkqyuMuWHnGbg47R+BFH67VVWvDY6I06unh+PmrFdgYIljgBHOfpgQM9+5d+hBEesWHUQQeQhNTk5LmhbqclQGAlsR7/EPjAUsFwSdIhwXiZQ1JynY+qcV+i7aXn0zq2MRjQaBRiSfPtOnYwbKBj6+jo6Ojo6Ojo6Ojo2Axms/8H50GNLwNPzIwAAAAASUVORK5CYII=</pixelData>
        </body>
      </item>
    </_items>
    <_size dataType="Int">1</_size>
    <_version dataType="Int">3</_version>
  </layers>
</root>
<!-- XmlFormatterBase Document Separator -->
