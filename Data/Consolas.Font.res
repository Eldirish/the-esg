﻿<root dataType="Struct" type="Duality.Resources.Font" id="129723834">
  <ascent dataType="Int">15</ascent>
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.String],[System.Object]]" id="1100841590" surrogate="true">
      <header />
      <body>
        <Size dataType="Float">12</Size>
        <Style dataType="Enum" type="Duality.Drawing.FontStyle" name="Regular" value="0" />
        <ExtendedCharSet dataType="String"></ExtendedCharSet>
        <AntiAlias dataType="Bool">false</AntiAlias>
        <Monospace dataType="Bool">false</Monospace>
      </body>
    </customData>
    <importerId dataType="String">BasicFontAssetImporter</importerId>
    <sourceFileHint dataType="Array" type="System.String[]" id="649525530">
      <item dataType="String">{Name}.ttf</item>
    </sourceFileHint>
  </assetInfo>
  <baseLine dataType="Int">20</baseLine>
  <bodyAscent dataType="Int">11</bodyAscent>
  <descent dataType="Int">4</descent>
  <glyphs dataType="Array" type="Duality.Resources.Font+GlyphData[]" id="2035693768">
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x003F_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2796879029">3, 0, 2, 2, 1, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1876605768">2, 0, 0, 2, 2, 2</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">7</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0020_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1397987327">0, 0, 0, 0, 0, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2890172310">0, 0, 0, 0, 0, 0</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">4</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">a</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1332515681">4, 4, 1, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1414817956">3, 3, 1, 1, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">b</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2328102827">4, 0, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="572702706">3, 3, 0, 0, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">c</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1552065549">4, 4, 0, 0, 1, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1820493408">3, 3, 1, 3, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">d</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1773687607">5, 5, 1, 1, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2573851118">4, 1, 1, 1, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">e</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="4230456825">5, 5, 1, 1, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3749983420">4, 4, 0, 0, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">f</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="4178741315">5, 4, 0, 3, 3, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1139965226">4, 1, 1, 4, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">g</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="308275717">5, 5, 1, 1, 1, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1315247352">4, 4, 1, 2, 1, 1</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">h</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2109231951">4, 0, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1701464902">3, 3, 1, 0, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">i</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2059465073">4, 3, 0, 3, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="568736724">3, 3, 3, 3, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">j</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1581187323">4, 4, 1, 4, 4, 1</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3239917794">3, 0, 1, 1, 1, 1</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">k</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3393914333">5, 0, 0, 0, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2166964432">4, 4, 1, 3, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">l</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2562897479">4, 0, 3, 3, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2804740318">3, 3, 3, 3, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">m</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="234460809">5, 5, 0, 0, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1592831020">4, 4, 1, 1, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">n</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2412494227">4, 4, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1729776154">3, 3, 1, 0, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">o</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="556731541">5, 5, 1, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2001124776">4, 4, 0, 0, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">p</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3505057887">4, 4, 0, 0, 0, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1204210550">3, 3, 0, 0, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">q</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1833850945">5, 5, 1, 1, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="474941956">4, 4, 1, 1, 1, 1</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">r</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1583527563">4, 4, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="340610898">3, 3, 0, 0, 3, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">s</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1328307437">4, 4, 0, 1, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1650186816">3, 3, 1, 1, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">t</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2637345175">5, 3, 0, 3, 3, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="827690958">4, 4, 0, 4, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">u</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="12794457">4, 4, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="4192443548">3, 3, 0, 0, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">v</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="176358051">5, 5, 0, 2, 3, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1031531914">4, 4, 1, 2, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">w</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2545849829">6, 6, 1, 1, 2, 6</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="767709656">5, 5, 0, 1, 1, 5</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">x</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2720664751">5, 5, 1, 3, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1919379622">4, 4, 1, 3, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">y</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2214660817">5, 5, 0, 2, 3, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="504758964">4, 4, 1, 2, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">z</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2172976859">4, 4, 0, 2, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="208565570">3, 3, 1, 3, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">A</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2681284029">6, 4, 3, 2, 1, 6</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="4278471088">5, 4, 2, 1, 0, 5</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">B</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1852316199">4, 0, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1043355070">3, 1, 1, 0, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">C</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3850747497">5, 2, 0, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="4033158412">4, 0, 0, 4, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">D</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2007773939">5, 0, 0, 0, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3206011770">4, 2, 1, 1, 2, 4</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">E</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3461882357">4, 0, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="89353992">3, 1, 1, 1, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">F</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2079289151">4, 0, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="166941014">3, 1, 1, 1, 3, 3</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">G</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2318159009">5, 2, 0, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1299495012">4, 0, 0, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">H</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="511226603">5, 0, 0, 0, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3517265330">4, 0, 0, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">I</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2487693645">4, 0, 3, 3, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3116007968">3, 1, 3, 3, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">J</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2592711287">4, 0, 4, 4, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1352711086">3, 0, 0, 0, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">8</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">K</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2216806201">5, 0, 0, 0, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="70110332">4, 1, 3, 3, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">L</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3134249859">4, 1, 1, 1, 1, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2598349546">3, 3, 3, 3, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">M</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="206229317">5, 0, 0, 0, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3320887480">4, 1, 1, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">N</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2997693071">5, 1, 1, 1, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2746684678">4, 0, 0, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">O</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3122846385">5, 1, 0, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3511155604">4, 2, 0, 0, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">P</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="202791995">4, 0, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="134008482">3, 0, 0, 1, 3, 3</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">Q</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="206660381">5, 1, 0, 0, 1, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2233191568">4, 2, 0, 0, 1, 0</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">R</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="596608391">5, 0, 0, 0, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="4177416862">4, 2, 2, 2, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">S</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2102343625">5, 1, 1, 3, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1714209772">4, 1, 1, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">T</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="793350355">5, 0, 4, 4, 4, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2482489306">4, 1, 4, 4, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">U</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="459616725">5, 0, 0, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3917574504">4, 0, 0, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">V</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2649724831">6, 1, 1, 3, 4, 6</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3645396278">5, 0, 1, 2, 4, 5</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">W</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3292332417">5, 0, 0, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3158608324">4, 0, 0, 1, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">X</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1297535435">6, 1, 3, 3, 1, 6</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3935270162">5, 1, 2, 2, 0, 5</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">Y</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1199823405">6, 1, 2, 4, 5, 6</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2830763520">5, 0, 1, 4, 5, 5</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">Z</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2093392087">5, 1, 5, 2, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1367970190">4, 0, 1, 4, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0031_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="951634841">4, 0, 0, 4, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1463870556">3, 3, 3, 3, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0032_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="428099043">4, 0, 1, 1, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2616937290">3, 1, 1, 2, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0033_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3752061733">4, 0, 2, 2, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="910325144">3, 1, 1, 0, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0034_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1283298287">5, 4, 1, 0, 5, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="532161126">4, 3, 3, 0, 3, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0035_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2494833681">4, 0, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="496710260">3, 1, 2, 0, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0036_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2909276699">5, 2, 1, 1, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="4048145154">4, 1, 1, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0037_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="312677117">5, 1, 5, 4, 2, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3036882288">4, 0, 0, 2, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0038_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1489893735">5, 1, 1, 1, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1159759742">4, 0, 0, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0039_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1862266793">5, 1, 0, 1, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3666707660">4, 1, 0, 0, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0030_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="505615923">5, 2, 0, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3901249850">4, 2, 1, 1, 2, 4</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x002C_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="4108982069">2, 2, 2, 2, 2, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1100931784">1, 1, 1, 1, 0, 0</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">5</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x003B_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2459948671">3, 3, 2, 3, 2, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3394333462">2, 2, 1, 2, 1, 1</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">6</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x002E_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3255780833">2, 2, 2, 2, 0, 2</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2521843748">1, 1, 1, 1, 0, 1</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">4</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">:</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1789500971">2, 2, 1, 2, 1, 2</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1515395954">1, 1, 0, 1, 0, 1</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">4</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x002D_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="862416525">3, 3, 3, 0, 3, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3322584544">2, 2, 2, 1, 2, 2</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">7</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1148830135">6, 6, 6, 6, 6, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2300390766">5, 5, 5, 5, 5, 0</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x003C_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1718302841">4, 4, 1, 0, 4, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="510149692">3, 0, 0, 2, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">8</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x003E_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="888442563">4, 1, 1, 3, 1, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1431222442">3, 3, 2, 1, 3, 3</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x007C_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="4920453">0, 0, 0, 0, 0, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3943566456">0, 0, 0, 0, 0, 0</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">2</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0023_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2467504591">5, 3, 0, 0, 2, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2526576326">4, 2, 0, 1, 3, 4</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0027_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="294708209">1, 0, 0, 1, 1, 1</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="588471124">1, 0, 1, 1, 1, 1</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">3</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x002B_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2101532539">5, 5, 4, 0, 4, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3715892322">4, 4, 4, 1, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x002A_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1735766109">4, 0, 0, 3, 4, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2769848400">3, 1, 1, 3, 3, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x007E_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2239796423">5, 5, 1, 0, 5, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2932012126">4, 4, 4, 1, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0040_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1506089225">6, 3, 1, 1, 1, 1</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2558913452">5, 1, 0, 0, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x005E_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1679111187">5, 3, 1, 1, 5, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2901738906">4, 3, 1, 0, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x00B0_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="925034261">4, 1, 1, 4, 4, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2789385512">3, 0, 0, 3, 3, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">8</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0021_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1304686303">1, 0, 0, 0, 0, 1</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1097375478">1, 1, 1, 1, 0, 1</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">3</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0022_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2600561345">3, 0, 0, 3, 3, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="612288900">2, 0, 0, 2, 2, 2</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">7</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x00A7_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="4188253451">4, 1, 0, 0, 3, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="447384274">3, 1, 1, 0, 1, 1</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0024_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1051625325">5, 1, 1, 3, 0, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3658338752">4, 1, 3, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0025_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="4078993431">6, 1, 1, 3, 1, 6</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3086271310">5, 1, 3, 0, 0, 5</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0026_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3706780889">5, 1, 1, 0, 0, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3696184348">4, 3, 3, 1, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x002F_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="637610275">5, 5, 5, 3, 2, 1</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1977653514">4, 1, 2, 4, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0028_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1309700197">3, 2, 1, 1, 1, 2</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2628454744">2, 1, 2, 2, 2, 1</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">7</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0029_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3279264559">3, 0, 3, 3, 3, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1665293350">2, 2, 0, 0, 0, 2</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">6</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x003D_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3416309073">5, 5, 1, 1, 5, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="673505844">4, 4, 0, 0, 4, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x0060_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3786184027">3, 1, 3, 3, 3, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="773312706">2, 1, 2, 2, 2, 2</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">6</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x00B2_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2841702461">3, 0, 0, 3, 3, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="604254512">2, 1, 0, 2, 2, 2</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">7</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x00B3_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1670191271">3, 0, 0, 3, 3, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="455932222">2, 1, 0, 2, 2, 2</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">7</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x007B_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2448240873">4, 3, 2, 0, 3, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3513224332">3, 0, 3, 3, 3, 0</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">8</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x005B_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3261602163">3, 0, 0, 0, 0, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1445938938">2, 1, 2, 2, 2, 1</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">6</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x005D_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1928457333">3, 0, 3, 3, 3, 0</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1811580552">2, 0, 0, 0, 0, 0</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">6</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x007D_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2907758015">4, 1, 4, 4, 4, 1</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1935844566">3, 3, 2, 0, 3, 3</KerningSamplesRight>
      <OffsetX dataType="Int">1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x005C_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1516494625">4, 0, 2, 3, 4, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3349280740">3, 3, 3, 2, 1, 0</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">_x00B4_</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3261290859">3, 0, 3, 3, 3, 3</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1221550386">2, 1, 2, 2, 2, 2</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">6</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">ö</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1180077005">5, 2, 1, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="906226080">4, 1, 0, 0, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">ä</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3256885495">4, 1, 1, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2046418734">3, 1, 1, 1, 1, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">ü</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3112167865">4, 1, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="1459891196">3, 1, 0, 0, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">Ö</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="446558723">2, 1, 0, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2436793962">2, 2, 0, 0, 1, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">11</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">Ä</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="2623293893">3, 3, 3, 2, 1, 6</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="2237698104">2, 3, 2, 1, 0, 5</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">12</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">Ü</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="3583727887">2, 0, 0, 0, 1, 5</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="3458672774">1, 0, 0, 0, 0, 4</KerningSamplesRight>
      <OffsetX dataType="Int">0</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">10</Width>
    </item>
    <item dataType="Struct" type="Duality.Resources.Font+GlyphData">
      <Glyph dataType="Char">ß</Glyph>
      <Height dataType="Int">26</Height>
      <KerningSamplesLeft dataType="Array" type="System.Int32[]" id="1806040369">4, 0, 0, 0, 0, 4</KerningSamplesLeft>
      <KerningSamplesRight dataType="Array" type="System.Int32[]" id="4020144916">3, 1, 1, 0, 0, 3</KerningSamplesRight>
      <OffsetX dataType="Int">-1</OffsetX>
      <OffsetY dataType="Int">0</OffsetY>
      <Width dataType="Int">9</Width>
    </item>
  </glyphs>
  <height dataType="Int">25</height>
  <kerning dataType="Bool">false</kerning>
  <lineHeightFactor dataType="Float">1</lineHeightFactor>
  <maxGlyphWidth dataType="Int">12</maxGlyphWidth>
  <metrics dataType="Struct" type="Duality.Resources.FontMetrics" id="876525375">
    <ascent dataType="Int">15</ascent>
    <baseLine dataType="Int">20</baseLine>
    <bodyAscent dataType="Int">11</bodyAscent>
    <descent dataType="Int">4</descent>
    <height dataType="Int">25</height>
    <monospace dataType="Bool">false</monospace>
    <size dataType="Float">16</size>
  </metrics>
  <monospace dataType="Bool">false</monospace>
  <pixelData dataType="Struct" type="Duality.Resources.Pixmap" id="2716248726">
    <animCols dataType="Int">0</animCols>
    <animFrameBorder dataType="Int">0</animFrameBorder>
    <animRows dataType="Int">0</animRows>
    <assetInfo />
    <atlas dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Rect]]" id="3244550529">
      <_items dataType="Array" type="Duality.Rect[]" id="1003990830">
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">7</W>
          <X dataType="Float">1</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">8</W>
          <X dataType="Float">12</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">24</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">37</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">50</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">63</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">77</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">91</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">106</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">121</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">134</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">147</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">160</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">174</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">187</X>
          <Y dataType="Float">1</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">1</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">14</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">28</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">41</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">55</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">68</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">81</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">95</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">108</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">123</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">139</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">154</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">169</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">182</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">198</X>
          <Y dataType="Float">31</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">1</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">15</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">30</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">43</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">56</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">70</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">84</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">8</W>
          <X dataType="Float">97</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">109</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">123</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">136</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">151</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">165</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">180</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">193</X>
          <Y dataType="Float">61</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">1</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">15</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">29</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">44</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">58</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">74</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">89</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">105</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">121</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">135</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">148</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">161</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">174</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">189</X>
          <Y dataType="Float">91</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">1</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">15</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">29</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">43</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">57</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">5</W>
          <X dataType="Float">72</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">6</W>
          <X dataType="Float">81</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">4</W>
          <X dataType="Float">91</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">4</W>
          <X dataType="Float">99</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">7</W>
          <X dataType="Float">107</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">118</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">8</W>
          <X dataType="Float">134</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">146</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">2</W>
          <X dataType="Float">159</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">165</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">3</W>
          <X dataType="Float">180</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">187</X>
          <Y dataType="Float">121</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">1</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">14</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">29</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">45</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">8</W>
          <X dataType="Float">59</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">3</W>
          <X dataType="Float">71</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">7</W>
          <X dataType="Float">78</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">89</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">102</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">116</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">132</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">147</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">7</W>
          <X dataType="Float">161</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">6</W>
          <X dataType="Float">172</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">182</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">6</W>
          <X dataType="Float">196</X>
          <Y dataType="Float">151</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">7</W>
          <X dataType="Float">1</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">7</W>
          <X dataType="Float">12</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">8</W>
          <X dataType="Float">23</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">6</W>
          <X dataType="Float">35</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">6</W>
          <X dataType="Float">45</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">55</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">68</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">6</W>
          <X dataType="Float">81</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">91</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">105</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">118</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">11</W>
          <X dataType="Float">131</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">12</W>
          <X dataType="Float">146</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">10</W>
          <X dataType="Float">162</X>
          <Y dataType="Float">181</Y>
        </item>
        <item dataType="Struct" type="Duality.Rect">
          <H dataType="Float">26</H>
          <W dataType="Float">9</W>
          <X dataType="Float">176</X>
          <Y dataType="Float">181</Y>
        </item>
      </_items>
      <_size dataType="Int">107</_size>
      <_version dataType="Int">0</_version>
    </atlas>
    <layers dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Drawing.PixelData]]" id="3416008032">
      <_items dataType="Array" type="Duality.Drawing.PixelData[]" id="1887721035" length="4">
        <item dataType="Struct" type="Duality.Drawing.PixelData" id="1462301430" custom="true">
          <body>
            <version dataType="Int">4</version>
            <formatId dataType="String">image/png</formatId>
            <pixelData dataType="Array" type="System.Byte[]" id="4003657952">iVBORw0KGgoAAAANSUhEUgAAANMAAAFKCAYAAACHGWrmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACiISURBVHhe7ZyNkWQ501Y/A3AAB3AAB7AAC/AAD3ABF7ABJ/ACZ17mfOzZyMl9pCtVd/X07GZGnOh7U3+pVKakmqiaf/vXv/41DMMnEJXDMNwTlcMw3BOVwzDcE5XDMNwTlcPQ+A8/+E9Nd8N//INU9rfBByb6n//4+1OFXwS2QCr7KvQJgZTKPxPHks8e035T2RPYovz3H6Q6OxhXSTaQpK/MufsMfmn8+oCTlP/7g1ec9pkoqeyr0CcsUir/DAik//2DLp89ppLKnsBG5X/9INXZ8d9+oPDcy53/7ZxrzCq/NG59+K8/YFIkkvJ/fvAVu3JCSWVfxVckU/U3/pePXKkSSio7AV+QSK/YRQz9zz9I8fRqMhmzoB+/RTJVOCpJJOR//CDVeTdKKvsq3p1M9Iu8stvfoqSyX82ryVRxrb5dMkG9J6fyV8Fh8LTDKb5Tn3Y3J6Vj3dyjHQduksl2N2P9lx8grwaAdnaSjxTfT/z56meS6sPOzclUx6/6xGkyYRvwbP+W9fdrovIPlFR2Aw7kiO/C0UxApTYI5Uy8XoUQjvfUBhhLx1Zh998FDnb0cZSdg1M7gsMF69RA0yf8VQertlI/gyShj95GufFn8uNJ4pscSZJtKZmwU9mtt5wmE2MxfzcyhNio79zKUttH6gvB5oIyAMLAtc4r0J/CZLqzU5AjddGZoFdPJC0K1L6pX99XbaojGZM2J2P1BaBdtTnNq889CXV6OyGwlD4/hPeUjEi1rduaTpxXP5Pw0cB2oiRfWm7ZbSLBTTIpdf481/enDS1SX5hMl9PJ7MAw+unBZcImBysETJ2YuzK7ea0PBlpvY9nKQTqx2+ECJfvAdr1f26V/uaqBZnv+qoPd51TKkTomfjX5V9ewKrWta8DGUOt3ToN1hXYnX9YybFNW65U4tc+x8Dl+w9cK7/azWvMt9YUOEAby6lHLPwrG0qd4zUnjKD0BLSN4uj4F2hOMjaTk3DnWRScYnU8FwZ7ernIaABXn2PUGBWP3MlC6b05teMXWSk2YVVm9vt6sIZza51geErbr7ys/bqkvBsGrDltBQrgDJkmGK12/K1sF2o7dnHeOtd1OsKe3q9j/jb/dgPAnpwl22A+S2oDS9ac2vGJr5SSZlFfGOLWv22G71fsV9YXdgMHM0s+iOotn8ZqTDFe63rLdydT1OxgbSYuwc6zt+hWtsruugf2nsVfUz2ldduumdP2pDa/YWsEfSPKlZczN6+ptDJ7a1+2w3er9iq54qZMHlH5l2xmudD1ORtK1zNMvfVaBdGVkbKT35zUOSfbx2QRhzF52ymkAVAgGEpg58gwk7eqzkihdf2rDK7ZWehCvylgjN9mbhDq1r9thu9X7FfXFe/dHAiSh4ByM5K/XFSQZrhA0lEP9sJjaoFPo32sQ47FAqQ0oOJI6OlRZtXNh+OvcKil5K47D31SeQNi9tbWS6ovS9Tsb2FDs2/Xir7qnBK6cJhPvdSPbfXZ6xb4vS6YaHFX/Uey3y8k1L8lux6oJ12W1MPSXxA/EK8fSn3NI8rQgu0Be4TVoJSvfKF2/s2G1bsqN3T2In8pOEuoV+/pYzn/1fkV9wWgGe/pn0leo1xJ3D8dLzmJXoQ2npO1OrjNA39S9acecHQuHcqrs7BPqdTtl1w4IfOrtNoeOmwU+rGPVpF7tyND1OxuqDxM3dlMfSbY5TvcXa4Ie36ZT/hX7+ljOf/V+RVQO3xIWGiEgUrmn1ku76iexutpqWyr72xCVw7fEKwiBWT9LsoOTYMrTZ7V3gj2cktU+bfvsz+LfjqgcviVckZ7k5ur1DrgereTkiv5bE5XDtyV9RuMZ3XcJVk6j+pkO237lafllROUwDPdE5TAM90TlMAz3ROUwDPdE5TAM90TlMAz3ROUwDPdE5TAM90TlMAz3ROUwDPdE5TAM90TlMAz3ROXwYf7235D+TfFnISfrY93Ktl1UDh8C4dvSqWz4ddSfh/CbsFSnshJ+rxV/jf4XxfBhkEmm74c/UvTv0+mk+FMSqPKX9j5whNXfvXus3fwO5ego/EEfi+fbsWwDvNM2tceW3ncff4fjPNXXFkDY+aoOkl8s63rtTm2e/LUrR7fqt0KdNGftWvmDstXYH7HbshWrPiucKPz2i74QfmeV6onS9fSBMO5PZT4gZB7HV/8fd55+venPqavQx18G+wOEsSi/HSvZp6Tx6v820+2kn1VQYUcfh/dVEJ0I46/adb22pjburKtg0O5VgCH4P5UBvkLS2PSpdH9r8+rn6R+xu58KXdLaV5yT8cVYT1c9peudx19iwYcuGF+DaRV0doxgXJ90Cr4utANl5RgSScE2xnpq5wLXetXGtPA6HqFu9UUKMLBe7bvqIG0UStdrdxqPtUCYUy9z1+WXrr1MEOzx3dPGd+e/CnrL8YmB77jYtEpi7aZdL7PPld3EWfcnILsxhX4R6/m+imtQsE2Md8b+SxsfqtQE8EhLH7h0DtKTRmNTsCo4obZj8ZCVQw3obouBx2SrvpYhPOtM/iqrNj34sTX5oYNEZweUrtcG/vYyMJB6MLjYyRdiW997YLkOuz7qWuFLN6seBx3jaRUvu+Du0KYm9A6krokbM/6q9SorWSavD0qf5G5RdehqB1NW+mQQgrFdz8IiKdG0cZdMyUZ3u653LBaKhDpZrAry7mRaBQOS/Fcx4Xx3k9JHjv0U2CaGyZlO3k6yG/8iaeNd4Ryekhccs8cAkk5JUZifKDGJe8OfCn+wW1TLVjuYg3e90vW7MgP81o4nG1cQGFVY6JNgAYS5p7KO0vXaneYrLGgNBoPmyc7qE4IRITgNZk+J3q5jEiCn8wWT13d9fbpG1j9JJHA+tGMMMT5X/Shdb39/2dh9UH4q/MFuUeui9DLw6O96pest251Mt3Y82fgEAarzkJPdE/mKZLKOwaCdTyepvuQvuzVzMhFpi+1PpxvQtsppcNuOMXlnvN0JUdH2040NnqSf7r3dquwva1wLkZ8Kf7BbVHeIFGBOOgWV0vX295eM/4H99bHcWRHq1DLQ/lS2IgWjQYY8BQ1yGhxuOHVMrlfqd8lEPcRgQJLvOvqMvpmT1x+ENUD3tGnYB3W1gzk/JTJQB2EM254kR7U7lSfcJLANWzvIaq2Urtfmv/jIB+Wnwh8YjGkCOgWhYwwnaOuO5e5TUahHffAOjKwCX8EW6mibktpZZ9VnQkdX+1hsA/wpmVgcBJ/YHliEXtcFta4bipL8XqEd4xk0T7YJQltEu3jXnt24dd1t65rTR6+f8BQ9PU0pZ56sQfWpJN+C/a/8og9SuVLHYZ6u7182gN7wp8IfGIwr57qISZ6OzyS7HaoHmrJLmF3ZCh2c5GTnr5tJleTDlf8MgpXfxfYEGYmQ6iTcGPirrvp3tw4mHPM80SdYD+XEp7s1QVZ+QlYnDzjnFKs7ibHtA45Ii8Fg6HfOZVegc/vAObvgVawPtF/tLhWCxx2UNuxYu4TR/tMdWxiHeVT7bhKSurU9rHxY50Qb/IC9uzYV29JPKk+4XjXw8SU6WPlLf6YEwG7bn6zl6UkPNb4SyU/6cJfc2pzmU/sXYm1pb1S+GSWVvcIumYbvCWuFEKCp/LckKt+MkspeYZLp94D1AdcLOTmVfhui8s0oqewVJpl+D7qcXGF/K6LyzXj/TGWv4D3+b7XL/Q1x3U8/H/92ROUwDPdE5TAM90TlMAz3ROUwDPdE5TAM90TlMAz3ROUwDPdE5TAM90TlMAz3ROUwDPdE5TAM90TlMAz3ROUf+JX53RdI+cKi9U7qV1LbSvop8yttEtY/mdvqS5m0pXylX3E7rzS+ZV1P36uyWn66RrDr83Q84LmX79pW9OmJ3fZZWa3hipvx/qQrmDDf6u3i/zPQ69ffpnTh14vJgbJrizCZz2gj2IJNXZhb+jmAY/G3lwHfgEZW+pXcziuNr3S9P+9e/byBsRFsTOUJ2yA9KGusVL3UtjfzkPRfALBeu3VeySqGK/iNelVO2v079YVg86fEiP+vgLq0AAYBA1IO1RjarhLKto7TSbtCGu+pDaCvcjO3FARAG6Tr60+slTrHZKM/I6noxzS+UnUGXvoJtrySTPoB6T8Br2td9VKTCemxoFSd1HH1X5VVQinVl1VWP2OviZvGe0yo+uKuTUfdUHakZMQq4GhvoK4W17YrpyRW4z2hLfztu+vt3ERnpzKxzs0cZTe+4rubBePVeh3sQJ7qVbSDdaztCC7E06m2EccztvqJqVQdsCZK9519kchVL0rXOw+kl9Xxqo31NrMa7098qJ31YNuxW/B6GqTTybbvTiYXHWesTsnE01jfJZmYE8JG8TS/jySTfnQMggwsT2M7HnUYswekUnVggq6uq26O6ZRXun5X5njMp5eBJ/B2HX3AaGTV2YrdgoOTTkbYllOB8k6vX9tg52kb6iL19GERaru0KE9jObfervIVyeQYaQ4d7EBeSSaeEQMcIcEsT3N0POoYYzVBlNoGnNNqc3DMlGxK19cDo5c9rZMxlNbiT3zQuG3lwFO7nZG2XUmv/2qbZIM6hffaBp7GUnq7ytMi7dj5VrHOaf/UQ9J8V9RrHEHFP3J4SqHb2eB4zoEdvo6t+F71u2tV77eiUEe0Edm16XqxfWr7Jz4cVQ48tdsFk23Z4anX6fVrGxflpA16pNrQ/5EgtX0aS+ntKtZL839i59sq2Pd0vRPsQLArlSecA89e3UkofIhOO9McHc85eDpZV6lt1O9s7P1WdsJm0OvXNqkMdmvxJz4cVQ48tdsFk21T2YpX7HwKaCQt3OncUpk8jb1jN75icLMh9ToJgzDNdwV16ynBM+K1sidIxfGcA0mPkIy8K7UNOEbXi2OmfzhSsFtI/N01WFn9e8FTLPw7Pmjc6aLIbhAdh6Sd07Y3gXY0qYbXFHfSDpKC62ks2iCpTKzzrmTi2bVb7bqVV5Op1ieATQawzzRHy+ocnNfJZ5hVcPsZJo2pdP0Ox8OXqXz32f9PfKiBv+pwlxBpwZ3wauFsuzWwsRtvhQuKpN0JSTY+jeUCpDKxztMck2/dBPh80ssU3/X1k1/0xWpNEtTd1bfPNHYqM9acH1LbAAmL1KQVT+N6WlaUVLbC8dJhUuOnl/1EfamTY3FYRDriL+/JoQYc5dQFDPOYRlbHq22pb9vKbfLuMKgR5ukY7uq7ua3Gss+uZ772745W55jmRV/V546NpPqK79RxrNVmCPSNUFd7KmmtWMvkH6Edkvy0KjP5lVoGJhziP3jQV/ULPu3tQEllK+opyVzTeGl+P9EVfZJV0i5RB+vCIqwSCXZtESazavM4sQaLY/AnScHyNJb9rfQrSfPCt0lWiaFUXQ2Ild8N7pUkPyBJL/aZ/LQqq7YitUwI6JWsruygpLIdbqxJyIvU5ieSkoXAWBwIdLRaVPTWE9riiFS/ktpWUkDYZmXPEyxumlva/Z/Gsp+VfsUq0BmHpKIOdqWkE/vqetqgp580J8a2bQLbe5uVXuwz+WlXRoI5bi8Tkq7Ww46dX+Cpzx2MxxjK0zr8RFQOwz8cE6onUtqg/iQqh+EfjldMPqpwqpJUnFLpHyj+JCqHYfj35Omyu+5OMg3DBk4lP4Olz30/EZXDMNwTlcMw3BOVwzDcE5XDMNwTlcMw3BOVwzDcE5XDMNwTlcMw3BOVwzDcE5XDMNwTlcMw3BOVwzDc4wM/4uJr5pXtbzcW2A9/U7mk8RLJBn7ARRl/exnYtuvpC/2Tbav2p21XdjnnlV8tT+2d86u2O/cdT33DaT2oY67m3HGekMqfqO13Y9Z6ndP5/QUf+FZsEvRpcTv8GrILvwVJP2WG1XhdmFxv61irvpWuZx4IdvUysU763QqC3V1fQVZ2+Z92pD4cF0lBgE5ZBYl+ST8TwI9Pspob49k38uQD6tdfqyr8pGFlOwGcYmLXpkM91lZJsSN1Pl2e5rfEByfCX1F2wQfVcQRhb79qk8aq7SHtEjpiFbRKKqN/ZLX7GPDpP+tAdj8OM2BXdoHj9/71we7n/tpGgPUyAklJwWewJgzANOcUdLTp9Sr1/7OgrnNGku3gONhy2qbTE/gkmRhHP0jajI7wgU6QWsgCKCvD6iL2E4yyXWBVHH/nANERq76VVGZArhzmIqaATD6qnCRT9akJrU3pP6zpaF/3EwGHPP7mpuH6rcY20ZiTvz7FD6muMK+emPU/R6n6Wt7npK+exgP7d5NGdrFkDJ3E2zE+rALlyTAn/HR6PXHiAPlIMu2uepatAqv7iECs9vKMPM1B+0mMp+tdxzFoq+4m6DrOaTU2yWnSO/Yr44AbQSpLPCW6WE+fnMTSL0kmHaBDE+5eTLqfTqecOEA+kkywmpMnxGp37z7yWuGcDbaTOWiDkq5YKzyFbKNduzVKON/d1bLi/BgvlT+hpDJwcwJ/4Yo8zavX8323DsYQPnDMVO8KH6oBgIPVmfErmIQJhZBUpwskJw6QjyaTQdSvegb4apc2iH23vkFNACAnc8BnCnNPdVa4EyNeb1a+WOH4N58PmBdyay+sfF6x/ypP11b7rfVOYskY6vKRA+EvydQF/Sq4Okyo7rgk2ElgwYkD5KPJ5NWqXvXU7a4UjouNtb7BZfnJYtgeedqsEiYuwjxO10gY87adwe58TzFxGXM3HvXoG2ocrf4Bwn57+Uks4T/H6uMht/78d3zQADtHcDYG/9TgAALFXRw5SRDH/IpkAp3n/NzhdidqTSY3DoMa51ue2nb64nm63WAf2JDKV2jn7dq+kkwGPLbeBihxtJujZawZtol6fMr7yeYGdbxX1mP5mUknIKfGdAzQ3W4vN8lkAH8kmbTNa4dO7PUqNZmYE20JEASbTpPJesyZvpRbP9/4TBxv5bsdtj1NJtcJ37600/9gd409lZu5ujav+Gf7DxB2vDpmYeekG+ffBMau39MxCVyE09fn3TzBhTVItJWxTC6kt6ukTcp2J36q3PgMWCvmezuOnPoWPiORwH7428uwI6EwNu+p7QrWEblp8yc+aEQtdNdFVrsmCYfR/MXZws7PwiEnWe74J4FR7WLyHvOeNsiJMxzTYN5d8YAxEOel3nFPAtUTkDbqDHLkZhFvfAbOkzFok+htSH7LnCdzUJeuil7xmVO/gq3a1HGAdtqL3CTkiV/6ePjEdtj90gbggx39VPgDBkFWQVIDOAntTgw7cUBFu5I8nTDS+0h1Ktim1KurJxuy8hN40qc6nnrI6ULe+Kz2v5PezjFWkubyJKnNahwCOyXfjhO/rMZjo1gdHI/4wC6QJgkEDmWrQdATKNYDT6pUP+H4N45jXNsBSXQzJkFr23pSrMA265OItcy5Y0/VSx1rNUfncmIL3PjMnfeJ3q76N5Hmm+pVUps+DmvZfXzKiV/6eLw/3UweicphGO6JymEY7onKYRjuicphGO6JymEY7onKYRjuicphGO6JymEY7onKYRjuicphGO6JymEY7onKYRjuicph+CL4svLNl5O/NVE5DF+EP0tJZb8dUTkMX8Qk0zB8Eu9IJn7HxNXx5R/5vUpUFjBKUjk/eqt1Eic/Xqv1U/kK25z+OlV+mcOHnzhJpr5OvO9iih/7IfSdyt9GUmKsBlVZ1X0S+kptQWd2eXJEasevXZ+Sil9T+v8tKNh3kvDD/2fn49vN6SSZkBoPSP1vAzrfJpn6/4uAYdLrAkFY61T8z0N4Tm1rQvQ2yOpny/6HHYjtlNVYUP8fBBKqtkNuT7d/Iqw3vkv/zwb+Y/1O/w8OOE0mNm2eGQPZJYrrepJM3lA+Ze3rSz1lPvx7+B/oqFVSeEL0iZjQ6f8KYPJI/48v6IPdClmdMo5X7TEAkBPnP4FNq/FP+Yw+3oG+R9Ka4ks3upNbAqySCR8Qj/6HPfyt74zDe/KTyXQSw9alr1R+RX15CsYbcCRCAKdyULrTPUHSfyyiM9NiutgpKSxLJ5dlO1tPIACUVzejz+jjHbjBIU/xYUKxST0l1CqZ1D9JWk9sRX8Sx29LJsS7qDvDq4PojBT0Yh2C2Hou2moh6k7Vy0zg5OCUhNTvi1bb3IKvFPpNdZ74jD4+G33HmpwEKLiOrO2ujf7vetqzjt4meK7v2MJ7ur3cQB/IpyaTuzPJ5ABVbhbW3fVkp8cZXdCtdrS6c2OTCV/7wf7ezkXTaQZIl9NgWYEdp1ecFbs+sM85Vz4y3o6bU6ZjQiErv66SSXq578RBrXdK9x/zQoiHqn/Jnz7QQRUSgaB0MKTu6jtcgJP6TM7dpsqubUrAKrtkom0dj7osjBsIfuhtvxPa2eUddjuWt5VXYH2VdG1dJZNBjQ2sV31HfO/tnlj5r8srff8lmUie3pGfYTCk6hOeHCd1687FGLQ1EZHdZwZspC7jADuLC0fC9PoumkKbOk/ekXft8J8Fc3POldXO/xGMCeR0I+24nquTdpVMp9LbPdH9p3htlJf86cMuEEFJZRWd1xOyg2OVfmTXO3rVP2FypoW3zF2ulytd/0+HuPAkv0ko1tdbDTGR6sAqmXqwr957u1vs8ylej6gvCI6rOjDwn4w3IU8m6a63qquksoRjI2kH9MREernJ+3SdoY+nHYs6TwvzVOdknK/kNDEE262Pb1MdWSWTIHVdkM9IInlbMnmqMABXLAZgNzrdmW4Mq8HNaUgbYFych6yCm8WyPjZpN7KzUftYaOrRvl7/eE/toCbr6vSuc1otOGMoqZ/ax+6a+9WQUK4L/k4bFlQ/PcULnCQTdVbvH+VtyVR3oC5PO5JBsgqiRA3kLiQwgZXa6YAuT4u3m99T2xokSKpTEwFJ9nsKIukWUJPtM4Pms6gbbi+r88dfvTyxSyb70w+uwWf6xc9Qp/Zu6QoCjgVnAMB5J1nLBKl/m+HUd0LA7kdgr3Y+qPV5fqpfeXV+wFhKKoeaLKlfxmdMxl4tIOPgh9M5fTXMcWX7riyxSyb6wU9udP392xGVQ6QmSioHrmbKd02G78QumX47onL4CU4YFx1J1wzq1GTjdOl1vhquSdh1Surj3Uwy/cOosvrsWIWryHc4leoGcCKpj3czyfQPg+TgpNnt3tQh0b7Tv8Dx2QK7Tkl9vBttTGW/HVE5DMM9UTkMwz1ROQzDPVE5DMM9UTkMwz1ROQzDPVE5DMM9UTkMwz1ROQzDPVE5DMM9UTl8W/zyair7u7H7fuM7v/tYvyCcypck5a9cMCfxFV8U5fcxjMV8UzlYJ9mjrba3Ln973Qp92ba2P+Hmi6GOs7Ln1tZUfoN9ncyX7+whqS7fk+RHnmlNPgP69Uekqy82R5KSnxKkX4G+E3++wCT44iPCRE4dhtNPF5w+HUNZ/RjPeqlvBdtr3dXPL+ifOXVJP+lIEPjVnt18qVuFdrVcf+++mMs8ujwl3w7n/pRM/iZs998WIO9OKH+mf/xjRB+qUSxSdf7NzvkKOrkGB/bgrJVDO6f/TwUYkLTxGUlBXYO3lymWWXeVHDWRqCtPNnsaJUn1QX9oE+I4rCey860nA0If9oek+k+wnsjTemrbU6Jo32l8vAobCvPf2fInPtAIhxEYJhMduDu9K6HchdKOp2OfdkODzUXfTdxdrS6CC80C1rpgMKZkoj5imcmySiYkjbGjJyDiJgCpDSCekNiHaJf97PyU5u18XzmdXKPkx4rjnoyhb77Nz158wLFe73CaOxFB9+SAVzGIV8EH2LIrr32YKLv6PbDEOVcdpKBaldEnshpfn54Go5sJ7Zjnib8EYU7Y5obI+p5c7yAlXNKdYlylMnFtSJJU3tEfad1+CV1Rj3cS6V0nEjhWXRwcSrChA4/Z2q5iANuHu9XKbgMUYXzGs4+0iLtk0jaTg/54T3XBcRCe67wTBpdB6Puq/4p2V3Gz8cTaYdJZt7/f4DrzN5UL8YbcxNzudMK/+GrH6cZ2hA+eSiwCz54IyFNSYdArRtEv8OxCV2Gy2IBNvS2YGNTput3uRkAkSXM0KLv+Fq+z+LUG+tMJoTAfg/LE13Ujoi1z49lTLrWpUAdbEdogryQSOG4qqyBPp1eHGEGSbZbtBNt6u5fxgUV158AIB2ERarB2qKfgiLpQ7gwpSAEhcXl2wVxAhPa7ZHJX6sHhJsDYVV9hriQyfbMQKxvtK5XdwPxqIJsYyC5ISRx9ozwlYMKTZeeTCv6oa1FPFWw67Yd6yC6GwHqvJCySYgQ70e94dYOIJCWLxUCprOICGShITSgdlBbfk4g6lCM6nCShP55XycQY6E3GVPa0gCd8VjIhfR7YacA+JUhNPgT/nAY0iYEYOE/JYH2ENoobjjb3donVhtcxVl5ZMyWVfSlJycKdJBN1EB3lnRdn4xzLWbzeVufx7ILZD+0d39PDdl/NZyWTm033hZuK190d+AHfGKDIySlFO8bvp426Xt/+tVUf0NZE47m361iX/lJ55R3JRDzR744Umy8TlYeQBPWYrDutskoEJoLwTB2EZ/pAdCoLfrIY7+J0Y3nC+SI1AQy4k2RC9IUn1ZNt9XrnpoVPXafkW6Qnixulm0Jd9xWOlxK2o39eTabkh+rzlXzG2v5JVH4AkgGHYCTO9LTpeLXjmboIOheNv+6I9b7+1WD/Z+1eLK7ByJwJMudLWa/ffYcYbAbKLhhMVAOfuoxvOQmT2it1fJ61HXnyCfWRk00CbjaVyq4dNjK/HSebwjFR+QUYDDy7eyp10eri/wq05eQ6dQJB5iahrDYL6hHw/NVHPmvXLhhoSz2TguBBmMvuZDPBaU9d1spTRknJX6knYipPOKdUtsJ5/MoN90+i8gswmdhZWGyuGywsi+eOgo7y1P6rUDwRPgvmRbAiq0Ttm0wXfGSidEzYGswGXpU0Nv5fiQmJ7NaGxIBUtkKbbxJDe1Z++FKi8gtg8sgqkL4LBDQ79TsWy6AlIFI5UIdTwV2burzvdnxspV7aAAhUypjTzvf0wdypC2xsjkm/2tHbAf0it6eFMXGahG7Iu9P5S4nKL4KdOX0A/jtCUrD4QsB5nTo59fATksq+GyQaksqe8HR68gmJ5wbzjo3uJaLyi/DaQaCl8m/jpE/AAOvChnIyT3bfb7MDb+Dqx1w51VL5Cfpqd7K5EXlafgui8gtxx2UnctcmaNh1vpWjPghzIkiEed9eg/4psLn4eTJ9LsOXyLfzX1R+MTiF4DLQcNbfKZGGe0io1VWPm8y33IiichiGe6JyGIZ7onIYhnuichiGe6JyGIZ7onIYhnuichiGe6JyGIZ7onIYhnuicvjHw9d4+ld5br6V8pH2Hx37l9EVfrv56cuXTJZ6J3U71Kdd+oKrfVZS/3V86QsAjgW97B18xC/fCcV3f1t1+gVWxfeb9orvt2P/Mnxg4f1yobL6/pNfk6/y9JV5YAy/7YvwPbxeJ0lKhFMbaFvl5DtdbiipbAfz82cBT9/wJun6ZvKUgF/VBrC/zoE+WK/e14qPtP9IW+YGaWN9Oz4Y5Bhdk+qnyn9AQFJPlFS34jfEEdpVh4lS+09OrDZob0omF8IgR54cjV3I7YJox0nCKv5Az92Xdeh1RXl3mwr+IzhPAjlxE9zU2dW1r1TmvKqwHq/a/RI+4Ow6sOJ7x4nTjkDF8FSvYkDvJogQ/KlsBXYgT6ejyfx0XWBuCPVTecLkNmifIGEJaAMHn9B+l4hf1QZScN7+LIaxlJP48LaxWkel65mLwjjMz1jjudd/G0lp0KWTQ5y4cuJk5GlyyK0DTpPptB7og6fr0N+RurYGZ5XThPKE968JvcJxV+ujdD02In2DUF91b6W+sJNowFPAYThOro5+cjLyuyTTTd2PwDjwFGgV27BeqVys1/WMtRoTndLbusGw61f9CupxKnpCPd0I8DWy8rmS9N0mx7yNpQ/hgxNBmLQLkRyOoZZzrTGhnpyF/C7JBNhxGji3VH8rjIWNqT4npMFchTarq6XS9Y6d/OApsroGutmeJDJiP9hJ216vsrMLlKSvMeO1j/Ge7PxUfFhJmpjJ0+XpSoT8ymRy1z39LOSirALrVQxYhAXv/kwB4BwR6vc2yfdK1++C1n5Xa2nbJ5/0a7LvaXOWnV2gJL0xo59O/4HlU/HBBeokp3ECWY7RvK+c7ylGPeQzTq/OaTIBOyRCXdrtFhdOdtQb6jWqJ40BlwKBuqxF97N+ZS5VD0rX74IWYc5dL6e+Ruo6cnoiJ5/DV30rSe9Y9nH6D0GfSlR+IkxSeVoAQN6ZTATljU0kP5KC9RXsb7WpKKkMSCZsERMw2ad0vQGX5o7s/M84yM5vJk6fI7JL1J1doCS9Nmsfkk74txKVnwg7kfds5GmCyDuTyQVDGOfpukLwIp91bXD8FPxgonc9dngKJfmsZPLk7nrBX8juhmGCUxe7xLmtYmBnFyhJX2NGG5GdnZ9OVL4BdyscncoFeWcyIbf9n9z3TzFgUvCDG0/XG4gIz2Lwp/6Urt8FreOs5ro7CeVJVlc9k+CjyQQkrL78soSKyjeB9El3kKc6ndNkukm6ip9znjaCEwyYdNJpX5q/0j8zmRgpuA2m2oa5qE9+IPCQ1ec2ZHdVc9OkDvPoIKv2u/k/+SbpvVXs7P1UovJNIGnSFeSpTuc0SU7rJbxi9WC+xQVG6JPgwy6DGEkfnhWvTvz1lEDQ9Tb4EWEc21RJfljZZ9IiJ1e81VVOP6byE98wh94OMWbYLGhT58vm0du8hah8E8jvmky2/Ywrg7t3ktUVyMTosrvmrcYx4Fd+eMU+QXYngQH+dNVLsroZIMZMTXolJeBbiMo3gfyuyQTscJ91ZWAHJaCYKxAoKSEqJHKvzw7P++okIDHY5W3DuLbZBRn18JPjYeuTffa723DoV1tSOdz6Rvt4Zk6n7T6dqHwTyFMwIjgila3AYchTkrjjvppM7ppfttMNvxdR+SZIEsQ7fNpNEU4AyiV9TmH3stw7dUoS2lLHREBIql7vFGx/NRmHvzlR+Sb6XTydQElIhl6PgO6Sgpy2Vb7sw+jwzyMq3wjB7X3Ye24FfSedYPVuLOn65T0eOMHSKTcMn0JUDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3BPVA7DcE9UDsNwT1QOw3DLv/7t/wE1sIL2kRcjXgAAAABJRU5ErkJggg==</pixelData>
          </body>
        </item>
      </_items>
      <_size dataType="Int">1</_size>
      <_version dataType="Int">1</_version>
    </layers>
  </pixelData>
  <renderMode dataType="Enum" type="Duality.Resources.Font+RenderMode" name="SharpBitmap" value="3" />
  <size dataType="Float">16</size>
  <spacing dataType="Float">0</spacing>
  <style dataType="Enum" type="Duality.Drawing.FontStyle" name="Regular" value="0" />
</root>
<!-- XmlFormatterBase Document Separator -->
