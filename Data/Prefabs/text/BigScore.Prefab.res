﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="2640933354">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3693509805">
      <_items dataType="Array" type="Duality.Component[]" id="51778406" length="4">
        <item dataType="Struct" type="Duality.Components.Transform" id="706280990">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">2640933354</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-0.5696335</X>
            <Y dataType="Float">-2.64725876</Y>
            <Z dataType="Float">0</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-0.5696335</X>
            <Y dataType="Float">-2.64725876</Y>
            <Z dataType="Float">0</Z>
          </posAbs>
          <scale dataType="Float">3.58045459</scale>
          <scaleAbs dataType="Float">3.58045459</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="88594880">
          <active dataType="Bool">true</active>
          <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customMat />
          <gameobj dataType="ObjectRef">2640933354</gameobj>
          <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
          <offset dataType="Int">0</offset>
          <text dataType="Struct" type="Duality.Drawing.FormattedText" id="2663373788">
            <flowAreas />
            <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="3462568644">
              <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                <contentPath dataType="String">Default:Font:GenericMonospace10</contentPath>
              </item>
            </fonts>
            <icons />
            <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
            <maxHeight dataType="Int">0</maxHeight>
            <maxWidth dataType="Int">0</maxWidth>
            <sourceText dataType="String">-FINAL SCORE- /n/cFFFF00FF/ac0</sourceText>
            <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
          </text>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="The_ESG.BigScoreBoard" id="2022687107">
          <active dataType="Bool">true</active>
          <board dataType="String">-FINAL SCORE- /n/cFFFF00FF/ac0</board>
          <gameobj dataType="ObjectRef">2640933354</gameobj>
          <text dataType="ObjectRef">88594880</text>
        </item>
      </_items>
      <_size dataType="Int">3</_size>
      <_version dataType="Int">3</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2887236472" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3839256007">
          <item dataType="Type" id="381128910" value="Duality.Components.Transform" />
          <item dataType="Type" id="3673104202" value="Duality.Components.Renderers.TextRenderer" />
          <item dataType="Type" id="3547862654" value="The_ESG.BigScoreBoard" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1620576512">
          <item dataType="ObjectRef">706280990</item>
          <item dataType="ObjectRef">88594880</item>
          <item dataType="ObjectRef">2022687107</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">706280990</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="2757690949">gT22Pt+Qa0ylcTNyZ5MNAA==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">BigScore</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
