﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="3461956583">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1968265292">
      <_items dataType="Array" type="Duality.Component[]" id="3680604068" length="4">
        <item dataType="Struct" type="Duality.Components.Transform" id="1527304219">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">3461956583</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-65.53608</X>
            <Y dataType="Float">-133.146851</Y>
            <Z dataType="Float">0</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-65.53608</X>
            <Y dataType="Float">-133.146851</Y>
            <Z dataType="Float">0</Z>
          </posAbs>
          <scale dataType="Float">1.88485408</scale>
          <scaleAbs dataType="Float">1.88485408</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="909618109">
          <active dataType="Bool">true</active>
          <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customMat />
          <gameobj dataType="ObjectRef">3461956583</gameobj>
          <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
          <offset dataType="Int">0</offset>
          <text dataType="Struct" type="Duality.Drawing.FormattedText" id="2121459409">
            <flowAreas />
            <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="948312302">
              <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                <contentPath dataType="String">Default:Font:GenericMonospace10</contentPath>
              </item>
            </fonts>
            <icons />
            <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
            <maxHeight dataType="Int">50</maxHeight>
            <maxWidth dataType="Int">0</maxWidth>
            <sourceText dataType="String">Base Score:   /cFFFF00FF0/cFFFFFFFF /n Enemy Kills: /cFFFF00FF0/cFFFFFFFF /n Accuracy:    /cFFFF00FF0/cFFFFFFFF </sourceText>
            <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
          </text>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="The_ESG.Scoreboard" id="2165166944">
          <active dataType="Bool">true</active>
          <board dataType="String">Base Score:   /cFFFF00FF0/cFFFFFFFF /n Enemy Kills: /cFFFF00FF0/cFFFFFFFF /n Accuracy:    /cFFFF00FF0/cFFFFFFFF </board>
          <gameobj dataType="ObjectRef">3461956583</gameobj>
          <text dataType="ObjectRef">909618109</text>
        </item>
      </_items>
      <_size dataType="Int">3</_size>
      <_version dataType="Int">3</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1033037302" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3377194438">
          <item dataType="Type" id="1490054400" value="Duality.Components.Transform" />
          <item dataType="Type" id="2791084494" value="Duality.Components.Renderers.TextRenderer" />
          <item dataType="Type" id="258699164" value="The_ESG.Scoreboard" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="2292093626">
          <item dataType="ObjectRef">1527304219</item>
          <item dataType="ObjectRef">909618109</item>
          <item dataType="ObjectRef">2165166944</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">1527304219</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3867627206">pAyRzMgyok+h1jBQ1dbp4Q==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">smallScore</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
