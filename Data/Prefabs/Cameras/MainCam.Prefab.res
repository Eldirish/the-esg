﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="3875542296">
    <active dataType="Bool">true</active>
    <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="1544855311">
      <_items dataType="Array" type="Duality.GameObject[]" id="2094061486" length="4">
        <item dataType="Struct" type="Duality.GameObject" id="2874577675">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3741253195">
            <_items dataType="Array" type="Duality.Component[]" id="2708116214" length="4">
              <item dataType="Struct" type="Duality.Components.Transform" id="939925311">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">2874577675</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="Struct" type="Duality.Components.Transform" id="1940889932">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3875542296</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">100</X>
                    <Y dataType="Float">-5.266133</Y>
                    <Z dataType="Float">-600</Z>
                  </pos>
                  <posAbs dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">100</X>
                    <Y dataType="Float">-5.266133</Y>
                    <Z dataType="Float">-600</Z>
                  </posAbs>
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-6.982048</X>
                  <Y dataType="Float">-241.726288</Y>
                  <Z dataType="Float">580</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">93.01795</X>
                  <Y dataType="Float">-246.992416</Y>
                  <Z dataType="Float">-20</Z>
                </posAbs>
                <scale dataType="Float">1.07212436</scale>
                <scaleAbs dataType="Float">1.07212436</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="322239201">
                <active dataType="Bool">true</active>
                <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customMat />
                <gameobj dataType="ObjectRef">2874577675</gameobj>
                <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                <offset dataType="Int">0</offset>
                <text dataType="Struct" type="Duality.Drawing.FormattedText" id="3433508353">
                  <flowAreas />
                  <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="304837422">
                    <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                      <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                    </item>
                  </fonts>
                  <icons />
                  <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                  <maxHeight dataType="Int">0</maxHeight>
                  <maxWidth dataType="Int">0</maxWidth>
                  <sourceText dataType="String">df</sourceText>
                  <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                </text>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="The_ESG.Tutorial" id="3442807214">
                <active dataType="Bool">true</active>
                <gameobj dataType="ObjectRef">2874577675</gameobj>
                <opacity dataType="Float">80</opacity>
                <text dataType="ObjectRef">322239201</text>
                <tutorial dataType="String">[WASD] To Fly! /n[J] To Light'em Up!</tutorial>
              </item>
            </_items>
            <_size dataType="Int">3</_size>
            <_version dataType="Int">3</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2177056584" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="2003022945">
                <item dataType="Type" id="1215682926" value="Duality.Components.Transform" />
                <item dataType="Type" id="3401369034" value="Duality.Components.Renderers.TextRenderer" />
                <item dataType="Type" id="3496320862" value="The_ESG.Tutorial" />
              </keys>
              <values dataType="Array" type="System.Object[]" id="1972940832">
                <item dataType="ObjectRef">939925311</item>
                <item dataType="ObjectRef">322239201</item>
                <item dataType="ObjectRef">3442807214</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">939925311</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="2600311027">AtLx0adds0GTEyfx3vLhug==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">Info text</name>
          <parent dataType="ObjectRef">3875542296</parent>
          <prefabLink />
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="2834562584">
          <active dataType="Bool">true</active>
          <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="3037758020">
            <_items dataType="Array" type="Duality.GameObject[]" id="3425741380" length="4">
              <item dataType="Struct" type="Duality.GameObject" id="3861271473">
                <active dataType="Bool">true</active>
                <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="2684448269">
                  <_items dataType="Array" type="Duality.GameObject[]" id="4021287718" length="4">
                    <item dataType="Struct" type="Duality.GameObject" id="2600699573">
                      <active dataType="Bool">true</active>
                      <children />
                      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3620378469">
                        <_items dataType="Array" type="Duality.Component[]" id="106788758" length="4">
                          <item dataType="Struct" type="Duality.Components.Transform" id="666047209">
                            <active dataType="Bool">true</active>
                            <angle dataType="Float">0.004886627</angle>
                            <angleAbs dataType="Float">0</angleAbs>
                            <angleVel dataType="Float">0</angleVel>
                            <angleVelAbs dataType="Float">0</angleVelAbs>
                            <deriveAngle dataType="Bool">true</deriveAngle>
                            <gameobj dataType="ObjectRef">2600699573</gameobj>
                            <ignoreParent dataType="Bool">false</ignoreParent>
                            <parentTransform dataType="Struct" type="Duality.Components.Transform" id="1926619109">
                              <active dataType="Bool">true</active>
                              <angle dataType="Float">6.278299</angle>
                              <angleAbs dataType="Float">6.278299</angleAbs>
                              <angleVel dataType="Float">0</angleVel>
                              <angleVelAbs dataType="Float">0</angleVelAbs>
                              <deriveAngle dataType="Bool">true</deriveAngle>
                              <gameobj dataType="ObjectRef">3861271473</gameobj>
                              <ignoreParent dataType="Bool">false</ignoreParent>
                              <parentTransform dataType="Struct" type="Duality.Components.Transform" id="899910220">
                                <active dataType="Bool">true</active>
                                <angle dataType="Float">0</angle>
                                <angleAbs dataType="Float">0</angleAbs>
                                <angleVel dataType="Float">0</angleVel>
                                <angleVelAbs dataType="Float">0</angleVelAbs>
                                <deriveAngle dataType="Bool">true</deriveAngle>
                                <gameobj dataType="ObjectRef">2834562584</gameobj>
                                <ignoreParent dataType="Bool">false</ignoreParent>
                                <parentTransform dataType="ObjectRef">1940889932</parentTransform>
                                <pos dataType="Struct" type="Duality.Vector3">
                                  <X dataType="Float">-8.204281</X>
                                  <Y dataType="Float">19.1070538</Y>
                                  <Z dataType="Float">500</Z>
                                </pos>
                                <posAbs dataType="Struct" type="Duality.Vector3">
                                  <X dataType="Float">91.7957153</X>
                                  <Y dataType="Float">13.8409214</Y>
                                  <Z dataType="Float">-100</Z>
                                </posAbs>
                                <scale dataType="Float">1</scale>
                                <scaleAbs dataType="Float">1</scaleAbs>
                                <vel dataType="Struct" type="Duality.Vector3" />
                                <velAbs dataType="Struct" type="Duality.Vector3" />
                              </parentTransform>
                              <pos dataType="Struct" type="Duality.Vector3">
                                <X dataType="Float">0.833333</X>
                                <Y dataType="Float">255.833313</Y>
                                <Z dataType="Float">0</Z>
                              </pos>
                              <posAbs dataType="Struct" type="Duality.Vector3">
                                <X dataType="Float">92.62905</X>
                                <Y dataType="Float">269.674225</Y>
                                <Z dataType="Float">-100</Z>
                              </posAbs>
                              <scale dataType="Float">0.299232155</scale>
                              <scaleAbs dataType="Float">0.299232155</scaleAbs>
                              <vel dataType="Struct" type="Duality.Vector3" />
                              <velAbs dataType="Struct" type="Duality.Vector3" />
                            </parentTransform>
                            <pos dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">-3.507018</X>
                              <Y dataType="Float">-1.76907849</Y>
                              <Z dataType="Float">0</Z>
                            </pos>
                            <posAbs dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">91.5770645</X>
                              <Y dataType="Float">269.15</Y>
                              <Z dataType="Float">-100</Z>
                            </posAbs>
                            <scale dataType="Float">6.68377352</scale>
                            <scaleAbs dataType="Float">2</scaleAbs>
                            <vel dataType="Struct" type="Duality.Vector3" />
                            <velAbs dataType="Struct" type="Duality.Vector3" />
                          </item>
                          <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="48361099">
                            <active dataType="Bool">true</active>
                            <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                              <A dataType="Byte">255</A>
                              <B dataType="Byte">255</B>
                              <G dataType="Byte">255</G>
                              <R dataType="Byte">255</R>
                            </colorTint>
                            <customMat />
                            <gameobj dataType="ObjectRef">2600699573</gameobj>
                            <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                            <offset dataType="Int">0</offset>
                            <text dataType="Struct" type="Duality.Drawing.FormattedText" id="3526397883">
                              <flowAreas />
                              <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="1906255062">
                                <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                                  <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                                </item>
                              </fonts>
                              <icons />
                              <lineAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                              <maxHeight dataType="Int">0</maxHeight>
                              <maxWidth dataType="Int">0</maxWidth>
                              <sourceText dataType="String">2</sourceText>
                              <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                            </text>
                            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                          </item>
                          <item dataType="Struct" type="The_ESG.ShowHP" id="2328686595">
                            <active dataType="Bool">true</active>
                            <gameobj dataType="ObjectRef">2600699573</gameobj>
                            <player dataType="Struct" type="Duality.GameObject" id="3026331258">
                              <active dataType="Bool">true</active>
                              <children />
                              <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3254302635">
                                <_items dataType="Array" type="Duality.Component[]" id="4218301686">
                                  <item dataType="Struct" type="Duality.Components.Transform" id="1091678894">
                                    <active dataType="Bool">true</active>
                                    <angle dataType="Float">0</angle>
                                    <angleAbs dataType="Float">0</angleAbs>
                                    <angleVel dataType="Float">0</angleVel>
                                    <angleVelAbs dataType="Float">0</angleVelAbs>
                                    <deriveAngle dataType="Bool">true</deriveAngle>
                                    <gameobj dataType="ObjectRef">3026331258</gameobj>
                                    <ignoreParent dataType="Bool">false</ignoreParent>
                                    <parentTransform />
                                    <pos dataType="Struct" type="Duality.Vector3">
                                      <X dataType="Float">3.81469727E-06</X>
                                      <Y dataType="Float">160.8393</Y>
                                      <Z dataType="Float">-200</Z>
                                    </pos>
                                    <posAbs dataType="Struct" type="Duality.Vector3">
                                      <X dataType="Float">3.81469727E-06</X>
                                      <Y dataType="Float">160.8393</Y>
                                      <Z dataType="Float">-200</Z>
                                    </posAbs>
                                    <scale dataType="Float">1</scale>
                                    <scaleAbs dataType="Float">1</scaleAbs>
                                    <vel dataType="Struct" type="Duality.Vector3" />
                                    <velAbs dataType="Struct" type="Duality.Vector3" />
                                  </item>
                                  <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2733766639">
                                    <active dataType="Bool">true</active>
                                    <animDuration dataType="Float">5</animDuration>
                                    <animFirstFrame dataType="Int">0</animFirstFrame>
                                    <animFrameCount dataType="Int">144</animFrameCount>
                                    <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                                    <animPaused dataType="Bool">true</animPaused>
                                    <animTime dataType="Float">0</animTime>
                                    <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                                      <A dataType="Byte">255</A>
                                      <B dataType="Byte">255</B>
                                      <G dataType="Byte">255</G>
                                      <R dataType="Byte">255</R>
                                    </colorTint>
                                    <customFrameSequence />
                                    <customMat />
                                    <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                                    <gameobj dataType="ObjectRef">3026331258</gameobj>
                                    <offset dataType="Int">0</offset>
                                    <pixelGrid dataType="Bool">false</pixelGrid>
                                    <rect dataType="Struct" type="Duality.Rect">
                                      <H dataType="Float">32</H>
                                      <W dataType="Float">32</W>
                                      <X dataType="Float">-16</X>
                                      <Y dataType="Float">-16</Y>
                                    </rect>
                                    <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                                    <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                                      <contentPath dataType="String">Data\Graphics\Sprites\Player\playersheet.Material.res</contentPath>
                                    </sharedMat>
                                    <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                                  </item>
                                  <item dataType="Struct" type="The_ESG.ActionInputCheck" id="3659035559">
                                    <_x003C_shotPref_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                                      <contentPath dataType="String">Data\Prefabs\PlayerShot.Prefab.res</contentPath>
                                    </_x003C_shotPref_x003E_k__BackingField>
                                    <_x003C_shotSound_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
                                      <contentPath dataType="String">Data\Audio\SFX\shotsfx.Sound.res</contentPath>
                                    </_x003C_shotSound_x003E_k__BackingField>
                                    <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.WeaponStats]]">
                                      <contentPath dataType="String">Data\Scripts_Resources\Data\Player\BaseWeaponStats.WeaponStats.res</contentPath>
                                    </_x003C_statsRef_x003E_k__BackingField>
                                    <abilityKey dataType="Bool">false</abilityKey>
                                    <active dataType="Bool">true</active>
                                    <attackKey dataType="Bool">false</attackKey>
                                    <gameobj dataType="ObjectRef">3026331258</gameobj>
                                    <itemkey dataType="Bool">false</itemkey>
                                    <offset dataType="Struct" type="Duality.Vector3">
                                      <X dataType="Float">0</X>
                                      <Y dataType="Float">-20</Y>
                                      <Z dataType="Float">0</Z>
                                    </offset>
                                    <ship dataType="ObjectRef">1091678894</ship>
                                    <wStats dataType="Struct" type="The_ESG.WeaponStats" id="1314105095">
                                      <_x003C_attackCooldown_x003E_k__BackingField dataType="Float">10</_x003C_attackCooldown_x003E_k__BackingField>
                                      <_x003C_attackCooldownDefault_x003E_k__BackingField dataType="Float">10</_x003C_attackCooldownDefault_x003E_k__BackingField>
                                      <_x003C_bulletCritChance_x003E_k__BackingField dataType="Float">0.2</_x003C_bulletCritChance_x003E_k__BackingField>
                                      <_x003C_bulletDamage_x003E_k__BackingField dataType="Int">5</_x003C_bulletDamage_x003E_k__BackingField>
                                      <_x003C_bulletRange_x003E_k__BackingField dataType="Float">70</_x003C_bulletRange_x003E_k__BackingField>
                                      <_x003C_bulletSpeed_x003E_k__BackingField dataType="Float">13</_x003C_bulletSpeed_x003E_k__BackingField>
                                      <assetInfo />
                                    </wStats>
                                  </item>
                                  <item dataType="Struct" type="The_ESG.MovementInputCheck" id="502850072">
                                    <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                                      <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                                    </_x003C_stateRef_x003E_k__BackingField>
                                    <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                                      <contentPath dataType="String">Data\Scripts_Resources\Data\Player\PlayerStats.ShipStats.res</contentPath>
                                    </_x003C_statsRef_x003E_k__BackingField>
                                    <active dataType="Bool">true</active>
                                    <Backwards dataType="Bool">false</Backwards>
                                    <Forward dataType="Bool">false</Forward>
                                    <gameobj dataType="ObjectRef">3026331258</gameobj>
                                    <movementVector dataType="Struct" type="Duality.Vector3" />
                                    <pos dataType="Struct" type="Duality.Vector3">
                                      <X dataType="Float">-4.76837158E-07</X>
                                      <Y dataType="Float">204.8393</Y>
                                      <Z dataType="Float">0</Z>
                                    </pos>
                                    <rotateLeft dataType="Bool">false</rotateLeft>
                                    <rotateRight dataType="Bool">false</rotateRight>
                                    <ship dataType="ObjectRef">1091678894</ship>
                                    <spriteAnim dataType="ObjectRef">2733766639</spriteAnim>
                                    <sStats dataType="Struct" type="The_ESG.ShipStats" id="3201856404">
                                      <_x003C_abiltyCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_abiltyCooldownModifier_x003E_k__BackingField>
                                      <_x003C_damageCooldown_x003E_k__BackingField dataType="Float">5</_x003C_damageCooldown_x003E_k__BackingField>
                                      <_x003C_damageModifier_x003E_k__BackingField dataType="Float">0</_x003C_damageModifier_x003E_k__BackingField>
                                      <_x003C_damageMultiplier_x003E_k__BackingField dataType="Float">1</_x003C_damageMultiplier_x003E_k__BackingField>
                                      <_x003C_deathPrefabRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                                      <_x003C_deathSound_x003E_k__BackingField />
                                      <_x003C_friction_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                                      <_x003C_linearSpeed_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                                        <X dataType="Float">0.6</X>
                                        <Y dataType="Float">0.6</Y>
                                        <Z dataType="Float">0</Z>
                                      </_x003C_linearSpeed_x003E_k__BackingField>
                                      <_x003C_maxVelocity_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                                        <X dataType="Float">1.5</X>
                                        <Y dataType="Float">1.5</Y>
                                        <Z dataType="Float">0</Z>
                                      </_x003C_maxVelocity_x003E_k__BackingField>
                                      <_x003C_rotationSpeed_x003E_k__BackingField dataType="Float">0.05</_x003C_rotationSpeed_x003E_k__BackingField>
                                      <_x003C_scoreWorth_x003E_k__BackingField dataType="Int">0</_x003C_scoreWorth_x003E_k__BackingField>
                                      <_x003C_shipCurrentHealthPoints_x003E_k__BackingField dataType="Int">0</_x003C_shipCurrentHealthPoints_x003E_k__BackingField>
                                      <_x003C_shipMaxHealthPoints_x003E_k__BackingField dataType="Int">40</_x003C_shipMaxHealthPoints_x003E_k__BackingField>
                                      <_x003C_shipRamDamage_x003E_k__BackingField dataType="Int">20</_x003C_shipRamDamage_x003E_k__BackingField>
                                      <_x003C_shotCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_shotCooldownModifier_x003E_k__BackingField>
                                      <assetInfo />
                                    </sStats>
                                    <state dataType="Struct" type="The_ESG.StateManager" id="3294319670">
                                      <_x003C_gameState_x003E_k__BackingField dataType="Int">0</_x003C_gameState_x003E_k__BackingField>
                                      <assetInfo />
                                    </state>
                                    <strafeLeft dataType="Bool">false</strafeLeft>
                                    <strafeRight dataType="Bool">false</strafeRight>
                                  </item>
                                  <item dataType="Struct" type="The_ESG.InputMaster" id="3849802971">
                                    <_x003C_customSet_x003E_k__BackingField dataType="Bool">false</_x003C_customSet_x003E_k__BackingField>
                                    <_x003C_item_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="3670578507">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Use Item</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="1388460790">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="2615699680">0, 0, 1</m_buckets>
                                        <m_comparer dataType="Struct" type="System.Collections.Generic.EnumEqualityComparer`1[[Duality.Input.Key]]" id="2749241230" />
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="2586265596" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">98</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="P" value="98" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_item_x003E_k__BackingField>
                                    <_x003C_mainAttack_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="2291165000">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Fire</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="3284730721">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="1590040430">0, 0, 1</m_buckets>
                                        <m_comparer dataType="ObjectRef">2749241230</m_comparer>
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="30548426" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">92</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="J" value="92" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_mainAttack_x003E_k__BackingField>
                                    <_x003C_mBackwards_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="336437761">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Fly Backwards</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="1947325252">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="3405260356">0, 0, 1</m_buckets>
                                        <m_comparer dataType="ObjectRef">2749241230</m_comparer>
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="2669648534" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">101</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="S" value="101" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_mBackwards_x003E_k__BackingField>
                                    <_x003C_mForward_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="3554908054">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Fly Forward</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="3157352255">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="591360686">1, 0, 0</m_buckets>
                                        <m_comparer dataType="ObjectRef">2749241230</m_comparer>
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="2499274" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">105</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="W" value="105" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_mForward_x003E_k__BackingField>
                                    <_x003C_rotateLeft_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="590219935">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Rotate Left</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="2528240186">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="3212893440">1, 0, 0</m_buckets>
                                        <m_comparer dataType="ObjectRef">2749241230</m_comparer>
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="980357582" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">99</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="Q" value="99" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_rotateLeft_x003E_k__BackingField>
                                    <_x003C_rotateRight_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="1553468580">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Rotate Right</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="3693625661">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="1849043494">1, 0, 0</m_buckets>
                                        <m_comparer dataType="ObjectRef">2749241230</m_comparer>
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="231745210" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">87</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="E" value="87" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_rotateRight_x003E_k__BackingField>
                                    <_x003C_shipAbility_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="1483005525">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Ship Ability</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="1296343896">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="264897196">0, 1, 0</m_buckets>
                                        <m_comparer dataType="ObjectRef">2749241230</m_comparer>
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="1751138742" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">97</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="O" value="97" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_shipAbility_x003E_k__BackingField>
                                    <_x003C_strafeLeft_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="3257478130">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Strafe Left</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="3948485355">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="1821864054">0, 0, 1</m_buckets>
                                        <m_comparer dataType="ObjectRef">2749241230</m_comparer>
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="250650906" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">83</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="A" value="83" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_strafeLeft_x003E_k__BackingField>
                                    <_x003C_strafeRight_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="1073141235">
                                      <_x003C_Name_x003E_k__BackingField dataType="String">Strafe Right</_x003C_Name_x003E_k__BackingField>
                                      <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="2427673646">
                                        <m_buckets dataType="Array" type="System.Int32[]" id="589853520">0, 0, 1</m_buckets>
                                        <m_comparer dataType="ObjectRef">2749241230</m_comparer>
                                        <m_count dataType="Int">1</m_count>
                                        <m_freeList dataType="Int">-1</m_freeList>
                                        <m_lastIndex dataType="Int">1</m_lastIndex>
                                        <m_siInfo />
                                        <m_slots dataType="Array" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]][]" id="2042400622" length="3">
                                          <item dataType="Struct" type="System.Collections.Generic.HashSet`1+Slot[[Duality.Input.Key]]">
                                            <hashCode dataType="Int">86</hashCode>
                                            <next dataType="Int">-1</next>
                                            <value dataType="Enum" type="Duality.Input.Key" name="D" value="86" />
                                          </item>
                                        </m_slots>
                                        <m_version dataType="Int">1</m_version>
                                      </associatedKeys>
                                      <ButtonChanged />
                                    </_x003C_strafeRight_x003E_k__BackingField>
                                    <active dataType="Bool">true</active>
                                    <gameobj dataType="ObjectRef">3026331258</gameobj>
                                  </item>
                                  <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1794140486">
                                    <active dataType="Bool">true</active>
                                    <angularDamp dataType="Float">0.3</angularDamp>
                                    <angularVel dataType="Float">0</angularVel>
                                    <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                                    <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                                    <colFilter />
                                    <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                                    <continous dataType="Bool">true</continous>
                                    <explicitInertia dataType="Float">0</explicitInertia>
                                    <explicitMass dataType="Float">0</explicitMass>
                                    <fixedAngle dataType="Bool">false</fixedAngle>
                                    <gameobj dataType="ObjectRef">3026331258</gameobj>
                                    <ignoreGravity dataType="Bool">true</ignoreGravity>
                                    <joints />
                                    <linearDamp dataType="Float">0.3</linearDamp>
                                    <linearVel dataType="Struct" type="Duality.Vector2" />
                                    <revolutions dataType="Float">0</revolutions>
                                    <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="469161258">
                                      <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2636897568">
                                        <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3820341212">
                                          <density dataType="Float">1</density>
                                          <friction dataType="Float">0.3</friction>
                                          <parent dataType="ObjectRef">1794140486</parent>
                                          <restitution dataType="Float">0.3</restitution>
                                          <sensor dataType="Bool">false</sensor>
                                          <vertices dataType="Array" type="Duality.Vector2[]" id="548207300">
                                            <item dataType="Struct" type="Duality.Vector2">
                                              <X dataType="Float">-10.4256134</X>
                                              <Y dataType="Float">-11.2275848</Y>
                                            </item>
                                            <item dataType="Struct" type="Duality.Vector2">
                                              <X dataType="Float">-9.536724</X>
                                              <Y dataType="Float">14.7724152</Y>
                                            </item>
                                            <item dataType="Struct" type="Duality.Vector2">
                                              <X dataType="Float">10.463274</X>
                                              <Y dataType="Float">15.1057587</Y>
                                            </item>
                                            <item dataType="Struct" type="Duality.Vector2">
                                              <X dataType="Float">11.2410526</X>
                                              <Y dataType="Float">-11.5609131</Y>
                                            </item>
                                          </vertices>
                                        </item>
                                      </_items>
                                      <_size dataType="Int">1</_size>
                                      <_version dataType="Int">2</_version>
                                    </shapes>
                                  </item>
                                  <item dataType="Struct" type="The_ESG.Tag" id="1844732727">
                                    <_x003C_id_x003E_k__BackingField dataType="String">_Player</_x003C_id_x003E_k__BackingField>
                                    <_x003C_wideId_x003E_k__BackingField dataType="String">_Friendly</_x003C_wideId_x003E_k__BackingField>
                                    <active dataType="Bool">true</active>
                                    <gameobj dataType="ObjectRef">3026331258</gameobj>
                                  </item>
                                  <item dataType="Struct" type="The_ESG.PlayerBehaviour" id="4252852847">
                                    <_x003C_Stats_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                                      <contentPath dataType="String">Data\Scripts_Resources\Data\Player\PlayerStats.ShipStats.res</contentPath>
                                    </_x003C_Stats_x003E_k__BackingField>
                                    <active dataType="Bool">true</active>
                                    <dmgCooldown dataType="Float">5</dmgCooldown>
                                    <gameobj dataType="ObjectRef">3026331258</gameobj>
                                    <hp dataType="Int">40</hp>
                                    <ship dataType="ObjectRef">1091678894</ship>
                                  </item>
                                </_items>
                                <_size dataType="Int">8</_size>
                                <_version dataType="Int">8</_version>
                              </compList>
                              <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3560308040" surrogate="true">
                                <header />
                                <body>
                                  <keys dataType="Array" type="System.Object[]" id="2842795649">
                                    <item dataType="ObjectRef">1215682926</item>
                                    <item dataType="Type" id="3340737838" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                                    <item dataType="Type" id="4181610698" value="The_ESG.ActionInputCheck" />
                                    <item dataType="Type" id="3605108894" value="The_ESG.MovementInputCheck" />
                                    <item dataType="Type" id="4084286682" value="The_ESG.InputMaster" />
                                    <item dataType="Type" id="189347278" value="Duality.Components.Physics.RigidBody" />
                                    <item dataType="Type" id="982933482" value="The_ESG.Tag" />
                                    <item dataType="Type" id="340904254" value="The_ESG.PlayerBehaviour" />
                                  </keys>
                                  <values dataType="Array" type="System.Object[]" id="1146556256">
                                    <item dataType="ObjectRef">1091678894</item>
                                    <item dataType="ObjectRef">2733766639</item>
                                    <item dataType="ObjectRef">3659035559</item>
                                    <item dataType="ObjectRef">502850072</item>
                                    <item dataType="ObjectRef">3849802971</item>
                                    <item dataType="ObjectRef">1794140486</item>
                                    <item dataType="ObjectRef">1844732727</item>
                                    <item dataType="ObjectRef">4252852847</item>
                                  </values>
                                </body>
                              </compMap>
                              <compTransform dataType="ObjectRef">1091678894</compTransform>
                              <identifier dataType="Struct" type="System.Guid" surrogate="true">
                                <header>
                                  <data dataType="Array" type="System.Byte[]" id="2200709587">/mOO9uz/KUWrHr349OPNrQ==</data>
                                </header>
                                <body />
                              </identifier>
                              <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                              <name dataType="String">PlayerShip</name>
                              <parent />
                              <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="372920737">
                                <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1547314692">
                                  <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="3020755780" length="4">
                                    <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                                      <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1684047944">
                                        <_items dataType="Array" type="System.Int32[]" id="3611724908"></_items>
                                        <_size dataType="Int">0</_size>
                                        <_version dataType="Int">1</_version>
                                      </childIndex>
                                      <componentType dataType="ObjectRef">1215682926</componentType>
                                      <prop dataType="MemberInfo" id="90757342" value="P:Duality.Components.Transform:RelativePos" />
                                      <val dataType="Struct" type="Duality.Vector3">
                                        <X dataType="Float">3.81469727E-06</X>
                                        <Y dataType="Float">160.8393</Y>
                                        <Z dataType="Float">-200</Z>
                                      </val>
                                    </item>
                                  </_items>
                                  <_size dataType="Int">1</_size>
                                  <_version dataType="Int">473</_version>
                                </changes>
                                <obj dataType="ObjectRef">3026331258</obj>
                                <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                                  <contentPath dataType="String">Data\Prefabs\PlayerShip.Prefab.res</contentPath>
                                </prefab>
                              </prefabLink>
                            </player>
                            <text dataType="ObjectRef">48361099</text>
                          </item>
                        </_items>
                        <_size dataType="Int">3</_size>
                        <_version dataType="Int">3</_version>
                      </compList>
                      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2625131112" surrogate="true">
                        <header />
                        <body>
                          <keys dataType="Array" type="System.Object[]" id="934304783">
                            <item dataType="ObjectRef">1215682926</item>
                            <item dataType="ObjectRef">3401369034</item>
                            <item dataType="Type" id="2503927726" value="The_ESG.ShowHP" />
                          </keys>
                          <values dataType="Array" type="System.Object[]" id="1091769312">
                            <item dataType="ObjectRef">666047209</item>
                            <item dataType="ObjectRef">48361099</item>
                            <item dataType="ObjectRef">2328686595</item>
                          </values>
                        </body>
                      </compMap>
                      <compTransform dataType="ObjectRef">666047209</compTransform>
                      <identifier dataType="Struct" type="System.Guid" surrogate="true">
                        <header>
                          <data dataType="Array" type="System.Byte[]" id="2064564573">6ccUHRfYTUGlUV5TtPfCEQ==</data>
                        </header>
                        <body />
                      </identifier>
                      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                      <name dataType="String">Text</name>
                      <parent dataType="ObjectRef">3861271473</parent>
                      <prefabLink />
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">1</_version>
                </children>
                <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3234874808">
                  <_items dataType="Array" type="Duality.Component[]" id="1806184295" length="4">
                    <item dataType="ObjectRef">1926619109</item>
                    <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3568706854">
                      <active dataType="Bool">true</active>
                      <animDuration dataType="Float">5</animDuration>
                      <animFirstFrame dataType="Int">0</animFirstFrame>
                      <animFrameCount dataType="Int">0</animFrameCount>
                      <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                      <animPaused dataType="Bool">false</animPaused>
                      <animTime dataType="Float">0</animTime>
                      <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                        <A dataType="Byte">255</A>
                        <B dataType="Byte">255</B>
                        <G dataType="Byte">255</G>
                        <R dataType="Byte">255</R>
                      </colorTint>
                      <customFrameSequence />
                      <customMat />
                      <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                      <gameobj dataType="ObjectRef">3861271473</gameobj>
                      <offset dataType="Int">0</offset>
                      <pixelGrid dataType="Bool">false</pixelGrid>
                      <rect dataType="Struct" type="Duality.Rect">
                        <H dataType="Float">256</H>
                        <W dataType="Float">256</W>
                        <X dataType="Float">-128</X>
                        <Y dataType="Float">-128</Y>
                      </rect>
                      <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                      <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                        <contentPath dataType="String">Data\Graphics\Sprites\UI\UIsht.Material.res</contentPath>
                      </sharedMat>
                      <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                    </item>
                  </_items>
                  <_size dataType="Int">2</_size>
                  <_version dataType="Int">2</_version>
                </compList>
                <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="378233575" surrogate="true">
                  <header />
                  <body>
                    <keys dataType="Array" type="System.Object[]" id="2557190420">
                      <item dataType="ObjectRef">1215682926</item>
                      <item dataType="ObjectRef">3340737838</item>
                    </keys>
                    <values dataType="Array" type="System.Object[]" id="4189362486">
                      <item dataType="ObjectRef">1926619109</item>
                      <item dataType="ObjectRef">3568706854</item>
                    </values>
                  </body>
                </compMap>
                <compTransform dataType="ObjectRef">1926619109</compTransform>
                <identifier dataType="Struct" type="System.Guid" surrogate="true">
                  <header>
                    <data dataType="Array" type="System.Byte[]" id="1803287472">CxK5Up/SNEGt+Gb3kdhsjg==</data>
                  </header>
                  <body />
                </identifier>
                <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                <name dataType="String">HP</name>
                <parent dataType="ObjectRef">2834562584</parent>
                <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="731429542">
                  <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="4281276537">
                    <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="2138638158" length="8">
                      <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                        <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4110687628">
                          <_items dataType="Array" type="System.Int32[]" id="3671236516"></_items>
                          <_size dataType="Int">0</_size>
                          <_version dataType="Int">1</_version>
                        </childIndex>
                        <componentType dataType="ObjectRef">1215682926</componentType>
                        <prop dataType="MemberInfo" id="2892322294" value="P:Duality.Components.Transform:RelativeScale" />
                        <val dataType="Float">0.299232155</val>
                      </item>
                      <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                        <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1707050776">
                          <_items dataType="Array" type="System.Int32[]" id="754024696">0, 0, 0, 0</_items>
                          <_size dataType="Int">1</_size>
                          <_version dataType="Int">2</_version>
                        </childIndex>
                        <componentType dataType="ObjectRef">1215682926</componentType>
                        <prop dataType="ObjectRef">2892322294</prop>
                        <val dataType="Float">6.68377352</val>
                      </item>
                      <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                        <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1277270930">
                          <_items dataType="Array" type="System.Int32[]" id="1334554266">0, 0, 0, 0</_items>
                          <_size dataType="Int">1</_size>
                          <_version dataType="Int">2</_version>
                        </childIndex>
                        <componentType dataType="ObjectRef">3401369034</componentType>
                        <prop dataType="MemberInfo" id="2356212356" value="P:Duality.Components.Renderers.TextRenderer:Text" />
                        <val dataType="Struct" type="Duality.Drawing.FormattedText" id="602008974">
                          <flowAreas />
                          <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="3789407198">
                            <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                              <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                            </item>
                          </fonts>
                          <icons />
                          <lineAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                          <maxHeight dataType="Int">0</maxHeight>
                          <maxWidth dataType="Int">0</maxWidth>
                          <sourceText dataType="String">2</sourceText>
                          <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                        </val>
                      </item>
                      <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                        <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4247773904">
                          <_items dataType="Array" type="System.Int32[]" id="3574027760"></_items>
                          <_size dataType="Int">0</_size>
                          <_version dataType="Int">1</_version>
                        </childIndex>
                        <componentType dataType="ObjectRef">1215682926</componentType>
                        <prop dataType="ObjectRef">90757342</prop>
                        <val dataType="Struct" type="Duality.Vector3">
                          <X dataType="Float">0.833333</X>
                          <Y dataType="Float">255.833313</Y>
                          <Z dataType="Float">0</Z>
                        </val>
                      </item>
                      <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                        <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4215252458">
                          <_items dataType="Array" type="System.Int32[]" id="1117780530">0, 0, 0, 0</_items>
                          <_size dataType="Int">1</_size>
                          <_version dataType="Int">2</_version>
                        </childIndex>
                        <componentType dataType="ObjectRef">1215682926</componentType>
                        <prop dataType="ObjectRef">90757342</prop>
                        <val dataType="Struct" type="Duality.Vector3">
                          <X dataType="Float">-3.507018</X>
                          <Y dataType="Float">-1.76907849</Y>
                          <Z dataType="Float">0</Z>
                        </val>
                      </item>
                    </_items>
                    <_size dataType="Int">5</_size>
                    <_version dataType="Int">293</_version>
                  </changes>
                  <obj dataType="ObjectRef">3861271473</obj>
                  <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                    <contentPath dataType="String">Data\Graphics\Sprites\UI\HP.Prefab.res</contentPath>
                  </prefab>
                </prefabLink>
              </item>
              <item dataType="Struct" type="Duality.GameObject" id="2939860787">
                <active dataType="Bool">true</active>
                <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="3811970271">
                  <_items dataType="Array" type="Duality.GameObject[]" id="474873454" length="4">
                    <item dataType="Struct" type="Duality.GameObject" id="2700417343">
                      <active dataType="Bool">true</active>
                      <children />
                      <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="487066383">
                        <_items dataType="Array" type="Duality.Component[]" id="4204439470" length="4">
                          <item dataType="Struct" type="Duality.Components.Transform" id="765764979">
                            <active dataType="Bool">true</active>
                            <angle dataType="Float">0</angle>
                            <angleAbs dataType="Float">0</angleAbs>
                            <angleVel dataType="Float">0</angleVel>
                            <angleVelAbs dataType="Float">0</angleVelAbs>
                            <deriveAngle dataType="Bool">true</deriveAngle>
                            <gameobj dataType="ObjectRef">2700417343</gameobj>
                            <ignoreParent dataType="Bool">false</ignoreParent>
                            <parentTransform dataType="Struct" type="Duality.Components.Transform" id="1005208423">
                              <active dataType="Bool">true</active>
                              <angle dataType="Float">0</angle>
                              <angleAbs dataType="Float">0</angleAbs>
                              <angleVel dataType="Float">0</angleVel>
                              <angleVelAbs dataType="Float">0</angleVelAbs>
                              <deriveAngle dataType="Bool">true</deriveAngle>
                              <gameobj dataType="ObjectRef">2939860787</gameobj>
                              <ignoreParent dataType="Bool">false</ignoreParent>
                              <parentTransform dataType="ObjectRef">899910220</parentTransform>
                              <pos dataType="Struct" type="Duality.Vector3">
                                <X dataType="Float">80.83333</X>
                                <Y dataType="Float">254.999969</Y>
                                <Z dataType="Float">0</Z>
                              </pos>
                              <posAbs dataType="Struct" type="Duality.Vector3">
                                <X dataType="Float">172.629044</X>
                                <Y dataType="Float">268.840881</Y>
                                <Z dataType="Float">-100</Z>
                              </posAbs>
                              <scale dataType="Float">0.3</scale>
                              <scaleAbs dataType="Float">0.3</scaleAbs>
                              <vel dataType="Struct" type="Duality.Vector3" />
                              <velAbs dataType="Struct" type="Duality.Vector3" />
                            </parentTransform>
                            <pos dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">-0.000101725258</X>
                              <Y dataType="Float">16.6630039</Y>
                              <Z dataType="Float">0</Z>
                            </pos>
                            <posAbs dataType="Struct" type="Duality.Vector3">
                              <X dataType="Float">172.629013</X>
                              <Y dataType="Float">273.839783</Y>
                              <Z dataType="Float">-100</Z>
                            </posAbs>
                            <scale dataType="Float">5.52157164</scale>
                            <scaleAbs dataType="Float">1.65647161</scaleAbs>
                            <vel dataType="Struct" type="Duality.Vector3" />
                            <velAbs dataType="Struct" type="Duality.Vector3" />
                          </item>
                          <item dataType="Struct" type="Duality.Components.Renderers.TextRenderer" id="148078869">
                            <active dataType="Bool">true</active>
                            <blockAlign dataType="Enum" type="Duality.Alignment" name="Center" value="0" />
                            <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                              <A dataType="Byte">255</A>
                              <B dataType="Byte">255</B>
                              <G dataType="Byte">255</G>
                              <R dataType="Byte">255</R>
                            </colorTint>
                            <customMat />
                            <gameobj dataType="ObjectRef">2700417343</gameobj>
                            <iconMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]" />
                            <offset dataType="Int">0</offset>
                            <text dataType="Struct" type="Duality.Drawing.FormattedText" id="630954773">
                              <flowAreas />
                              <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="2203889782">
                                <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                                  <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                                </item>
                              </fonts>
                              <icons />
                              <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                              <maxHeight dataType="Int">0</maxHeight>
                              <maxWidth dataType="Int">0</maxWidth>
                              <sourceText dataType="String">2</sourceText>
                              <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                            </text>
                            <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                          </item>
                          <item dataType="Struct" type="The_ESG.ShowScore" id="1639301479">
                            <active dataType="Bool">true</active>
                            <gameobj dataType="ObjectRef">2700417343</gameobj>
                            <text dataType="ObjectRef">148078869</text>
                          </item>
                        </_items>
                        <_size dataType="Int">3</_size>
                        <_version dataType="Int">3</_version>
                      </compList>
                      <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="59692000" surrogate="true">
                        <header />
                        <body>
                          <keys dataType="Array" type="System.Object[]" id="598752933">
                            <item dataType="ObjectRef">1215682926</item>
                            <item dataType="ObjectRef">3401369034</item>
                            <item dataType="Type" id="3748425110" value="The_ESG.ShowScore" />
                          </keys>
                          <values dataType="Array" type="System.Object[]" id="4002503784">
                            <item dataType="ObjectRef">765764979</item>
                            <item dataType="ObjectRef">148078869</item>
                            <item dataType="ObjectRef">1639301479</item>
                          </values>
                        </body>
                      </compMap>
                      <compTransform dataType="ObjectRef">765764979</compTransform>
                      <identifier dataType="Struct" type="System.Guid" surrogate="true">
                        <header>
                          <data dataType="Array" type="System.Byte[]" id="1951867759">J17SwCf3IkevX5fwG50mOg==</data>
                        </header>
                        <body />
                      </identifier>
                      <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                      <name dataType="String">Text</name>
                      <parent dataType="ObjectRef">2939860787</parent>
                      <prefabLink />
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">1</_version>
                </children>
                <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2892835104">
                  <_items dataType="Array" type="Duality.Component[]" id="1616166613" length="4">
                    <item dataType="ObjectRef">1005208423</item>
                    <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2647296168">
                      <active dataType="Bool">true</active>
                      <animDuration dataType="Float">5</animDuration>
                      <animFirstFrame dataType="Int">2</animFirstFrame>
                      <animFrameCount dataType="Int">0</animFrameCount>
                      <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
                      <animPaused dataType="Bool">true</animPaused>
                      <animTime dataType="Float">0</animTime>
                      <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                        <A dataType="Byte">255</A>
                        <B dataType="Byte">255</B>
                        <G dataType="Byte">255</G>
                        <R dataType="Byte">255</R>
                      </colorTint>
                      <customFrameSequence />
                      <customMat />
                      <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                      <gameobj dataType="ObjectRef">2939860787</gameobj>
                      <offset dataType="Int">0</offset>
                      <pixelGrid dataType="Bool">false</pixelGrid>
                      <rect dataType="Struct" type="Duality.Rect">
                        <H dataType="Float">256</H>
                        <W dataType="Float">256</W>
                        <X dataType="Float">-128</X>
                        <Y dataType="Float">-128</Y>
                      </rect>
                      <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                      <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                        <contentPath dataType="String">Data\Graphics\Sprites\UI\UIsht.Material.res</contentPath>
                      </sharedMat>
                      <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
                    </item>
                  </_items>
                  <_size dataType="Int">2</_size>
                  <_version dataType="Int">2</_version>
                </compList>
                <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1064792909" surrogate="true">
                  <header />
                  <body>
                    <keys dataType="Array" type="System.Object[]" id="3738781092">
                      <item dataType="ObjectRef">1215682926</item>
                      <item dataType="ObjectRef">3340737838</item>
                    </keys>
                    <values dataType="Array" type="System.Object[]" id="3315619606">
                      <item dataType="ObjectRef">1005208423</item>
                      <item dataType="ObjectRef">2647296168</item>
                    </values>
                  </body>
                </compMap>
                <compTransform dataType="ObjectRef">1005208423</compTransform>
                <identifier dataType="Struct" type="System.Guid" surrogate="true">
                  <header>
                    <data dataType="Array" type="System.Byte[]" id="1802494112">S/EFvD9e00a8100mg3i/qw==</data>
                  </header>
                  <body />
                </identifier>
                <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
                <name dataType="String">Score</name>
                <parent dataType="ObjectRef">2834562584</parent>
                <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="4708342">
                  <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="3535960963">
                    <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="37269286" length="4">
                      <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                        <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1852232604">
                          <_items dataType="Array" type="System.Int32[]" id="3153951172">0, 0, 0, 0</_items>
                          <_size dataType="Int">1</_size>
                          <_version dataType="Int">2</_version>
                        </childIndex>
                        <componentType dataType="ObjectRef">1215682926</componentType>
                        <prop dataType="ObjectRef">2892322294</prop>
                        <val dataType="Float">5.52157164</val>
                      </item>
                      <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                        <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3925582870">
                          <_items dataType="Array" type="System.Int32[]" id="739248438">0, 0, 0, 0</_items>
                          <_size dataType="Int">1</_size>
                          <_version dataType="Int">2</_version>
                        </childIndex>
                        <componentType dataType="ObjectRef">3401369034</componentType>
                        <prop dataType="ObjectRef">2356212356</prop>
                        <val dataType="Struct" type="Duality.Drawing.FormattedText" id="3165947400">
                          <flowAreas />
                          <fonts dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Font]][]" id="4072363672">
                            <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Font]]">
                              <contentPath dataType="String">Data\Consolas.Font.res</contentPath>
                            </item>
                          </fonts>
                          <icons />
                          <lineAlign dataType="Enum" type="Duality.Alignment" name="Left" value="1" />
                          <maxHeight dataType="Int">0</maxHeight>
                          <maxWidth dataType="Int">0</maxWidth>
                          <sourceText dataType="String">2</sourceText>
                          <wrapMode dataType="Enum" type="Duality.Drawing.FormattedText+WrapMode" name="Word" value="1" />
                        </val>
                      </item>
                      <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                        <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="4038501298">
                          <_items dataType="Array" type="System.Int32[]" id="3824976010">0, 0, 0, 0</_items>
                          <_size dataType="Int">1</_size>
                          <_version dataType="Int">2</_version>
                        </childIndex>
                        <componentType dataType="ObjectRef">1215682926</componentType>
                        <prop dataType="ObjectRef">90757342</prop>
                        <val dataType="Struct" type="Duality.Vector3">
                          <X dataType="Float">-0.000101725258</X>
                          <Y dataType="Float">16.6630039</Y>
                          <Z dataType="Float">0</Z>
                        </val>
                      </item>
                    </_items>
                    <_size dataType="Int">3</_size>
                    <_version dataType="Int">215</_version>
                  </changes>
                  <obj dataType="ObjectRef">2939860787</obj>
                  <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                    <contentPath dataType="String">Data\Graphics\Sprites\UI\Score.Prefab.res</contentPath>
                  </prefab>
                </prefabLink>
              </item>
            </_items>
            <_size dataType="Int">2</_size>
            <_version dataType="Int">2</_version>
          </children>
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1671056022">
            <_items dataType="Array" type="Duality.Component[]" id="3195540430" length="4">
              <item dataType="ObjectRef">899910220</item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">1</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3905280000" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="980862024">
                <item dataType="ObjectRef">1215682926</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="4268138718">
                <item dataType="ObjectRef">899910220</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">899910220</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="3042892468">MLS6xEnwTUqmB5xG9PgYZQ==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">UI</name>
          <parent dataType="ObjectRef">3875542296</parent>
          <prefabLink />
        </item>
      </_items>
      <_size dataType="Int">2</_size>
      <_version dataType="Int">2</_version>
    </children>
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2021630944">
      <_items dataType="Array" type="Duality.Component[]" id="947956389" length="4">
        <item dataType="ObjectRef">1940889932</item>
        <item dataType="Struct" type="Duality.Components.Camera" id="117850807">
          <active dataType="Bool">true</active>
          <farZ dataType="Float">10000</farZ>
          <focusDist dataType="Float">600</focusDist>
          <gameobj dataType="ObjectRef">3875542296</gameobj>
          <nearZ dataType="Float">0</nearZ>
          <passes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Camera+Pass]]" id="2022992917">
            <_items dataType="Array" type="Duality.Components.Camera+Pass[]" id="1644101238" length="8">
              <item dataType="Struct" type="Duality.Components.Camera+Pass" id="1685305312">
                <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                <clearDepth dataType="Float">1</clearDepth>
                <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="All" value="3" />
                <input />
                <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="PerspectiveWorld" value="0" />
                <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="AllGroups" value="2147483647" />
              </item>
              <item dataType="Struct" type="Duality.Components.Camera+Pass" id="1435800462">
                <clearColor dataType="Struct" type="Duality.Drawing.ColorRgba" />
                <clearDepth dataType="Float">1</clearDepth>
                <clearFlags dataType="Enum" type="Duality.Drawing.ClearFlag" name="None" value="0" />
                <input />
                <matrixMode dataType="Enum" type="Duality.Drawing.RenderMatrix" name="OrthoScreen" value="1" />
                <output dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.RenderTarget]]" />
                <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
              </item>
            </_items>
            <_size dataType="Int">2</_size>
            <_version dataType="Int">115634</_version>
          </passes>
          <perspective dataType="Enum" type="Duality.Drawing.PerspectiveMode" name="Flat" value="0" />
          <visibilityMask dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="All" value="4294967295" />
        </item>
      </_items>
      <_size dataType="Int">2</_size>
      <_version dataType="Int">2</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2407286877" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3916346276">
          <item dataType="ObjectRef">1215682926</item>
          <item dataType="Type" id="3609109700" value="Duality.Components.Camera" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1501216534">
          <item dataType="ObjectRef">1940889932</item>
          <item dataType="ObjectRef">117850807</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">1940889932</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3834729120">ulZ8A3ykCEOEaEBaRpviqg==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Camera</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
