﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="1183285269">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="427043990">
      <_items dataType="Array" type="Duality.Component[]" id="988029472" length="8">
        <item dataType="Struct" type="Duality.Components.Transform" id="3543600201">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">1183285269</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">200.555573</X>
            <Y dataType="Float">-204.166687</Y>
            <Z dataType="Float">-200</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">200.555573</X>
            <Y dataType="Float">-204.166687</Y>
            <Z dataType="Float">-200</Z>
          </posAbs>
          <scale dataType="Float">0.15</scale>
          <scaleAbs dataType="Float">0.15</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="890720650">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">5</animDuration>
          <animFirstFrame dataType="Int">4</animFirstFrame>
          <animFrameCount dataType="Int">0</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
          <animPaused dataType="Bool">true</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">1183285269</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">256</H>
            <W dataType="Float">256</W>
            <X dataType="Float">-128</X>
            <Y dataType="Float">-128</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="4246061793">
          <active dataType="Bool">true</active>
          <angularDamp dataType="Float">0.3</angularDamp>
          <angularVel dataType="Float">0</angularVel>
          <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
          <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
          <colFilter />
          <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
          <continous dataType="Bool">true</continous>
          <explicitInertia dataType="Float">0</explicitInertia>
          <explicitMass dataType="Float">0</explicitMass>
          <fixedAngle dataType="Bool">false</fixedAngle>
          <gameobj dataType="ObjectRef">1183285269</gameobj>
          <ignoreGravity dataType="Bool">true</ignoreGravity>
          <joints />
          <linearDamp dataType="Float">0.3</linearDamp>
          <linearVel dataType="Struct" type="Duality.Vector2" />
          <revolutions dataType="Float">0</revolutions>
          <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3845568261">
            <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3784719958">
              <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3171192864">
                <density dataType="Float">1</density>
                <friction dataType="Float">0.3</friction>
                <parent dataType="ObjectRef">4246061793</parent>
                <restitution dataType="Float">0.3</restitution>
                <sensor dataType="Bool">false</sensor>
                <vertices dataType="Array" type="Duality.Vector2[]" id="2890341340">
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-75.5045547</X>
                    <Y dataType="Float">-82.23653</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">70.7917252</X>
                    <Y dataType="Float">-81.4958649</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">63.7546768</X>
                    <Y dataType="Float">54.0597534</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-66.2453156</X>
                    <Y dataType="Float">48.5041275</Y>
                  </item>
                </vertices>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">44</_version>
          </shapes>
        </item>
        <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="743001610">
          <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
          <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
            <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
          </_x003C_stateRef_x003E_k__BackingField>
          <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
          </_x003C_statsRef_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <dirChange dataType="Float">0</dirChange>
          <dirChangeMax dataType="Float">68.37547</dirChangeMax>
          <gameobj dataType="ObjectRef">1183285269</gameobj>
          <movVector dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-2.45051217</X>
            <Y dataType="Float">0</Y>
            <Z dataType="Float">0</Z>
          </movVector>
          <random dataType="Struct" type="System.Random" id="4260971202">
            <inext dataType="Int">17</inext>
            <inextp dataType="Int">38</inextp>
            <SeedArray dataType="Array" type="System.Int32[]" id="1306204688">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 547078764, 388675363, 1367965900, 123291023, 1874489040, 1413710203, 1601705134, 1426702234, 1135458749, 2126763751, 1308163136, 241866884, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
          </random>
          <ship dataType="ObjectRef">3543600201</ship>
          <stats dataType="Struct" type="The_ESG.ShipStats" id="2414766602">
            <_x003C_abiltyCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_abiltyCooldownModifier_x003E_k__BackingField>
            <_x003C_damageCooldown_x003E_k__BackingField dataType="Float">0</_x003C_damageCooldown_x003E_k__BackingField>
            <_x003C_damageModifier_x003E_k__BackingField dataType="Float">0</_x003C_damageModifier_x003E_k__BackingField>
            <_x003C_damageMultiplier_x003E_k__BackingField dataType="Float">1</_x003C_damageMultiplier_x003E_k__BackingField>
            <_x003C_deathPrefabRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\VFX\BadGuyFlyDestroy.Prefab.res</contentPath>
            </_x003C_deathPrefabRef_x003E_k__BackingField>
            <_x003C_deathSound_x003E_k__BackingField />
            <_x003C_friction_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
            <_x003C_linearSpeed_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">5</X>
              <Y dataType="Float">1.05</Y>
              <Z dataType="Float">0</Z>
            </_x003C_linearSpeed_x003E_k__BackingField>
            <_x003C_maxVelocity_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
            <_x003C_rotationSpeed_x003E_k__BackingField dataType="Float">0.05</_x003C_rotationSpeed_x003E_k__BackingField>
            <_x003C_scoreWorth_x003E_k__BackingField dataType="Int">2</_x003C_scoreWorth_x003E_k__BackingField>
            <_x003C_shipCurrentHealthPoints_x003E_k__BackingField dataType="Int">0</_x003C_shipCurrentHealthPoints_x003E_k__BackingField>
            <_x003C_shipMaxHealthPoints_x003E_k__BackingField dataType="Int">6</_x003C_shipMaxHealthPoints_x003E_k__BackingField>
            <_x003C_shipRamDamage_x003E_k__BackingField dataType="Int">10</_x003C_shipRamDamage_x003E_k__BackingField>
            <_x003C_shotCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_shotCooldownModifier_x003E_k__BackingField>
            <assetInfo />
          </stats>
          <xModifier dataType="Float">-2.45051217</xModifier>
        </item>
        <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="3972084151">
          <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
          </_x003C_enemyStatsRef_x003E_k__BackingField>
          <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
            <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
          </_x003C_stateRef_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <enemyStats dataType="ObjectRef">2414766602</enemyStats>
          <gameobj dataType="ObjectRef">1183285269</gameobj>
          <hp dataType="Int">6</hp>
          <ship dataType="ObjectRef">3543600201</ship>
        </item>
        <item dataType="Struct" type="The_ESG.Tag" id="1686738">
          <_x003C_id_x003E_k__BackingField dataType="String">_Enemy</_x003C_id_x003E_k__BackingField>
          <_x003C_wideId_x003E_k__BackingField dataType="String">_Solid</_x003C_wideId_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">1183285269</gameobj>
        </item>
      </_items>
      <_size dataType="Int">6</_size>
      <_version dataType="Int">6</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2869576922" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="403838052">
          <item dataType="Type" id="1997740484" value="Duality.Components.Transform" />
          <item dataType="Type" id="2846699926" value="Duality.Components.Renderers.AnimSpriteRenderer" />
          <item dataType="Type" id="432830592" value="Duality.Components.Physics.RigidBody" />
          <item dataType="Type" id="2930106402" value="The_ESG.BadguyFlyBehaviour" />
          <item dataType="Type" id="753597916" value="The_ESG.GeneralEnemyBehaviour" />
          <item dataType="Type" id="3171285246" value="The_ESG.Tag" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1969472534">
          <item dataType="ObjectRef">3543600201</item>
          <item dataType="ObjectRef">890720650</item>
          <item dataType="ObjectRef">4246061793</item>
          <item dataType="ObjectRef">743001610</item>
          <item dataType="ObjectRef">3972084151</item>
          <item dataType="ObjectRef">1686738</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">3543600201</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="2455930208">Q8ZUbNNhCUaG3ks6ulj+Xw==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">BadGuyFly</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
