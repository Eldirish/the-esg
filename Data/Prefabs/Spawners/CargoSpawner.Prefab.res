﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="1069754209">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3290591354">
      <_items dataType="Array" type="Duality.Component[]" id="265925504" length="4">
        <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="1716419013">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">1069754209</gameobj>
          <t />
        </item>
        <item dataType="Struct" type="The_ESG.Tag" id="4183122974">
          <_x003C_id_x003E_k__BackingField dataType="String">_Spawner</_x003C_id_x003E_k__BackingField>
          <_x003C_wideId_x003E_k__BackingField />
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">1069754209</gameobj>
        </item>
        <item dataType="Struct" type="Duality.Components.Transform" id="3430069141">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">1069754209</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3" />
          <posAbs dataType="Struct" type="Duality.Vector3" />
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
      </_items>
      <_size dataType="Int">3</_size>
      <_version dataType="Int">3</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1237762874" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="310553536">
          <item dataType="Type" id="1300628252" value="The_ESG.SpawnerBehaviour" />
          <item dataType="Type" id="4256280086" value="The_ESG.Tag" />
          <item dataType="Type" id="2908577928" value="Duality.Components.Transform" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1035746382">
          <item dataType="ObjectRef">1716419013</item>
          <item dataType="ObjectRef">4183122974</item>
          <item dataType="ObjectRef">3430069141</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">3430069141</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="644931420">cmeB70IiukCxvvKhe8XlqQ==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">CargoSpawner</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
