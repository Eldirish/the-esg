﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="865746787">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="600086928">
      <_items dataType="Array" type="Duality.Component[]" id="339366204" length="4">
        <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="1512411591">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">865746787</gameobj>
          <t />
        </item>
        <item dataType="Struct" type="Duality.Components.Transform" id="3226061719">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">865746787</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3" />
          <posAbs dataType="Struct" type="Duality.Vector3" />
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="The_ESG.Tag" id="3979115552">
          <_x003C_id_x003E_k__BackingField dataType="String">_Spawner</_x003C_id_x003E_k__BackingField>
          <_x003C_wideId_x003E_k__BackingField />
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">865746787</gameobj>
        </item>
      </_items>
      <_size dataType="Int">3</_size>
      <_version dataType="Int">3</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2932372206" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3284857826">
          <item dataType="Type" id="417984144" value="The_ESG.SpawnerBehaviour" />
          <item dataType="Type" id="1454890734" value="Duality.Components.Transform" />
          <item dataType="Type" id="1639629676" value="The_ESG.Tag" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1870322826">
          <item dataType="ObjectRef">1512411591</item>
          <item dataType="ObjectRef">3226061719</item>
          <item dataType="ObjectRef">3979115552</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">3226061719</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="1038182930">3nroj3JF7kWfAPmRJ3NOUw==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">BossSpawner</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
