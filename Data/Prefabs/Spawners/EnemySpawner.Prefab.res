﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="4100712608">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="14626551">
      <_items dataType="Array" type="Duality.Component[]" id="2632678030" length="4">
        <item dataType="Struct" type="Duality.Components.Transform" id="2166060244">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">4100712608</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-6.09259272</X>
            <Y dataType="Float">-433.7963</Y>
            <Z dataType="Float">0</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-6.09259272</X>
            <Y dataType="Float">-433.7963</Y>
            <Z dataType="Float">0</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="The_ESG.Tag" id="2919114077">
          <_x003C_id_x003E_k__BackingField dataType="String">_Spawner</_x003C_id_x003E_k__BackingField>
          <_x003C_wideId_x003E_k__BackingField />
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">4100712608</gameobj>
        </item>
        <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="452410116">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">4100712608</gameobj>
          <t />
        </item>
      </_items>
      <_size dataType="Int">3</_size>
      <_version dataType="Int">3</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2752742976" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="487422525">
          <item dataType="Type" id="4290061350" value="Duality.Components.Transform" />
          <item dataType="Type" id="221851322" value="The_ESG.Tag" />
          <item dataType="Type" id="2208672550" value="The_ESG.SpawnerBehaviour" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="3537213112">
          <item dataType="ObjectRef">2166060244</item>
          <item dataType="ObjectRef">2919114077</item>
          <item dataType="ObjectRef">452410116</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">2166060244</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="2195532823">GaUkC08Swk2hlO1m2zxcew==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">EnemySpawner</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
