﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="2911616635">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1314643272">
      <_items dataType="Array" type="Duality.Component[]" id="1757423724" length="4">
        <item dataType="Struct" type="The_ESG.SpawnerBehaviour" id="3558281439">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">2911616635</gameobj>
          <t />
        </item>
        <item dataType="Struct" type="Duality.Components.Transform" id="976964271">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">2911616635</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3" />
          <posAbs dataType="Struct" type="Duality.Vector3" />
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="The_ESG.Tag" id="1730018104">
          <_x003C_id_x003E_k__BackingField dataType="String">_Spawner</_x003C_id_x003E_k__BackingField>
          <_x003C_wideId_x003E_k__BackingField />
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">2911616635</gameobj>
        </item>
      </_items>
      <_size dataType="Int">3</_size>
      <_version dataType="Int">3</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1935557854" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="329563658">
          <item dataType="Type" id="2605997792" value="The_ESG.SpawnerBehaviour" />
          <item dataType="Type" id="4086820750" value="Duality.Components.Transform" />
          <item dataType="Type" id="2124542460" value="The_ESG.Tag" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="2725485594">
          <item dataType="ObjectRef">3558281439</item>
          <item dataType="ObjectRef">976964271</item>
          <item dataType="ObjectRef">1730018104</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">976964271</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="625933546">7vqN6wZ2WkSU5TBRT6axzw==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">HazardSpawner</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
