﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="3353091028">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1395749267">
      <_items dataType="Array" type="Duality.Component[]" id="1115292902" length="4">
        <item dataType="Struct" type="Duality.Components.Transform" id="1418438664">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">3353091028</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-101.111115</X>
            <Y dataType="Float">29.4444466</Y>
            <Z dataType="Float">0</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-101.111115</X>
            <Y dataType="Float">29.4444466</Y>
            <Z dataType="Float">0</Z>
          </posAbs>
          <scale dataType="Float">0.9</scale>
          <scaleAbs dataType="Float">0.9</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3060526409">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">0.2</animDuration>
          <animFirstFrame dataType="Int">4</animFirstFrame>
          <animFrameCount dataType="Int">40</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2186679753">
            <_items dataType="Array" type="System.Int32[]" id="1233950350">36, 37, 38, 39, 40, 0, 0, 0</_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">23</_version>
          </customFrameSequence>
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">3353091028</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">32</H>
            <W dataType="Float">32</W>
            <X dataType="Float">-16</X>
            <Y dataType="Float">-16</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Graphics\Sprites\Player\playersheet.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="The_ESG.VFXPrefBehaviour" id="1563764295">
          <active dataType="Bool">true</active>
          <anim dataType="ObjectRef">3060526409</anim>
          <gameobj dataType="ObjectRef">3353091028</gameobj>
        </item>
      </_items>
      <_size dataType="Int">3</_size>
      <_version dataType="Int">3</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1156408056" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3344937977">
          <item dataType="Type" id="854637134" value="Duality.Components.Transform" />
          <item dataType="Type" id="655120970" value="Duality.Components.Renderers.AnimSpriteRenderer" />
          <item dataType="Type" id="3767497726" value="The_ESG.VFXPrefBehaviour" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1904142464">
          <item dataType="ObjectRef">1418438664</item>
          <item dataType="ObjectRef">3060526409</item>
          <item dataType="ObjectRef">1563764295</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">1418438664</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="4097131003">xFxcAoDs+UewuynqLGIy6Q==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">BaseShotDestroy</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
