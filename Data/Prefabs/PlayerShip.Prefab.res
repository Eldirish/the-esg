﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="213327579">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2178791592">
      <_items dataType="Array" type="Duality.Component[]" id="195277484">
        <item dataType="Struct" type="Duality.Components.Transform" id="2573642511">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">213327579</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-4.76837158E-07</X>
            <Y dataType="Float">185.950409</Y>
            <Z dataType="Float">0</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-4.76837158E-07</X>
            <Y dataType="Float">185.950409</Y>
            <Z dataType="Float">0</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="4215730256">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">5</animDuration>
          <animFirstFrame dataType="Int">0</animFirstFrame>
          <animFrameCount dataType="Int">144</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">true</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence />
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">213327579</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">32</H>
            <W dataType="Float">32</W>
            <X dataType="Float">-16</X>
            <Y dataType="Float">-16</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Graphics\Sprites\Player\playersheet.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="The_ESG.InputMaster" id="1036799292">
          <_x003C_customSet_x003E_k__BackingField dataType="Bool">false</_x003C_customSet_x003E_k__BackingField>
          <_x003C_item_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="1531158476">
            <_x003C_Name_x003E_k__BackingField dataType="String">Use Item</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="3828372644">
              <m_buckets />
              <m_comparer dataType="Struct" type="System.Collections.Generic.EnumEqualityComparer`1[[Duality.Input.Key]]" id="1166932164" />
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_item_x003E_k__BackingField>
          <_x003C_mainAttack_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="2148901622">
            <_x003C_Name_x003E_k__BackingField dataType="String">Fire</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="3206568006">
              <m_buckets />
              <m_comparer dataType="ObjectRef">1166932164</m_comparer>
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_mainAttack_x003E_k__BackingField>
          <_x003C_mBackwards_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="2668324824">
            <_x003C_Name_x003E_k__BackingField dataType="String">Fly Backwards</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="3830683512">
              <m_buckets />
              <m_comparer dataType="ObjectRef">1166932164</m_comparer>
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_mBackwards_x003E_k__BackingField>
          <_x003C_mForward_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="2420373522">
            <_x003C_Name_x003E_k__BackingField dataType="String">Fly Forward</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="596843226">
              <m_buckets />
              <m_comparer dataType="ObjectRef">1166932164</m_comparer>
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_mForward_x003E_k__BackingField>
          <_x003C_rotateLeft_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="464536644">
            <_x003C_Name_x003E_k__BackingField dataType="String">Rotate Left</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="703929692">
              <m_buckets />
              <m_comparer dataType="ObjectRef">1166932164</m_comparer>
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_rotateLeft_x003E_k__BackingField>
          <_x003C_rotateRight_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="1962826510">
            <_x003C_Name_x003E_k__BackingField dataType="String">Rotate Right</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="373879326">
              <m_buckets />
              <m_comparer dataType="ObjectRef">1166932164</m_comparer>
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_rotateRight_x003E_k__BackingField>
          <_x003C_shipAbility_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="2574908816">
            <_x003C_Name_x003E_k__BackingField dataType="String">Ship Ability</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="433148400">
              <m_buckets />
              <m_comparer dataType="ObjectRef">1166932164</m_comparer>
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_shipAbility_x003E_k__BackingField>
          <_x003C_strafeLeft_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="3732615658">
            <_x003C_Name_x003E_k__BackingField dataType="String">Strafe Left</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="2009677170">
              <m_buckets />
              <m_comparer dataType="ObjectRef">1166932164</m_comparer>
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_strafeLeft_x003E_k__BackingField>
          <_x003C_strafeRight_x003E_k__BackingField dataType="Struct" type="MFEP.Duality.Plugins.InputPlugin.VirtualButton" id="18420156">
            <_x003C_Name_x003E_k__BackingField dataType="String">Strafe Right</_x003C_Name_x003E_k__BackingField>
            <associatedKeys dataType="Struct" type="System.Collections.Generic.HashSet`1[[Duality.Input.Key]]" id="588485396">
              <m_buckets />
              <m_comparer dataType="ObjectRef">1166932164</m_comparer>
              <m_count dataType="Int">0</m_count>
              <m_freeList dataType="Int">-1</m_freeList>
              <m_lastIndex dataType="Int">0</m_lastIndex>
              <m_siInfo />
              <m_slots />
              <m_version dataType="Int">0</m_version>
            </associatedKeys>
            <ButtonChanged />
          </_x003C_strafeRight_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">213327579</gameobj>
        </item>
        <item dataType="Struct" type="The_ESG.MovementInputCheck" id="1984813689">
          <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
            <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
          </_x003C_stateRef_x003E_k__BackingField>
          <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Player\PlayerStats.ShipStats.res</contentPath>
          </_x003C_statsRef_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <Backwards dataType="Bool">false</Backwards>
          <Forward dataType="Bool">false</Forward>
          <gameobj dataType="ObjectRef">213327579</gameobj>
          <movementVector dataType="Struct" type="Duality.Vector3" />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">-4.76837158E-07</X>
            <Y dataType="Float">185.950409</Y>
            <Z dataType="Float">0</Z>
          </pos>
          <rotateLeft dataType="Bool">false</rotateLeft>
          <rotateRight dataType="Bool">false</rotateRight>
          <ship dataType="ObjectRef">2573642511</ship>
          <spriteAnim dataType="ObjectRef">4215730256</spriteAnim>
          <sStats dataType="Struct" type="The_ESG.ShipStats" id="2095297317">
            <_x003C_abiltyCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_abiltyCooldownModifier_x003E_k__BackingField>
            <_x003C_damageCooldown_x003E_k__BackingField dataType="Float">0</_x003C_damageCooldown_x003E_k__BackingField>
            <_x003C_damageModifier_x003E_k__BackingField dataType="Float">0</_x003C_damageModifier_x003E_k__BackingField>
            <_x003C_damageMultiplier_x003E_k__BackingField dataType="Float">1</_x003C_damageMultiplier_x003E_k__BackingField>
            <_x003C_deathPrefabRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
            <_x003C_deathSound_x003E_k__BackingField />
            <_x003C_friction_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
            <_x003C_linearSpeed_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">0.6</X>
              <Y dataType="Float">0.6</Y>
              <Z dataType="Float">0</Z>
            </_x003C_linearSpeed_x003E_k__BackingField>
            <_x003C_maxVelocity_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
              <X dataType="Float">1.5</X>
              <Y dataType="Float">1.5</Y>
              <Z dataType="Float">0</Z>
            </_x003C_maxVelocity_x003E_k__BackingField>
            <_x003C_rotationSpeed_x003E_k__BackingField dataType="Float">0.05</_x003C_rotationSpeed_x003E_k__BackingField>
            <_x003C_scoreWorth_x003E_k__BackingField dataType="Int">0</_x003C_scoreWorth_x003E_k__BackingField>
            <_x003C_shipCurrentHealthPoints_x003E_k__BackingField dataType="Int">0</_x003C_shipCurrentHealthPoints_x003E_k__BackingField>
            <_x003C_shipMaxHealthPoints_x003E_k__BackingField dataType="Int">40</_x003C_shipMaxHealthPoints_x003E_k__BackingField>
            <_x003C_shipRamDamage_x003E_k__BackingField dataType="Int">0</_x003C_shipRamDamage_x003E_k__BackingField>
            <_x003C_shotCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_shotCooldownModifier_x003E_k__BackingField>
            <assetInfo />
          </sStats>
          <state dataType="Struct" type="The_ESG.StateManager" id="3033601384">
            <_x003C_gameState_x003E_k__BackingField dataType="Int">0</_x003C_gameState_x003E_k__BackingField>
            <assetInfo />
          </state>
          <strafeLeft dataType="Bool">false</strafeLeft>
          <strafeRight dataType="Bool">false</strafeRight>
        </item>
        <item dataType="Struct" type="The_ESG.ActionInputCheck" id="846031880">
          <_x003C_shotPref_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
            <contentPath dataType="String">Data\Prefabs\PlayerShot.Prefab.res</contentPath>
          </_x003C_shotPref_x003E_k__BackingField>
          <_x003C_shotSound_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
            <contentPath dataType="String">Data\Audio\SFX\shotsfx.Sound.res</contentPath>
          </_x003C_shotSound_x003E_k__BackingField>
          <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.WeaponStats]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Player\BaseWeaponStats.WeaponStats.res</contentPath>
          </_x003C_statsRef_x003E_k__BackingField>
          <abilityKey dataType="Bool">false</abilityKey>
          <active dataType="Bool">true</active>
          <attackKey dataType="Bool">false</attackKey>
          <gameobj dataType="ObjectRef">213327579</gameobj>
          <itemkey dataType="Bool">false</itemkey>
          <offset dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">0</X>
            <Y dataType="Float">-20</Y>
            <Z dataType="Float">0</Z>
          </offset>
          <ship dataType="ObjectRef">2573642511</ship>
          <wStats dataType="Struct" type="The_ESG.WeaponStats" id="188176608">
            <_x003C_attackCooldown_x003E_k__BackingField dataType="Float">10</_x003C_attackCooldown_x003E_k__BackingField>
            <_x003C_attackCooldownDefault_x003E_k__BackingField dataType="Float">10</_x003C_attackCooldownDefault_x003E_k__BackingField>
            <_x003C_bulletCritChance_x003E_k__BackingField dataType="Float">0.2</_x003C_bulletCritChance_x003E_k__BackingField>
            <_x003C_bulletDamage_x003E_k__BackingField dataType="Int">5</_x003C_bulletDamage_x003E_k__BackingField>
            <_x003C_bulletRange_x003E_k__BackingField dataType="Float">70</_x003C_bulletRange_x003E_k__BackingField>
            <_x003C_bulletSpeed_x003E_k__BackingField dataType="Float">13</_x003C_bulletSpeed_x003E_k__BackingField>
            <assetInfo />
          </wStats>
        </item>
        <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3276104103">
          <active dataType="Bool">true</active>
          <angularDamp dataType="Float">0.3</angularDamp>
          <angularVel dataType="Float">0</angularVel>
          <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
          <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
          <colFilter />
          <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
          <continous dataType="Bool">true</continous>
          <explicitInertia dataType="Float">0</explicitInertia>
          <explicitMass dataType="Float">0</explicitMass>
          <fixedAngle dataType="Bool">false</fixedAngle>
          <gameobj dataType="ObjectRef">213327579</gameobj>
          <ignoreGravity dataType="Bool">true</ignoreGravity>
          <joints />
          <linearDamp dataType="Float">0.3</linearDamp>
          <linearVel dataType="Struct" type="Duality.Vector2" />
          <revolutions dataType="Float">0</revolutions>
          <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3375993723">
            <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="1338642262">
              <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3293353504">
                <density dataType="Float">1</density>
                <friction dataType="Float">0.3</friction>
                <parent dataType="ObjectRef">3276104103</parent>
                <restitution dataType="Float">0.3</restitution>
                <sensor dataType="Bool">false</sensor>
                <vertices dataType="Array" type="Duality.Vector2[]" id="101956572">
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-10.4256134</X>
                    <Y dataType="Float">-11.2275848</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-9.536724</X>
                    <Y dataType="Float">14.7724152</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">10.463274</X>
                    <Y dataType="Float">15.1057587</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">11.2410526</X>
                    <Y dataType="Float">-11.5609131</Y>
                  </item>
                </vertices>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">4</_version>
          </shapes>
        </item>
        <item dataType="Struct" type="The_ESG.Tag" id="3326696344">
          <_x003C_id_x003E_k__BackingField dataType="String">_Player</_x003C_id_x003E_k__BackingField>
          <_x003C_wideId_x003E_k__BackingField dataType="String">_Friendly</_x003C_wideId_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">213327579</gameobj>
        </item>
        <item dataType="Struct" type="The_ESG.PlayerBehaviour" id="1439849168">
          <_x003C_Stats_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Player\PlayerStats.ShipStats.res</contentPath>
          </_x003C_Stats_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <dmgCooldown dataType="Float">0</dmgCooldown>
          <gameobj dataType="ObjectRef">213327579</gameobj>
          <hp dataType="Int">0</hp>
          <ship />
        </item>
      </_items>
      <_size dataType="Int">8</_size>
      <_version dataType="Int">14</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="64598942" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="1525051754">
          <item dataType="Type" id="2365794848" value="Duality.Components.Transform" />
          <item dataType="Type" id="1552464782" value="Duality.Components.Renderers.AnimSpriteRenderer" />
          <item dataType="Type" id="1707800892" value="The_ESG.ActionInputCheck" />
          <item dataType="Type" id="2320103698" value="The_ESG.MovementInputCheck" />
          <item dataType="Type" id="2016856536" value="The_ESG.InputMaster" />
          <item dataType="Type" id="3651519014" value="Duality.Components.Physics.RigidBody" />
          <item dataType="Type" id="896693940" value="The_ESG.Tag" />
          <item dataType="Type" id="2453642026" value="The_ESG.PlayerBehaviour" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="2216717530">
          <item dataType="ObjectRef">2573642511</item>
          <item dataType="ObjectRef">4215730256</item>
          <item dataType="ObjectRef">846031880</item>
          <item dataType="ObjectRef">1984813689</item>
          <item dataType="ObjectRef">1036799292</item>
          <item dataType="ObjectRef">3276104103</item>
          <item dataType="ObjectRef">3326696344</item>
          <item dataType="ObjectRef">1439849168</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">2573642511</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="168431818">kF/AzXd+VUGU7r8/SNWtHg==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">PlayerShip</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
