﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="2549268516">
    <active dataType="Bool">true</active>
    <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="725622019">
      <_items dataType="Array" type="Duality.GameObject[]" id="898777382">
        <item dataType="Struct" type="Duality.GameObject" id="1570243337">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1607401481">
            <_items dataType="Array" type="Duality.Component[]" id="3457859214" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="3930558269">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">1570243337</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="Struct" type="Duality.Components.Transform" id="614616152">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">2549268516</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3" />
                  <posAbs dataType="Struct" type="Duality.Vector3" />
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-208.611084</X>
                  <Y dataType="Float">-210.000015</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-208.611084</X>
                  <Y dataType="Float">-210.000015</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1277678718">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">1570243337</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="338052565">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">1570243337</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2310500565">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="1377698294">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="1267064544">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">338052565</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="561575900">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">84</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="1129959678">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">1570243337</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="908798994">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="4200527440">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">3930558269</ship>
                <stats dataType="Struct" type="The_ESG.ShipStats" id="2115440586">
                  <_x003C_abiltyCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_abiltyCooldownModifier_x003E_k__BackingField>
                  <_x003C_damageModifier_x003E_k__BackingField dataType="Float">0</_x003C_damageModifier_x003E_k__BackingField>
                  <_x003C_damageMultiplier_x003E_k__BackingField dataType="Float">1</_x003C_damageMultiplier_x003E_k__BackingField>
                  <_x003C_friction_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                  <_x003C_linearSpeed_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                  <_x003C_maxVelocity_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                  <_x003C_rotationSpeed_x003E_k__BackingField dataType="Float">0.05</_x003C_rotationSpeed_x003E_k__BackingField>
                  <_x003C_shipCurrentHealthPoints_x003E_k__BackingField dataType="Int">0</_x003C_shipCurrentHealthPoints_x003E_k__BackingField>
                  <_x003C_shipMaxHealthPoints_x003E_k__BackingField dataType="Int">5</_x003C_shipMaxHealthPoints_x003E_k__BackingField>
                  <_x003C_shotCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_shotCooldownModifier_x003E_k__BackingField>
                  <assetInfo />
                </stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="64074923">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2115440586</enemyStats>
                <gameobj dataType="ObjectRef">1570243337</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">3930558269</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1860991552" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="159968963">
                <item dataType="Type" id="161278502" value="Duality.Components.Transform" />
                <item dataType="Type" id="448964282" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                <item dataType="Type" id="1739105574" value="Duality.Components.Physics.RigidBody" />
                <item dataType="Type" id="2793015226" value="The_ESG.BadguyFlyBehaviour" />
                <item dataType="Type" id="3113097254" value="The_ESG.GeneralEnemyBehaviour" />
              </keys>
              <values dataType="Array" type="System.Object[]" id="182497464">
                <item dataType="ObjectRef">3930558269</item>
                <item dataType="ObjectRef">1277678718</item>
                <item dataType="ObjectRef">338052565</item>
                <item dataType="ObjectRef">1129959678</item>
                <item dataType="ObjectRef">64074923</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">3930558269</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="3537121513">50/hCHncGEyHAYnE0aK99g==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">2549268516</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="76755371">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2481518772">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="216051108" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2348520392">
                    <_items dataType="Array" type="System.Int32[]" id="647793260"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">161278502</componentType>
                  <prop dataType="MemberInfo" id="2900824798" value="P:Duality.Components.Transform:RelativePos" />
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-208.611084</X>
                    <Y dataType="Float">-210.000015</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">275</_version>
            </changes>
            <obj dataType="ObjectRef">1570243337</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="3578109729">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3606155361">
            <_items dataType="Array" type="Duality.Component[]" id="3607906670" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="1643457365">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">3578109729</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">614616152</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">200.555573</X>
                  <Y dataType="Float">-204.166687</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">200.555573</X>
                  <Y dataType="Float">-204.166687</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3285545110">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">3578109729</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2345918957">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">3578109729</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2143783693">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2353000230">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2192745728">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">2345918957</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="2177653404">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">84</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="3137826070">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">3578109729</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="3349054378">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="2259657760">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">1643457365</ship>
                <stats dataType="ObjectRef">2115440586</stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="2071941315">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2115440586</enemyStats>
                <gameobj dataType="ObjectRef">3578109729</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">1643457365</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="4215455776" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="3866101227">
                <item dataType="ObjectRef">161278502</item>
                <item dataType="ObjectRef">448964282</item>
                <item dataType="ObjectRef">1739105574</item>
                <item dataType="ObjectRef">2793015226</item>
                <item dataType="ObjectRef">3113097254</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="1344827592">
                <item dataType="ObjectRef">1643457365</item>
                <item dataType="ObjectRef">3285545110</item>
                <item dataType="ObjectRef">2345918957</item>
                <item dataType="ObjectRef">3137826070</item>
                <item dataType="ObjectRef">2071941315</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">1643457365</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="3277287649">m9pNwQg4g0Wn4Cx86WYaOQ==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">2549268516</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2384555251">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="4093506468">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="3365774532" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1293426504">
                    <_items dataType="Array" type="System.Int32[]" id="566603884"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">161278502</componentType>
                  <prop dataType="ObjectRef">2900824798</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">200.555573</X>
                    <Y dataType="Float">-204.166687</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">219</_version>
            </changes>
            <obj dataType="ObjectRef">3578109729</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="2302011903">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="579924543">
            <_items dataType="Array" type="Duality.Component[]" id="1865946286" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="367359539">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">2302011903</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">614616152</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-98.611084</X>
                  <Y dataType="Float">-152.500015</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-98.611084</X>
                  <Y dataType="Float">-152.500015</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2009447284">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">2302011903</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1069821131">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">2302011903</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3627202187">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3330415222">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3343755232">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">1069821131</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="3542830044">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">84</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="1861728244">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">2302011903</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="2487909728">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="356555996">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">367359539</ship>
                <stats dataType="ObjectRef">2115440586</stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="795843489">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2115440586</enemyStats>
                <gameobj dataType="ObjectRef">2302011903</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">367359539</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3686912224" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="2059282933">
                <item dataType="ObjectRef">161278502</item>
                <item dataType="ObjectRef">448964282</item>
                <item dataType="ObjectRef">1739105574</item>
                <item dataType="ObjectRef">2793015226</item>
                <item dataType="ObjectRef">3113097254</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="3584447176">
                <item dataType="ObjectRef">367359539</item>
                <item dataType="ObjectRef">2009447284</item>
                <item dataType="ObjectRef">1069821131</item>
                <item dataType="ObjectRef">1861728244</item>
                <item dataType="ObjectRef">795843489</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">367359539</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="555107903">cK1hx1Ri5kSG/SwPzNinaQ==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">2549268516</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="422516461">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2161681444">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="418880196" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2716014408">
                    <_items dataType="Array" type="System.Int32[]" id="3703951468"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">161278502</componentType>
                  <prop dataType="ObjectRef">2900824798</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-98.611084</X>
                    <Y dataType="Float">-152.500015</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">231</_version>
            </changes>
            <obj dataType="ObjectRef">2302011903</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="3813766994">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1356408742">
            <_items dataType="Array" type="Duality.Component[]" id="1418732544" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="1879114630">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">3813766994</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">614616152</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">53.8889122</X>
                  <Y dataType="Float">-146.666687</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">53.8889122</X>
                  <Y dataType="Float">-146.666687</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3521202375">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">3813766994</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2581576222">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">3813766994</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="566033150">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3760089488">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="388100412">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">2581576222</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="251334468">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">84</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="3373483335">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">3813766994</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="459343555">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="1873959462">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">1879114630</ship>
                <stats dataType="ObjectRef">2115440586</stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="2307598580">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2115440586</enemyStats>
                <gameobj dataType="ObjectRef">3813766994</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">1879114630</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1802412474" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="4026330644">
                <item dataType="ObjectRef">161278502</item>
                <item dataType="ObjectRef">448964282</item>
                <item dataType="ObjectRef">1739105574</item>
                <item dataType="ObjectRef">2793015226</item>
                <item dataType="ObjectRef">3113097254</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="1801766710">
                <item dataType="ObjectRef">1879114630</item>
                <item dataType="ObjectRef">3521202375</item>
                <item dataType="ObjectRef">2581576222</item>
                <item dataType="ObjectRef">3373483335</item>
                <item dataType="ObjectRef">2307598580</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">1879114630</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="2804233904">IjGYksmndUar9NbVNltEBQ==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">2549268516</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2989104038">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="688261120">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="680028316" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1002409160">
                    <_items dataType="Array" type="System.Int32[]" id="2663084652"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">161278502</componentType>
                  <prop dataType="ObjectRef">2900824798</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">53.8889122</X>
                    <Y dataType="Float">-146.666687</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">183</_version>
            </changes>
            <obj dataType="ObjectRef">3813766994</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
      </_items>
      <_size dataType="Int">4</_size>
      <_version dataType="Int">4</_version>
    </children>
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3719882680">
      <_items dataType="Array" type="Duality.Component[]" id="1263296617" length="4">
        <item dataType="ObjectRef">614616152</item>
        <item dataType="Struct" type="The_ESG.SquadBehaviour" id="2898821778">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">2549268516</gameobj>
          <soldierPos dataType="Struct" type="Duality.Vector3" />
          <squad dataType="ObjectRef">614616152</squad>
        </item>
      </_items>
      <_size dataType="Int">2</_size>
      <_version dataType="Int">2</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1517104681" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3203836884">
          <item dataType="ObjectRef">161278502</item>
          <item dataType="Type" id="1832604900" value="The_ESG.SquadBehaviour" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="3553860534">
          <item dataType="ObjectRef">614616152</item>
          <item dataType="ObjectRef">2898821778</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">614616152</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="2638609136">kY+z40E9yU+A8knpcJtJfA==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Squad4Fly</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
