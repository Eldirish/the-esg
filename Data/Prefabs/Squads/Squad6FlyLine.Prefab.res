﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="3367630163">
    <active dataType="Bool">true</active>
    <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="3872329696">
      <_items dataType="Array" type="Duality.GameObject[]" id="3294747612" length="8">
        <item dataType="Struct" type="Duality.GameObject" id="2273991019">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1999486919">
            <_items dataType="Array" type="Duality.Component[]" id="713193678" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="339338655">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">2273991019</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="Struct" type="Duality.Components.Transform" id="1432977799">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">3367630163</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3" />
                  <posAbs dataType="Struct" type="Duality.Vector3" />
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-176.000015</X>
                  <Y dataType="Float">-113</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-176.000015</X>
                  <Y dataType="Float">-113</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1981426400">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">2273991019</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1041800247">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">2273991019</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="4172604615">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="2933225166">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2081868752">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">1041800247</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="2953812668">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">2</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="1833707360">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">50.6512756</dirChangeMax>
                <gameobj dataType="ObjectRef">2273991019</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-0.217917159</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="1195480876">
                  <inext dataType="Int">7</inext>
                  <inextp dataType="Int">28</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="1580190436">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 547078764, 388675363, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">339338655</ship>
                <stats dataType="Struct" type="The_ESG.ShipStats" id="2290025910">
                  <_x003C_abiltyCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_abiltyCooldownModifier_x003E_k__BackingField>
                  <_x003C_damageModifier_x003E_k__BackingField dataType="Float">0</_x003C_damageModifier_x003E_k__BackingField>
                  <_x003C_damageMultiplier_x003E_k__BackingField dataType="Float">1</_x003C_damageMultiplier_x003E_k__BackingField>
                  <_x003C_friction_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                  <_x003C_linearSpeed_x003E_k__BackingField dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">0.4</X>
                    <Y dataType="Float">0.7</Y>
                    <Z dataType="Float">0</Z>
                  </_x003C_linearSpeed_x003E_k__BackingField>
                  <_x003C_maxVelocity_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                  <_x003C_rotationSpeed_x003E_k__BackingField dataType="Float">0.05</_x003C_rotationSpeed_x003E_k__BackingField>
                  <_x003C_shipCurrentHealthPoints_x003E_k__BackingField dataType="Int">0</_x003C_shipCurrentHealthPoints_x003E_k__BackingField>
                  <_x003C_shipMaxHealthPoints_x003E_k__BackingField dataType="Int">5</_x003C_shipMaxHealthPoints_x003E_k__BackingField>
                  <_x003C_shotCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_shotCooldownModifier_x003E_k__BackingField>
                  <assetInfo />
                </stats>
                <xModifier dataType="Float">-0.217917159</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="767822605">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2290025910</enemyStats>
                <gameobj dataType="ObjectRef">2273991019</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">339338655</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2938548480" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="3172507757">
                <item dataType="Type" id="1078466790" value="Duality.Components.Transform" />
                <item dataType="Type" id="434186554" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                <item dataType="Type" id="2108812902" value="Duality.Components.Physics.RigidBody" />
                <item dataType="Type" id="2442883258" value="The_ESG.BadguyFlyBehaviour" />
                <item dataType="Type" id="939092454" value="The_ESG.GeneralEnemyBehaviour" />
              </keys>
              <values dataType="Array" type="System.Object[]" id="3688318712">
                <item dataType="ObjectRef">339338655</item>
                <item dataType="ObjectRef">1981426400</item>
                <item dataType="ObjectRef">1041800247</item>
                <item dataType="ObjectRef">1833707360</item>
                <item dataType="ObjectRef">767822605</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">339338655</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="991740807">2RSZLxNenE2hW/ZF06M8Lg==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">3367630163</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1548318789">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1233662740">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="257454180" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="228142792">
                    <_items dataType="Array" type="System.Int32[]" id="201776748"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">1078466790</componentType>
                  <prop dataType="MemberInfo" id="112604894" value="P:Duality.Components.Transform:RelativePos" />
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-176.000015</X>
                    <Y dataType="Float">-113</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">597</_version>
            </changes>
            <obj dataType="ObjectRef">2273991019</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="2403875401">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1894760341">
            <_items dataType="Array" type="Duality.Component[]" id="4244947574" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="469223037">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">2403875401</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">1432977799</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-31.4444275</X>
                  <Y dataType="Float">-8.166677</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-31.4444275</X>
                  <Y dataType="Float">-8.166677</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2111310782">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">2403875401</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1171684629">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">2403875401</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2273494885">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="4049721238">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="2134143008">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">1171684629</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="1135926236">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">2</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="1963591742">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">59.0741348</dirChangeMax>
                <gameobj dataType="ObjectRef">2403875401</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-0.217917159</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="1029072034">
                  <inext dataType="Int">7</inext>
                  <inextp dataType="Int">28</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="262258448">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 547078764, 388675363, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">469223037</ship>
                <stats dataType="ObjectRef">2290025910</stats>
                <xModifier dataType="Float">-0.217917159</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="897706987">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2290025910</enemyStats>
                <gameobj dataType="ObjectRef">2403875401</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">469223037</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1690990792" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="407864127">
                <item dataType="ObjectRef">1078466790</item>
                <item dataType="ObjectRef">434186554</item>
                <item dataType="ObjectRef">2108812902</item>
                <item dataType="ObjectRef">2442883258</item>
                <item dataType="ObjectRef">939092454</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="2124407520">
                <item dataType="ObjectRef">469223037</item>
                <item dataType="ObjectRef">2111310782</item>
                <item dataType="ObjectRef">1171684629</item>
                <item dataType="ObjectRef">1963591742</item>
                <item dataType="ObjectRef">897706987</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">469223037</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="2776349677">gXqBrMlPVkC/kWwk88hFng==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">3367630163</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3688783519">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1956689540">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="2697200708" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2855440456">
                    <_items dataType="ObjectRef">201776748</_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">1078466790</componentType>
                  <prop dataType="ObjectRef">112604894</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-31.4444275</X>
                    <Y dataType="Float">-8.166677</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">749</_version>
            </changes>
            <obj dataType="ObjectRef">2403875401</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="1011338632">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3560581888">
            <_items dataType="Array" type="Duality.Component[]" id="2893626012" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="3371653564">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">1011338632</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">1432977799</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-60.77777</X>
                  <Y dataType="Float">-35.5000076</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-60.77777</X>
                  <Y dataType="Float">-35.5000076</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="718774013">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">1011338632</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="4074115156">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">1011338632</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="565387476">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3837393636">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="594098116">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">4074115156</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="481463620">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">2</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="571054973">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">60.6002426</dirChangeMax>
                <gameobj dataType="ObjectRef">1011338632</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3" />
                <random dataType="Struct" type="System.Random" id="394529169">
                  <inext dataType="Int">7</inext>
                  <inextp dataType="Int">28</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="952570350">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 547078764, 388675363, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">3371653564</ship>
                <stats dataType="ObjectRef">2290025910</stats>
                <xModifier dataType="Float">0</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="3800137514">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2290025910</enemyStats>
                <gameobj dataType="ObjectRef">1011338632</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">3371653564</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2747040206" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="3401958354">
                <item dataType="ObjectRef">1078466790</item>
                <item dataType="ObjectRef">434186554</item>
                <item dataType="ObjectRef">2108812902</item>
                <item dataType="ObjectRef">2442883258</item>
                <item dataType="ObjectRef">939092454</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="1791540426">
                <item dataType="ObjectRef">3371653564</item>
                <item dataType="ObjectRef">718774013</item>
                <item dataType="ObjectRef">4074115156</item>
                <item dataType="ObjectRef">571054973</item>
                <item dataType="ObjectRef">3800137514</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">3371653564</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="1456878178">x4tKTw1JHEaVRzrMUgZ5Uw==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">3367630163</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="469906844">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="880004280">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1524269164" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1543683496">
                    <_items dataType="ObjectRef">201776748</_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">1078466790</componentType>
                  <prop dataType="ObjectRef">112604894</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-60.77777</X>
                    <Y dataType="Float">-35.5000076</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">585</_version>
            </changes>
            <obj dataType="ObjectRef">1011338632</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="2916541180">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="377416428">
            <_items dataType="Array" type="Duality.Component[]" id="2407188580" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="981888816">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">2916541180</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">1432977799</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-96.77777</X>
                  <Y dataType="Float">-56.8333435</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-96.77777</X>
                  <Y dataType="Float">-56.8333435</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2623976561">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">2916541180</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1684350408">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">2916541180</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3067839808">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="126235932">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="3074971588">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">1684350408</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="2898005316">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">2</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="2476257521">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">45.7067566</dirChangeMax>
                <gameobj dataType="ObjectRef">2916541180</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3" />
                <random dataType="Struct" type="System.Random" id="2799566541">
                  <inext dataType="Int">7</inext>
                  <inextp dataType="Int">28</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="1854569510">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 547078764, 388675363, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">981888816</ship>
                <stats dataType="ObjectRef">2290025910</stats>
                <xModifier dataType="Float">0</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="1410372766">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2290025910</enemyStats>
                <gameobj dataType="ObjectRef">2916541180</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">981888816</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1245350198" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="1288332582">
                <item dataType="ObjectRef">1078466790</item>
                <item dataType="ObjectRef">434186554</item>
                <item dataType="ObjectRef">2108812902</item>
                <item dataType="ObjectRef">2442883258</item>
                <item dataType="ObjectRef">939092454</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="2855596730">
                <item dataType="ObjectRef">981888816</item>
                <item dataType="ObjectRef">2623976561</item>
                <item dataType="ObjectRef">1684350408</item>
                <item dataType="ObjectRef">2476257521</item>
                <item dataType="ObjectRef">1410372766</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">981888816</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="4017516070">wqpVRoP/BUiDzBS0jHkzOw==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">3367630163</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3307879352">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1650382712">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1409034092" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3080299944">
                    <_items dataType="ObjectRef">201776748</_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">1078466790</componentType>
                  <prop dataType="ObjectRef">112604894</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-96.77777</X>
                    <Y dataType="Float">-56.8333435</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">663</_version>
            </changes>
            <obj dataType="ObjectRef">2916541180</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="1509760147">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2428682111">
            <_items dataType="Array" type="Duality.Component[]" id="3249786158" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="3870075079">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">1509760147</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">1432977799</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-136.777771</X>
                  <Y dataType="Float">-82.16669</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-136.777771</X>
                  <Y dataType="Float">-82.16669</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1217195528">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">1509760147</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="277569375">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">1509760147</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3712708975">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="1140049390">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="483354192">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">277569375</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="2229702076">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">2</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="1069476488">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">38.6933365</dirChangeMax>
                <gameobj dataType="ObjectRef">1509760147</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3" />
                <random dataType="Struct" type="System.Random" id="218224852">
                  <inext dataType="Int">7</inext>
                  <inextp dataType="Int">28</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="1011968740">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 547078764, 388675363, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">3870075079</ship>
                <stats dataType="ObjectRef">2290025910</stats>
                <xModifier dataType="Float">0</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="3591733">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2290025910</enemyStats>
                <gameobj dataType="ObjectRef">1509760147</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">3870075079</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2682134368" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="1678021813">
                <item dataType="ObjectRef">1078466790</item>
                <item dataType="ObjectRef">434186554</item>
                <item dataType="ObjectRef">2108812902</item>
                <item dataType="ObjectRef">2442883258</item>
                <item dataType="ObjectRef">939092454</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="4198423368">
                <item dataType="ObjectRef">3870075079</item>
                <item dataType="ObjectRef">1217195528</item>
                <item dataType="ObjectRef">277569375</item>
                <item dataType="ObjectRef">1069476488</item>
                <item dataType="ObjectRef">3591733</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">3870075079</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="3930247167">YFW3AEptJkmSIzT6QWKN1Q==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">3367630163</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3660148781">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="33827620">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="94685892" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3636090696">
                    <_items dataType="ObjectRef">201776748</_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">1078466790</componentType>
                  <prop dataType="ObjectRef">112604894</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-136.777771</X>
                    <Y dataType="Float">-82.16669</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">535</_version>
            </changes>
            <obj dataType="ObjectRef">1509760147</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
      </_items>
      <_size dataType="Int">5</_size>
      <_version dataType="Int">5</_version>
    </children>
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1812849550">
      <_items dataType="Array" type="Duality.Component[]" id="2182321970" length="4">
        <item dataType="ObjectRef">1432977799</item>
        <item dataType="Struct" type="The_ESG.SquadBehaviour" id="3717183425">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">3367630163</gameobj>
          <soldierPos dataType="Struct" type="Duality.Vector3" />
          <squad dataType="ObjectRef">1432977799</squad>
        </item>
      </_items>
      <_size dataType="Int">2</_size>
      <_version dataType="Int">2</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1975959292" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="2533107320">
          <item dataType="ObjectRef">1078466790</item>
          <item dataType="Type" id="1921808748" value="The_ESG.SquadBehaviour" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="44390878">
          <item dataType="ObjectRef">1432977799</item>
          <item dataType="ObjectRef">3717183425</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">1432977799</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="718764836">ebRBo/jWuUGUob1FrR1dkw==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Squad6FlyLine</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
