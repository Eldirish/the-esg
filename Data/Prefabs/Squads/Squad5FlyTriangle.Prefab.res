﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="1239273466">
    <active dataType="Bool">true</active>
    <children dataType="Struct" type="System.Collections.Generic.List`1[[Duality.GameObject]]" id="1946268701">
      <_items dataType="Array" type="Duality.GameObject[]" id="899602662" length="8">
        <item dataType="Struct" type="Duality.GameObject" id="3183642643">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="60901155">
            <_items dataType="Array" type="Duality.Component[]" id="3704921190" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="1248990279">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">3183642643</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="Struct" type="Duality.Components.Transform" id="3599588398">
                  <active dataType="Bool">true</active>
                  <angle dataType="Float">0</angle>
                  <angleAbs dataType="Float">0</angleAbs>
                  <angleVel dataType="Float">0</angleVel>
                  <angleVelAbs dataType="Float">0</angleVelAbs>
                  <deriveAngle dataType="Bool">true</deriveAngle>
                  <gameobj dataType="ObjectRef">1239273466</gameobj>
                  <ignoreParent dataType="Bool">false</ignoreParent>
                  <parentTransform />
                  <pos dataType="Struct" type="Duality.Vector3" />
                  <posAbs dataType="Struct" type="Duality.Vector3" />
                  <scale dataType="Float">1</scale>
                  <scaleAbs dataType="Float">1</scaleAbs>
                  <vel dataType="Struct" type="Duality.Vector3" />
                  <velAbs dataType="Struct" type="Duality.Vector3" />
                </parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-60.1110954</X>
                  <Y dataType="Float">-8.833349</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-60.1110954</X>
                  <Y dataType="Float">-8.833349</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2891078024">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">3183642643</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1951451871">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">3183642643</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2801794079">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="4117277038">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="708741200">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">1951451871</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="1932229052">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">4</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="2743358984">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">3183642643</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="4088952548">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="3283636164">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">1248990279</ship>
                <stats dataType="Struct" type="The_ESG.ShipStats" id="2726508054">
                  <_x003C_abiltyCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_abiltyCooldownModifier_x003E_k__BackingField>
                  <_x003C_damageModifier_x003E_k__BackingField dataType="Float">0</_x003C_damageModifier_x003E_k__BackingField>
                  <_x003C_damageMultiplier_x003E_k__BackingField dataType="Float">1</_x003C_damageMultiplier_x003E_k__BackingField>
                  <_x003C_friction_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                  <_x003C_linearSpeed_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                  <_x003C_maxVelocity_x003E_k__BackingField dataType="Struct" type="Duality.Vector3" />
                  <_x003C_rotationSpeed_x003E_k__BackingField dataType="Float">0.05</_x003C_rotationSpeed_x003E_k__BackingField>
                  <_x003C_shipCurrentHealthPoints_x003E_k__BackingField dataType="Int">0</_x003C_shipCurrentHealthPoints_x003E_k__BackingField>
                  <_x003C_shipMaxHealthPoints_x003E_k__BackingField dataType="Int">5</_x003C_shipMaxHealthPoints_x003E_k__BackingField>
                  <_x003C_shotCooldownModifier_x003E_k__BackingField dataType="Float">0</_x003C_shotCooldownModifier_x003E_k__BackingField>
                  <assetInfo />
                </stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="1677474229">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2726508054</enemyStats>
                <gameobj dataType="ObjectRef">3183642643</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">1248990279</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2648089720" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="2320386633">
                <item dataType="Type" id="2164725134" value="Duality.Components.Transform" />
                <item dataType="Type" id="3267246154" value="Duality.Components.Renderers.AnimSpriteRenderer" />
                <item dataType="Type" id="2389594814" value="Duality.Components.Physics.RigidBody" />
                <item dataType="Type" id="2475077850" value="The_ESG.BadguyFlyBehaviour" />
                <item dataType="Type" id="1604996462" value="The_ESG.GeneralEnemyBehaviour" />
              </keys>
              <values dataType="Array" type="System.Object[]" id="2385553728">
                <item dataType="ObjectRef">1248990279</item>
                <item dataType="ObjectRef">2891078024</item>
                <item dataType="ObjectRef">1951451871</item>
                <item dataType="ObjectRef">2743358984</item>
                <item dataType="ObjectRef">1677474229</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">1248990279</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="3288509035">psBISoqgdkibAleVPlULww==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">1239273466</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="3978638217">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1676025748">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1632382820" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2966473928">
                    <_items dataType="Array" type="System.Int32[]" id="4036784748"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">2164725134</componentType>
                  <prop dataType="MemberInfo" id="1431739102" value="P:Duality.Components.Transform:RelativePos" />
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-60.1110954</X>
                    <Y dataType="Float">-8.833349</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">731</_version>
            </changes>
            <obj dataType="ObjectRef">3183642643</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="2365400458">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="886278046">
            <_items dataType="Array" type="Duality.Component[]" id="38668176" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="430748094">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">2365400458</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">3599588398</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">59.88891</X>
                  <Y dataType="Float">-8.166677</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">59.88891</X>
                  <Y dataType="Float">-8.166677</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2072835839">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">2365400458</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1133209686">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">2365400458</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="4199035478">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="4247390240">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="742009820">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">1133209686</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="3101170372">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">4</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="1925116799">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">2365400458</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="1452926459">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="2646989398">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">430748094</ship>
                <stats dataType="ObjectRef">2726508054</stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="859232044">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2726508054</enemyStats>
                <gameobj dataType="ObjectRef">2365400458</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">430748094</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2361112970" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="3081886396">
                <item dataType="ObjectRef">2164725134</item>
                <item dataType="ObjectRef">3267246154</item>
                <item dataType="ObjectRef">2389594814</item>
                <item dataType="ObjectRef">2475077850</item>
                <item dataType="ObjectRef">1604996462</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="4246176406">
                <item dataType="ObjectRef">430748094</item>
                <item dataType="ObjectRef">2072835839</item>
                <item dataType="ObjectRef">1133209686</item>
                <item dataType="ObjectRef">1925116799</item>
                <item dataType="ObjectRef">859232044</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">430748094</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="1875220072">s2K6fkfwOkegyQJ6rYhyAw==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">1239273466</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="2866950766">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="2607099552">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1999772892" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="3711878600">
                    <_items dataType="Array" type="System.Int32[]" id="133629548"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">2164725134</componentType>
                  <prop dataType="ObjectRef">1431739102</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">59.88891</X>
                    <Y dataType="Float">-8.166677</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">625</_version>
            </changes>
            <obj dataType="ObjectRef">2365400458</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="1893514358">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="941760570">
            <_items dataType="Array" type="Duality.Component[]" id="1920689408" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="4253829290">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">1893514358</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">3599588398</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-0.1110996</X>
                  <Y dataType="Float">33.83331</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-0.1110996</X>
                  <Y dataType="Float">33.83331</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="1600949739">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">1893514358</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="661323586">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">1893514358</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2499952250">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="631572352">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="671203740">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">661323586</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="4243031492">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">4</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="1453230699">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">1893514358</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="3107201759">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="3661508206">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">4253829290</ship>
                <stats dataType="ObjectRef">2726508054</stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="387345944">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2726508054</enemyStats>
                <gameobj dataType="ObjectRef">1893514358</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">4253829290</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2754693818" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="1298031488">
                <item dataType="ObjectRef">2164725134</item>
                <item dataType="ObjectRef">3267246154</item>
                <item dataType="ObjectRef">2389594814</item>
                <item dataType="ObjectRef">2475077850</item>
                <item dataType="ObjectRef">1604996462</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="2653926606">
                <item dataType="ObjectRef">4253829290</item>
                <item dataType="ObjectRef">1600949739</item>
                <item dataType="ObjectRef">661323586</item>
                <item dataType="ObjectRef">1453230699</item>
                <item dataType="ObjectRef">387345944</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">4253829290</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="2221745436">bvqxN22P90agy8gYHbrV7w==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">1239273466</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="187650874">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="3852934656">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="701549724" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1231101128">
                    <_items dataType="Array" type="System.Int32[]" id="3000382060"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">2164725134</componentType>
                  <prop dataType="ObjectRef">1431739102</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-0.1110996</X>
                    <Y dataType="Float">33.83331</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">1533</_version>
            </changes>
            <obj dataType="ObjectRef">1893514358</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="3204878949">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="3325089685">
            <_items dataType="Array" type="Duality.Component[]" id="617308790" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="1270226585">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">3204878949</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">3599588398</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-90.1111</X>
                  <Y dataType="Float">-62.166687</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">-90.1111</X>
                  <Y dataType="Float">-62.166687</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="2912314330">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">3204878949</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="1972688177">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">3204878949</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="3090034513">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3305199598">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="230260304">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">1972688177</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="2043739580">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">4</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="2764595290">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">3204878949</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="3847067150">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="1969831888">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">1270226585</ship>
                <stats dataType="ObjectRef">2726508054</stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="1698710535">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2726508054</enemyStats>
                <gameobj dataType="ObjectRef">3204878949</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">1270226585</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="3163068616" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="3679771455">
                <item dataType="ObjectRef">2164725134</item>
                <item dataType="ObjectRef">3267246154</item>
                <item dataType="ObjectRef">2389594814</item>
                <item dataType="ObjectRef">2475077850</item>
                <item dataType="ObjectRef">1604996462</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="2012066528">
                <item dataType="ObjectRef">1270226585</item>
                <item dataType="ObjectRef">2912314330</item>
                <item dataType="ObjectRef">1972688177</item>
                <item dataType="ObjectRef">2764595290</item>
                <item dataType="ObjectRef">1698710535</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">1270226585</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="1828189165">xtgDsgVdfEq5HVBiGAJx3w==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">1239273466</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="4223276703">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="1464989316">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="52790340" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="598380616">
                    <_items dataType="Array" type="System.Int32[]" id="541150316"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">2164725134</componentType>
                  <prop dataType="ObjectRef">1431739102</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">-90.1111</X>
                    <Y dataType="Float">-62.166687</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">1167</_version>
            </changes>
            <obj dataType="ObjectRef">3204878949</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
        <item dataType="Struct" type="Duality.GameObject" id="3994605930">
          <active dataType="Bool">true</active>
          <children />
          <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="742526">
            <_items dataType="Array" type="Duality.Component[]" id="1050919056" length="8">
              <item dataType="Struct" type="Duality.Components.Transform" id="2059953566">
                <active dataType="Bool">true</active>
                <angle dataType="Float">0</angle>
                <angleAbs dataType="Float">0</angleAbs>
                <angleVel dataType="Float">0</angleVel>
                <angleVelAbs dataType="Float">0</angleVelAbs>
                <deriveAngle dataType="Bool">true</deriveAngle>
                <gameobj dataType="ObjectRef">3994605930</gameobj>
                <ignoreParent dataType="Bool">false</ignoreParent>
                <parentTransform dataType="ObjectRef">3599588398</parentTransform>
                <pos dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">105.888893</X>
                  <Y dataType="Float">-60.83336</Y>
                  <Z dataType="Float">0</Z>
                </pos>
                <posAbs dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">105.888893</X>
                  <Y dataType="Float">-60.83336</Y>
                  <Z dataType="Float">0</Z>
                </posAbs>
                <scale dataType="Float">0.15</scale>
                <scaleAbs dataType="Float">0.15</scaleAbs>
                <vel dataType="Struct" type="Duality.Vector3" />
                <velAbs dataType="Struct" type="Duality.Vector3" />
              </item>
              <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="3702041311">
                <active dataType="Bool">true</active>
                <animDuration dataType="Float">5</animDuration>
                <animFirstFrame dataType="Int">4</animFirstFrame>
                <animFrameCount dataType="Int">0</animFrameCount>
                <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Once" value="0" />
                <animPaused dataType="Bool">true</animPaused>
                <animTime dataType="Float">0</animTime>
                <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
                  <A dataType="Byte">255</A>
                  <B dataType="Byte">255</B>
                  <G dataType="Byte">255</G>
                  <R dataType="Byte">255</R>
                </colorTint>
                <customFrameSequence />
                <customMat />
                <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
                <gameobj dataType="ObjectRef">3994605930</gameobj>
                <offset dataType="Int">0</offset>
                <pixelGrid dataType="Bool">false</pixelGrid>
                <rect dataType="Struct" type="Duality.Rect">
                  <H dataType="Float">256</H>
                  <W dataType="Float">256</W>
                  <X dataType="Float">-128</X>
                  <Y dataType="Float">-128</Y>
                </rect>
                <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
                <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
                  <contentPath dataType="String">Data\Graphics\Sprites\Enemy\badguy_fly.Material.res</contentPath>
                </sharedMat>
                <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
              </item>
              <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="2762415158">
                <active dataType="Bool">true</active>
                <angularDamp dataType="Float">0.3</angularDamp>
                <angularVel dataType="Float">0</angularVel>
                <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
                <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
                <colFilter />
                <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
                <continous dataType="Bool">false</continous>
                <explicitInertia dataType="Float">0</explicitInertia>
                <explicitMass dataType="Float">0</explicitMass>
                <fixedAngle dataType="Bool">false</fixedAngle>
                <gameobj dataType="ObjectRef">3994605930</gameobj>
                <ignoreGravity dataType="Bool">true</ignoreGravity>
                <joints />
                <linearDamp dataType="Float">0.3</linearDamp>
                <linearVel dataType="Struct" type="Duality.Vector2" />
                <revolutions dataType="Float">0</revolutions>
                <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="2104747318">
                  <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="604689248">
                    <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="1942515932">
                      <density dataType="Float">1</density>
                      <friction dataType="Float">0.3</friction>
                      <parent dataType="ObjectRef">2762415158</parent>
                      <restitution dataType="Float">0.3</restitution>
                      <sensor dataType="Bool">true</sensor>
                      <vertices dataType="Array" type="Duality.Vector2[]" id="3429890756">
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">-65.57963</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">41.65038</X>
                          <Y dataType="Float">-63.1105537</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">39.1812439</X>
                          <Y dataType="Float">39.35862</Y>
                        </item>
                        <item dataType="Struct" type="Duality.Vector2">
                          <X dataType="Float">-52.1767731</X>
                          <Y dataType="Float">38.1240845</Y>
                        </item>
                      </vertices>
                    </item>
                  </_items>
                  <_size dataType="Int">1</_size>
                  <_version dataType="Int">4</_version>
                </shapes>
              </item>
              <item dataType="Struct" type="The_ESG.BadguyFlyBehaviour" id="3554322271">
                <_x003C_attackRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]" />
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_statsRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <dirChange dataType="Float">0</dirChange>
                <dirChangeMax dataType="Float">35.58758</dirChangeMax>
                <gameobj dataType="ObjectRef">3994605930</gameobj>
                <movVector dataType="Struct" type="Duality.Vector3">
                  <X dataType="Float">6.80154562</X>
                  <Y dataType="Float">0</Y>
                  <Z dataType="Float">0</Z>
                </movVector>
                <random dataType="Struct" type="System.Random" id="2787181403">
                  <inext dataType="Int">5</inext>
                  <inextp dataType="Int">26</inextp>
                  <SeedArray dataType="Array" type="System.Int32[]" id="1946314134">0, 1929848771, 1814644093, 1798421957, 838961425, 1566573721, 1607011204, 2002822899, 883702015, 362257755, 274661273, 1067603314, 224612850, 2051113826, 1870390281, 1561402080, 1370862199, 2112290179, 547812480, 557370667, 1760125585, 1807497381, 288710135, 688571036, 250691343, 1123708698, 1856779849, 1059932440, 1614147536, 1663219762, 238966732, 547655880, 1801376758, 770391363, 624411592, 734931532, 1582121976, 62699063, 1870423295, 19289535, 2051588279, 1238248121, 1092163070, 5644561, 1155684865, 622355116, 567369452, 547883906, 1053816318, 990744931, 1801226311, 105225179, 560920116, 952736054, 2087167349, 771784864</SeedArray>
                </random>
                <ship dataType="ObjectRef">2059953566</ship>
                <stats dataType="ObjectRef">2726508054</stats>
                <xModifier dataType="Float">6.80154562</xModifier>
              </item>
              <item dataType="Struct" type="The_ESG.GeneralEnemyBehaviour" id="2488437516">
                <_x003C_enemyStatsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.ShipStats]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Data\Enemies\Flyguystats.ShipStats.res</contentPath>
                </_x003C_enemyStatsRef_x003E_k__BackingField>
                <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
                  <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
                </_x003C_stateRef_x003E_k__BackingField>
                <active dataType="Bool">true</active>
                <enemyStats dataType="ObjectRef">2726508054</enemyStats>
                <gameobj dataType="ObjectRef">3994605930</gameobj>
                <hp dataType="Int">5</hp>
                <ship dataType="ObjectRef">2059953566</ship>
              </item>
            </_items>
            <_size dataType="Int">5</_size>
            <_version dataType="Int">5</_version>
          </compList>
          <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="2477832842" surrogate="true">
            <header />
            <body>
              <keys dataType="Array" type="System.Object[]" id="1609892700">
                <item dataType="ObjectRef">2164725134</item>
                <item dataType="ObjectRef">3267246154</item>
                <item dataType="ObjectRef">2389594814</item>
                <item dataType="ObjectRef">2475077850</item>
                <item dataType="ObjectRef">1604996462</item>
              </keys>
              <values dataType="Array" type="System.Object[]" id="2430346006">
                <item dataType="ObjectRef">2059953566</item>
                <item dataType="ObjectRef">3702041311</item>
                <item dataType="ObjectRef">2762415158</item>
                <item dataType="ObjectRef">3554322271</item>
                <item dataType="ObjectRef">2488437516</item>
              </values>
            </body>
          </compMap>
          <compTransform dataType="ObjectRef">2059953566</compTransform>
          <identifier dataType="Struct" type="System.Guid" surrogate="true">
            <header>
              <data dataType="Array" type="System.Byte[]" id="1567827912">hJ3e48Ypp0S/jCTn6RQ7SQ==</data>
            </header>
            <body />
          </identifier>
          <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
          <name dataType="String">BadGuyFly</name>
          <parent dataType="ObjectRef">1239273466</parent>
          <prefabLink dataType="Struct" type="Duality.Resources.PrefabLink" id="1106646286">
            <changes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Resources.PrefabLink+VarMod]]" id="303834400">
              <_items dataType="Array" type="Duality.Resources.PrefabLink+VarMod[]" id="1749018588" length="4">
                <item dataType="Struct" type="Duality.Resources.PrefabLink+VarMod">
                  <childIndex dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="2318763464">
                    <_items dataType="Array" type="System.Int32[]" id="1613203052"></_items>
                    <_size dataType="Int">0</_size>
                    <_version dataType="Int">1</_version>
                  </childIndex>
                  <componentType dataType="ObjectRef">2164725134</componentType>
                  <prop dataType="ObjectRef">1431739102</prop>
                  <val dataType="Struct" type="Duality.Vector3">
                    <X dataType="Float">105.888893</X>
                    <Y dataType="Float">-60.83336</Y>
                    <Z dataType="Float">0</Z>
                  </val>
                </item>
              </_items>
              <_size dataType="Int">1</_size>
              <_version dataType="Int">1035</_version>
            </changes>
            <obj dataType="ObjectRef">3994605930</obj>
            <prefab dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
              <contentPath dataType="String">Data\Prefabs\BadGuyFly.Prefab.res</contentPath>
            </prefab>
          </prefabLink>
        </item>
      </_items>
      <_size dataType="Int">5</_size>
      <_version dataType="Int">5</_version>
    </children>
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="1967720184">
      <_items dataType="Array" type="Duality.Component[]" id="877792119" length="4">
        <item dataType="ObjectRef">3599588398</item>
        <item dataType="Struct" type="The_ESG.SquadBehaviour" id="1588826728">
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">1239273466</gameobj>
          <soldierPos dataType="Struct" type="Duality.Vector3" />
          <squad dataType="ObjectRef">3599588398</squad>
        </item>
      </_items>
      <_size dataType="Int">2</_size>
      <_version dataType="Int">2</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="528998583" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="3979334420">
          <item dataType="ObjectRef">2164725134</item>
          <item dataType="Type" id="2253336676" value="The_ESG.SquadBehaviour" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="2349314358">
          <item dataType="ObjectRef">3599588398</item>
          <item dataType="ObjectRef">1588826728</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">3599588398</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3077486000">sI872ebEAUCi11Qxz5hSSw==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">Squad5FlyTriangle</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
