﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo />
  <objTree dataType="Struct" type="Duality.GameObject" id="493431184">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="2453441991">
      <_items dataType="Array" type="Duality.Component[]" id="2047848654" length="8">
        <item dataType="Struct" type="Duality.Components.Transform" id="2853746116">
          <active dataType="Bool">true</active>
          <angle dataType="Float">0</angle>
          <angleAbs dataType="Float">0</angleAbs>
          <angleVel dataType="Float">0</angleVel>
          <angleVelAbs dataType="Float">0</angleVelAbs>
          <deriveAngle dataType="Bool">true</deriveAngle>
          <gameobj dataType="ObjectRef">493431184</gameobj>
          <ignoreParent dataType="Bool">false</ignoreParent>
          <parentTransform />
          <pos dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">6.61157036</X>
            <Y dataType="Float">85.12397</Y>
            <Z dataType="Float">0</Z>
          </pos>
          <posAbs dataType="Struct" type="Duality.Vector3">
            <X dataType="Float">6.61157036</X>
            <Y dataType="Float">85.12397</Y>
            <Z dataType="Float">0</Z>
          </posAbs>
          <scale dataType="Float">1</scale>
          <scaleAbs dataType="Float">1</scaleAbs>
          <vel dataType="Struct" type="Duality.Vector3" />
          <velAbs dataType="Struct" type="Duality.Vector3" />
        </item>
        <item dataType="Struct" type="Duality.Components.Renderers.AnimSpriteRenderer" id="200866565">
          <active dataType="Bool">true</active>
          <animDuration dataType="Float">0.5</animDuration>
          <animFirstFrame dataType="Int">32</animFirstFrame>
          <animFrameCount dataType="Int">144</animFrameCount>
          <animLoopMode dataType="Enum" type="Duality.Components.Renderers.AnimSpriteRenderer+LoopMode" name="Loop" value="1" />
          <animPaused dataType="Bool">false</animPaused>
          <animTime dataType="Float">0</animTime>
          <colorTint dataType="Struct" type="Duality.Drawing.ColorRgba">
            <A dataType="Byte">255</A>
            <B dataType="Byte">255</B>
            <G dataType="Byte">255</G>
            <R dataType="Byte">255</R>
          </colorTint>
          <customFrameSequence dataType="Struct" type="System.Collections.Generic.List`1[[System.Int32]]" id="1612106405">
            <_items dataType="Array" type="System.Int32[]" id="2214395286">32, 33, 34, 35</_items>
            <_size dataType="Int">4</_size>
            <_version dataType="Int">12</_version>
          </customFrameSequence>
          <customMat />
          <flipMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+FlipMode" name="None" value="0" />
          <gameobj dataType="ObjectRef">493431184</gameobj>
          <offset dataType="Int">0</offset>
          <pixelGrid dataType="Bool">false</pixelGrid>
          <rect dataType="Struct" type="Duality.Rect">
            <H dataType="Float">32</H>
            <W dataType="Float">32</W>
            <X dataType="Float">-16</X>
            <Y dataType="Float">-16</Y>
          </rect>
          <rectMode dataType="Enum" type="Duality.Components.Renderers.SpriteRenderer+UVMode" name="Stretch" value="0" />
          <sharedMat dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Material]]">
            <contentPath dataType="String">Data\Graphics\Sprites\Player\playersheet.Material.res</contentPath>
          </sharedMat>
          <visibilityGroup dataType="Enum" type="Duality.Drawing.VisibilityFlag" name="Group0" value="1" />
        </item>
        <item dataType="Struct" type="Duality.Components.Physics.RigidBody" id="3556207708">
          <active dataType="Bool">true</active>
          <angularDamp dataType="Float">0.3</angularDamp>
          <angularVel dataType="Float">0</angularVel>
          <bodyType dataType="Enum" type="Duality.Components.Physics.BodyType" name="Dynamic" value="1" />
          <colCat dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="Cat1" value="1" />
          <colFilter />
          <colWith dataType="Enum" type="Duality.Components.Physics.CollisionCategory" name="All" value="2147483647" />
          <continous dataType="Bool">true</continous>
          <explicitInertia dataType="Float">0</explicitInertia>
          <explicitMass dataType="Float">0</explicitMass>
          <fixedAngle dataType="Bool">true</fixedAngle>
          <gameobj dataType="ObjectRef">493431184</gameobj>
          <ignoreGravity dataType="Bool">true</ignoreGravity>
          <joints />
          <linearDamp dataType="Float">0.3</linearDamp>
          <linearVel dataType="Struct" type="Duality.Vector2" />
          <revolutions dataType="Float">0</revolutions>
          <shapes dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Components.Physics.ShapeInfo]]" id="1560664008">
            <_items dataType="Array" type="Duality.Components.Physics.ShapeInfo[]" id="3145511532">
              <item dataType="Struct" type="Duality.Components.Physics.PolyShapeInfo" id="46949220">
                <density dataType="Float">1</density>
                <friction dataType="Float">0.3</friction>
                <parent dataType="ObjectRef">3556207708</parent>
                <restitution dataType="Float">0.3</restitution>
                <sensor dataType="Bool">true</sensor>
                <vertices dataType="Array" type="Duality.Vector2[]" id="905999300">
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-4.826209</X>
                    <Y dataType="Float">-4.55394</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">-4.159542</X>
                    <Y dataType="Float">6.69606</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">1.34045792</X>
                    <Y dataType="Float">7.02938843</Y>
                  </item>
                  <item dataType="Struct" type="Duality.Vector2">
                    <X dataType="Float">1.34045792</X>
                    <Y dataType="Float">-4.55394</Y>
                  </item>
                </vertices>
              </item>
            </_items>
            <_size dataType="Int">1</_size>
            <_version dataType="Int">49</_version>
          </shapes>
        </item>
        <item dataType="Struct" type="The_ESG.BulletBehaviour" id="2868360162">
          <_x003C_deathPrefRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
            <contentPath dataType="String">Data\Prefabs\VFX\BaseShotDestroy.Prefab.res</contentPath>
          </_x003C_deathPrefRef_x003E_k__BackingField>
          <_x003C_statsRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.WeaponStats]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Player\BaseWeaponStats.WeaponStats.res</contentPath>
          </_x003C_statsRef_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <bullet dataType="ObjectRef">2853746116</bullet>
          <bulletTime dataType="Float">0</bulletTime>
          <Direction dataType="Struct" type="Duality.Vector3" />
          <gameobj dataType="ObjectRef">493431184</gameobj>
          <wStats dataType="Struct" type="The_ESG.WeaponStats" id="3316327526">
            <_x003C_attackCooldown_x003E_k__BackingField dataType="Float">10</_x003C_attackCooldown_x003E_k__BackingField>
            <_x003C_attackCooldownDefault_x003E_k__BackingField dataType="Float">10</_x003C_attackCooldownDefault_x003E_k__BackingField>
            <_x003C_bulletCritChance_x003E_k__BackingField dataType="Float">0.2</_x003C_bulletCritChance_x003E_k__BackingField>
            <_x003C_bulletDamage_x003E_k__BackingField dataType="Int">5</_x003C_bulletDamage_x003E_k__BackingField>
            <_x003C_bulletRange_x003E_k__BackingField dataType="Float">70</_x003C_bulletRange_x003E_k__BackingField>
            <_x003C_bulletSpeed_x003E_k__BackingField dataType="Float">13</_x003C_bulletSpeed_x003E_k__BackingField>
            <assetInfo />
          </wStats>
        </item>
        <item dataType="Struct" type="The_ESG.Tag" id="3606799949">
          <_x003C_id_x003E_k__BackingField dataType="String">_PlayerShot</_x003C_id_x003E_k__BackingField>
          <_x003C_wideId_x003E_k__BackingField />
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">493431184</gameobj>
        </item>
      </_items>
      <_size dataType="Int">5</_size>
      <_version dataType="Int">5</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="1917167872" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="88337005">
          <item dataType="Type" id="4091849958" value="Duality.Components.Transform" />
          <item dataType="Type" id="602644794" value="Duality.Components.Renderers.AnimSpriteRenderer" />
          <item dataType="Type" id="3702358630" value="Duality.Components.Physics.RigidBody" />
          <item dataType="Type" id="1082978490" value="The_ESG.BulletBehaviour" />
          <item dataType="Type" id="430446054" value="The_ESG.Tag" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1681042168">
          <item dataType="ObjectRef">2853746116</item>
          <item dataType="ObjectRef">200866565</item>
          <item dataType="ObjectRef">3556207708</item>
          <item dataType="ObjectRef">2868360162</item>
          <item dataType="ObjectRef">3606799949</item>
        </values>
      </body>
    </compMap>
    <compTransform dataType="ObjectRef">2853746116</compTransform>
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="3714831751">nWcIk2G2YU6iPjhDsczSbA==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">PlayerBaseShot</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
