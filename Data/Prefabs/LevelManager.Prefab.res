﻿<root dataType="Struct" type="Duality.Resources.Prefab" id="129723834">
  <assetInfo dataType="Struct" type="Duality.Editor.AssetManagement.AssetInfo" id="427169525">
    <customData />
    <importerId />
    <sourceFileHint />
  </assetInfo>
  <objTree dataType="Struct" type="Duality.GameObject" id="3168187175">
    <active dataType="Bool">true</active>
    <children />
    <compList dataType="Struct" type="System.Collections.Generic.List`1[[Duality.Component]]" id="526202636">
      <_items dataType="Array" type="Duality.Component[]" id="779778212" length="4">
        <item dataType="Struct" type="The_ESG.SpawnManager" id="3234573594">
          <_x003C_stageRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StageProperties]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Stages\Stage1V.StageProperties.res</contentPath>
          </_x003C_stageRef_x003E_k__BackingField>
          <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
            <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
          </_x003C_stateRef_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <gameobj dataType="ObjectRef">3168187175</gameobj>
          <spawnTimer dataType="Float">50</spawnTimer>
          <stage dataType="Struct" type="The_ESG.StageProperties" id="2513354490">
            <_x003C_BgMusic_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Sound]]">
              <contentPath dataType="String">Data\Audio\stage1bg.Sound.res</contentPath>
            </_x003C_BgMusic_x003E_k__BackingField>
            <_x003C_cellQuantity_x003E_k__BackingField dataType="Int">0</_x003C_cellQuantity_x003E_k__BackingField>
            <_x003C_maxSpawners_x003E_k__BackingField dataType="Int">1</_x003C_maxSpawners_x003E_k__BackingField>
            <_x003C_maxSquadSpawns_x003E_k__BackingField dataType="Int">20</_x003C_maxSquadSpawns_x003E_k__BackingField>
            <_x003C_spawnIntervals_x003E_k__BackingField dataType="Float">50</_x003C_spawnIntervals_x003E_k__BackingField>
            <_x003C_squadList_x003E_k__BackingField dataType="Struct" type="System.Collections.Generic.List`1[[Duality.ContentRef`1[[Duality.Resources.Prefab]]]]" id="1494440320">
              <_items dataType="Array" type="Duality.ContentRef`1[[Duality.Resources.Prefab]][]" id="558194076" length="8">
                <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Prefabs\Squads\Squad4Fly.Prefab.res</contentPath>
                </item>
                <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Prefabs\Squads\Squad4Fly.Prefab.res</contentPath>
                </item>
                <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Prefabs\Squads\Squad5FlyTriangle.Prefab.res</contentPath>
                </item>
                <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Prefabs\Squads\Squad5FlyTriangle.Prefab.res</contentPath>
                </item>
                <item dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
                  <contentPath dataType="String">Data\Prefabs\Squads\Squad6FlyLine.Prefab.res</contentPath>
                </item>
              </_items>
              <_size dataType="Int">5</_size>
              <_version dataType="Int">13</_version>
            </_x003C_squadList_x003E_k__BackingField>
            <_x003C_stageIndex_x003E_k__BackingField dataType="Int">1</_x003C_stageIndex_x003E_k__BackingField>
            <_x003C_stageName_x003E_k__BackingField dataType="String">Outer Rim</_x003C_stageName_x003E_k__BackingField>
            <_x003C_Vertical_x003E_k__BackingField dataType="Bool">true</_x003C_Vertical_x003E_k__BackingField>
            <assetInfo />
          </stage>
          <state dataType="Struct" type="The_ESG.StateManager" id="465984826">
            <_x003C_gameState_x003E_k__BackingField dataType="Int">0</_x003C_gameState_x003E_k__BackingField>
            <assetInfo />
          </state>
        </item>
        <item dataType="Struct" type="The_ESG.GameManager" id="1026843031">
          <_x003C_bossSpawnTemplate_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
            <contentPath dataType="String">Data\Prefabs\Spawners\BossSpawner.Prefab.res</contentPath>
          </_x003C_bossSpawnTemplate_x003E_k__BackingField>
          <_x003C_cargoSpawnTemplate_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
            <contentPath dataType="String">Data\Prefabs\Spawners\CargoSpawner.Prefab.res</contentPath>
          </_x003C_cargoSpawnTemplate_x003E_k__BackingField>
          <_x003C_enemySpawnTemplate_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
            <contentPath dataType="String">Data\Prefabs\Spawners\EnemySpawner.Prefab.res</contentPath>
          </_x003C_enemySpawnTemplate_x003E_k__BackingField>
          <_x003C_hazardSpawnTemplate_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
            <contentPath dataType="String">Data\Prefabs\Spawners\HazardSpawner.Prefab.res</contentPath>
          </_x003C_hazardSpawnTemplate_x003E_k__BackingField>
          <_x003C_levelListRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.LevelList]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Stages\LevelList.LevelList.res</contentPath>
          </_x003C_levelListRef_x003E_k__BackingField>
          <_x003C_playerTemplate_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[Duality.Resources.Prefab]]">
            <contentPath dataType="String">Data\Prefabs\PlayerShip.Prefab.res</contentPath>
          </_x003C_playerTemplate_x003E_k__BackingField>
          <_x003C_stageRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StageProperties]]">
            <contentPath dataType="String">Data\Scripts_Resources\Data\Stages\Stage1V.StageProperties.res</contentPath>
          </_x003C_stageRef_x003E_k__BackingField>
          <_x003C_stateRef_x003E_k__BackingField dataType="Struct" type="Duality.ContentRef`1[[The_ESG.StateManager]]">
            <contentPath dataType="String">Data\Scripts_Resources\Managers\StateManager.StateManager.res</contentPath>
          </_x003C_stateRef_x003E_k__BackingField>
          <active dataType="Bool">true</active>
          <bSpawn />
          <cSpawn />
          <eSpawn />
          <gameobj dataType="ObjectRef">3168187175</gameobj>
          <hSpawn />
          <pSpawn />
        </item>
      </_items>
      <_size dataType="Int">2</_size>
      <_version dataType="Int">2</_version>
    </compList>
    <compMap dataType="Struct" type="System.Collections.Generic.Dictionary`2[[System.Type],[Duality.Component]]" id="597292790" surrogate="true">
      <header />
      <body>
        <keys dataType="Array" type="System.Object[]" id="549425542">
          <item dataType="Type" id="2145855360" value="The_ESG.SpawnManager" />
          <item dataType="Type" id="3328640206" value="The_ESG.GameManager" />
        </keys>
        <values dataType="Array" type="System.Object[]" id="1858701114">
          <item dataType="ObjectRef">3234573594</item>
          <item dataType="ObjectRef">1026843031</item>
        </values>
      </body>
    </compMap>
    <compTransform />
    <identifier dataType="Struct" type="System.Guid" surrogate="true">
      <header>
        <data dataType="Array" type="System.Byte[]" id="355399942">Zft7Oa2JNEiXAQwnHS3Bdg==</data>
      </header>
      <body />
    </identifier>
    <initState dataType="Enum" type="Duality.InitState" name="Initialized" value="1" />
    <name dataType="String">LevelManager</name>
    <parent />
    <prefabLink />
  </objTree>
</root>
<!-- XmlFormatterBase Document Separator -->
